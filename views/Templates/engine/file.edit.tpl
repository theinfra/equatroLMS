%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="FileEdit">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/file/editsubmit" method="POST" id="FileEditForm" enctype='multipart/form-data'>
	<input type="hidden" name="FileEditFileId" id="FileEditFileId" value="%%GLOBAL_FileEditFileId%%" />
		<table>
			<tr>
				<td>%%LNG_FileTitle%%</td>
				<td><input type="text" id="FileEditFileTitle" name="FileEditFileTitle" value="%%GLOBAL_FileEditFileTitle%%" class="FileEditFileTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_FileDescription%%</td>
				<td><textarea name="FileEditFileDescription" id="FileEditFileDescription" rows="5" cols="40">%%GLOBAL_FileEditFileDescription%%</textarea></td>
			</tr>
			<tr>
				<td colspan="2"><a href="%%GLOBAL_AppPath%%/files/%%GLOBAL_FileEditFileId%%/%%GLOBAL_FileEditFilePath%%">%%LNG_FileLink%%</a></td>
			</tr>
			<tr>
				<td>%%LNG_FileReplace%%</td>
				<td><input type="file" name="FileEditFileUpload" id="FileEditFileUpload" value="%%LNG_File%%" /></td>
			</tr>
			<tr>
				<td><input type="reset" name="FileEditReset" id="FileEditReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="FileEditSubmit" id="FileEditSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>