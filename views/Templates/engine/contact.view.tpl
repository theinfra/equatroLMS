%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%GLOBAL_BreadcrumbsList%%
<h1>%%LNG_Contact%%</h1>
<p>%%LNG_ContactDesc%%</p>
<div class="ContactForm">
	<form id="ContactForm" id="Contactform" action="contact/submit">
		<input type="hidden" id="ContactUserId" name="ContactUserId" value="%%GLOBAL_ContactUserId%%" />
		<div class="ContactFormContainer">
			<table>
				<tbody>
					<tr>
						<td class="FormLabel">%%LNG_Name%%:</td>
						<td><input type="text" id="ContactName" name="ContactName" value="%%GLOBAL_ContactName%%" class="ContactName FormField FormfieldText" /></td>
					</tr>
					<tr>
						<td class="FormLabel">%%LNG_Mail%%:</td>
						<td><input type="text" id="ContactMail" name="ContactMail" value="%%GLOBAL_ContactMail%%" class="ContactMail FormField FormfieldText" /></td>
					</tr>
					<tr>
						<td class="FormLabel">%%LNG_Phone%%:</td>
						<td><input type="text" id="ContactPhone" name="ContactPhone" value="%%GLOBAL_ContactPhone%%" class="ContactPhone FormField FormfieldText" /></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							%%LNG_Message%%:<br />
							<textarea id="ContactMessage" name="ContactMessage" class="ContactMessage FormField FormFielTextarea" rows="15" cols="80">%%GLOBAL_ContactMessage%%</textarea>
						</td>
					</tr>
					<tr class="FormSubmitRow">
						<td colspan="2"><input type="submit" name="ContactSubmit" id="ContactSubmit" value="%%LNG_Submit%%" class="ContactSubmit FormField FormFieldSubmit" /></td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
</div>
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>