%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="FileAdminActions">
%%GLOBAL_BreadcrumbsList%%
	<ul>
		<li><a href="%%GLOBAL_AppPath%%/file/create">%%LNG_FileCreate%%</a></li>
	</ul>
</div>
<div class="FileAdminList">
	<table id="FileAdminTable" class="FileAdminTable">
		%%GLOBAL_FileAdminFileList%%
	</table>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
$(function(){
	$('#FileAdminTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});


$('body').on('click', 'a.FileAdminDelete', function(){
	if(!confirm("%%LNG_ConfirmDeleteElement%%")){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var VfileId = Vrow.attr("fileid");
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/file/remote/?fileid="+VfileId,
		{
			w: "deleteFile"
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				Vrow.remove();
			}
		}
	);
});
</script>
</body>