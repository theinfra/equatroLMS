%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
	<h1>%%LNG_UsergroupEdit%%</h1>
	<form action="%%GLOBAL_AppPath%%/usergroup/editsubmit" method="POST">
	<input type="hidden" name="UsergroupEditGroupid" id="UsergroupEditGroupid" value="%%GLOBAL_UsergroupEditGroupid%%" />
	<table>
		<tr>
			<td>%%LNG_UsergroupName%%</td>
			<td><input type="text" id="UsergroupEditUsername" name="UsergroupEditUsername" value="%%GLOBAL_UsergroupEditUsername%%" class="UsergroupEditForm FormField FormFieldText UsergroupEditUsername" /></td>
		</tr>
		<tr>
			<td><input type="reset" value="%%LNG_Reset%%" id="UserEditReset" name="UserEditReset" class="UserEditForm FormField FormFieldText UserEditReset" /></td>
			<td><input type="submit" value="%%LNG_Submit%%" id="UserEditSubmit" name="UserEditSubmit" class="UserEditForm FormField FormFieldText UserEditSubmit" /></td>
		</tr>
		</table> 
	</form>
	<script type="text/javascript">
	</script>
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>