%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%Panel.HeaderFlashMessages%%
	<div class="LogoCenter">
		<img src="%%GLOBAL_AppPath%%/images/%%GLOBAL_AppLogoFilename%%" />
	</div>
	<h1>PLATAFORMA E-LEARNING</h1>
	<div class="UseForgotPasswordForm">
		<form method="POST" action="%%GLOBAL_AppPath%%/user/forgotpasswordsubmit" id="UserLoginForm" >
			<table style="width: 100%">
				<tr>
					<td colspan="2" align="center">%%LNG_ForgotPasswordExplain%%</td>
				</tr>
				<tr>
					<td class="FormLabel">%%LNG_Mail%%:</td>
					<td><input type="text" id="UserForgotPasswordMail" name="UserForgotPasswordMail" value="%%GLOBAL_UserForgotPasswordMail%%" class="UserForgotPasswordMail FormField FormFieldText" /></td>
				</tr>
				<tr class="FormSubmitRow">
					<td colspan="2"><input type="submit" name="UserForgotPasswordSubmit" id="UserForgotPasswordSubmit" value="%%LNG_Access%%" class="UserForgotPasswordSubmit FormField FormFieldSubmit" /></td>
				</tr>
			</table>
		</form>
	</div>

</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>