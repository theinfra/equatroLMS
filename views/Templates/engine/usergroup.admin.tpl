%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
	<h1>%%LNG_UserGroups%%</h1>
	<p class="UsergroupsAdminActionMenu" id="UsergroupsAdminActionMenu">
		<ul>
			<li><a href="%%GLOBAL_AppPath%%/usergroup/create">%%LNG_UsergroupCreate%%</a></li>
		</ul>
	</p>
	<table id="UsergroupsTable" class="UsergroupsTable">
		<thead>
			<tr>
				<th>%%LNG_UserGroup%%</th>
				<th>%%LNG_Action%%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>%%LNG_LoadingTable%%</td>
				<td>&nbsp;</td>
			</tr>
		</tbody>
	</table>
	
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
%%Mustache.UserGroupAdminListItem%%
<script lang="text/javascript">
$( document ).ready(function(){
	$.getJSON(
		"%%GLOBAL_AppPath%%/usergroup/remote/",
		{
			w: "getUserGroups",
			q: "*"
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				$("#UsergroupsTable > tbody").empty();
				if("msg" in data){
					$("#UsergroupsTable > tbody").append("<tr><td colspan='2'>"+data.msg+"</td></tr>");
				}

				var template = $('#tplUserGroupAdminListItem').html();
				Mustache.parse(template);
				
				$.each(
					data.usergroups,
					function(index, value){
						var newRow = Mustache.render(template, value);
						$("#UsergroupsTable > tbody").append(newRow);
					}
				);
			}
		}
	);
});

$(function(){
	$('#UserAdminTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});

$('body').on('click', 'a.UserGroupAdminDelete', function(){
	if(!confirm("%%LNG_ConfirmDeleteElement%%")){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var Vusergroupid = Vrow.attr("usergroupid");
	
	$.getJSON(
			"%%GLOBAL_AppPath%%/usergroup/remote/",
			{
				w: "deleteGroup",
				usergroupid: Vusergroupid
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					Vrow.remove();
				}
			}
		);
});
</script>
</body>