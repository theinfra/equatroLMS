%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="FileCreate">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/file/createsubmit" method="POST" id="FileCreateForm" enctype='multipart/form-data'>
	<input type="hidden" name="FileCreateFileId" id="FileCreateFileId" value="%%GLOBAL_FileCreateFileId%%" />
		<table>
			<tr>
				<td>%%LNG_FileTitle%%</td>
				<td><input type="text" id="FileCreateFileTitle" name="FileCreateFileTitle" value="%%GLOBAL_FileCreateFileTitle%%" class="FileCreateFileTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_FileDescription%%</td>
				<td><textarea name="FileCreateFileDescription" id="FileCreateFileDescription" rows="5" cols="40"></textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="file" name="FileCreateFileUpload" id="FileCreateFileUpload" value="%%LNG_File%%" /></td>
			</tr>
			<tr>
				<td><input type="reset" name="FileCreateReset" id="FileCreateReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="FileCreateSubmit" id="FileCreateSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>