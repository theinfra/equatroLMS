%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="UserViewPanel">
	<h1>%%LNG_User%%: %%GLOBAL_UserViewSingleUsername%%</h1>
	<ul>
		<li>%%LNG_FirstName%%: %%GLOBAL_UserViewSingleFirstName%% %%GLOBAL_UserViewSingleLastName%%</li>
		<li>%%LNG_Mail%%: <a href="mailto:%%GLOBAL_UserViewSingleMail%%">%%GLOBAL_UserViewSingleMail%%</a></li>
		<li>%%LNG_Phone%%: %%GLOBAL_UserViewSinglePhone%%</li>
	</ul>
	<ul>
		<li>%%LNG_Status%%: %%GLOBAL_UserViewSingleStatusText%%</li>
		<li>%%LNG_MembershipType%%: %%GLOBAL_UserViewSingleMembershipTypeText%%</li>
		<li>%%LNG_UserGroup%%: %%GLOBAL_UserViewSingleUserGroupText%%</li>
	</ul>
</div>
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>