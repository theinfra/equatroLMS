%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
	<h1>%%LNG_UsergroupCreate%%</h1>
	<form action="%%GLOBAL_AppPath%%/usergroup/createsubmit" method="POST">
	<table>
		<tr>
			<td>%%LNG_UsergroupName%%</td>
			<td><input type="text" id="UsergroupCreateUsername" name="UsergroupCreateUsername" value="%%GLOBAL_UsergroupCreateUsername%%" class="UsergroupCreateForm FormField FormFieldText UsergroupCreateUsername" /></td>
		</tr>
		<tr>
			<td><input type="reset" value="%%LNG_Reset%%" id="UserCreateReset" name="UserCreateReset" class="UserCreateForm FormField FormFieldText UserCreateReset" /></td>
			<td><input type="submit" value="%%LNG_Submit%%" id="UserCreateSubmit" name="UserCreateSubmit" class="UserCreateForm FormField FormFieldText UserCreateSubmit" /></td>
		</tr>
		</table> 
	</form>
	<script type="text/javascript">
	</script>
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>