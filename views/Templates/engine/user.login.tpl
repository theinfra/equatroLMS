%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%Panel.HeaderFlashMessages%%
	<div class="LogoCenter">
		<img src="%%GLOBAL_AppPath%%/images/%%GLOBAL_AppLogoFilename%%" />
	</div>
	<h1>PLATAFORMA E-LEARNING</h1>
	<div class="UserLoginForm">
		<form method="POST" action="%%GLOBAL_AppPath%%/user/loginsubmit" id="UserLoginForm" >
			<table style="width: 100%">
				<tr>
					<td class="FormLabel">%%LNG_Username%%:</td>
					<td><input type="text" id="UserLoginUsername" name="UserLoginUsername" value="%%GLOBAL_UserLoginUsername%%" class="UserLoginUsername FormField FormFieldText" /></td>
				</tr>
				<tr>
					<td class="FormLabel">%%LNG_Password%%:</td>
					<td><input type="password" id="UserLoginPassword" name="UserLoginPassword" class="UserLoginPassword FormField FormFieldText" /></td>
				</tr>
				<tr class="FormSubmitRow">
					<td colspan="2"><input type="submit" name="UserLoginSubmit" id="UserLoginSubmit" value="%%LNG_Access%%" class="UserLoginSubmit FormField FormFieldSubmit" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><a href="%%GLOBAL_AppPath%%/user/forgotpassword">%%LNG_ForgotPassword%%</a></td>
				</tr>
			</table>
		</form>
	</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>