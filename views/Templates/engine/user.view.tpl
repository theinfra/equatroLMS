%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="UserViewPanel">
	<ul class="UserViewMenu">
		<li><a href="%%GLOBAL_AppPath%%/user/mydetails">%%LNG_MyUserDetails%%</a></li>
		<li><a href="%%GLOBAL_AppPath%%/user/courses">%%LNG_UserCourses%%</a></li>
	</ul>
</div>

</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>