%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
	<h1>%%LNG_Users%%</h1>
	<p class="UserAdminActionMenu" id="UserAdminActionMenu">
		<ul>
			<li><a href="%%GLOBAL_AppPath%%/user/create">%%LNG_UserCreate%%</a></li>
		</ul>
	</p>
	%%GLOBAL_UsersResultSetTable%%
	
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
<script lang="text/javascript">
$(function(){
	$('#UserAdminTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});
</script>
</body>