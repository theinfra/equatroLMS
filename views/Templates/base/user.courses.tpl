%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="UserCoursesPanel">
%%GLOBAL_BreadcrumbsList%%
	
	<h2>%%LNG_UserCourses%%</h2>

	<ul class="CourseList">
		%%GLOBAL_UserCoursesList%%
	</ul>
</div>

</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>