%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="NewsCreate">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/news/createsubmit" method="POST" id="NewsCreateForm">
		<table>
			<tr>
				<td>%%LNG_NewsTitle%%</td>
				<td><input type="text" id="NewsCreateNewsTitle" name="NewsCreateNewsTitle" value="%%GLOBAL_NewsCreateNewsTitle%%" class="NewsCreateNewsTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_NewsContent%%</td>
				<td><textarea id="NewsCreateNewsContent" name="NewsCreateNewsContent" class="NewsCreateNewsContent FormField FormFieldText" rows="40" cols="120">%%GLOBAL_NewsCreateNewsContent%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_NewsStatus%%</td>
				<td><select name="NewsCreateNewsStatus" id="NewsCreateNewsStatus">
					<option %%GLOBAL_NewsCreateNewsStatus0selected%% value="0">%%LNG_Inactive%%</option>
					<option %%GLOBAL_NewsCreateNewsStatus1selected%% value="1">%%LNG_Active%%</option>
				</select>
			</tr>
			<tr>
				<td><input type="reset" name="NewsCreateReset" id="NewsCreateReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="NewsCreateSubmit" id="NewsCreateSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
<script lang="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    language: 'es'
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>