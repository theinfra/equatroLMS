%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="NewsAdminActions">
%%GLOBAL_BreadcrumbsList%%
	<ul>
		<li><a href="%%GLOBAL_AppPath%%/news/create">%%LNG_NewsCreate%%</a></li>
	</ul>
</div>
<div class="NewsAdminList">
	<table id="NewsAdminListTable" class="NewsAdminListTable">
		<thead>
			<tr>
				<th>%%LNG_NewsId%%</th>
				<th>%%LNG_NewsTitle%%</th>
				<th>%%LNG_NewsContent%%</th>
				<th>%%LNG_Created%%</th>
				<th>%%LNG_Modified%%</th>
				<th>%%LNG_Action%%</th>	
			</tr>
		</thead>
		<tbody>
			<tr id="NewsAdminNoNewsFoundRow" style="display: none">
				<td colspan="6">%%LNG_NoNewsFound%%</td>
			<tr>
			<tr id="NewsAdminLoadingRow">
				<td colspan="6">%%LNG_LoadingList%%</td>
			<tr>
		</tbody>
	</table>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
%%Mustache.NewsListItem%%
<script lang="text/javascript">
$( document ).ready(function(){
	$.getJSON(
			"%%GLOBAL_AppPath%%/news/remote/",
			{
				w: "getNewsList"
			},
			function(data){
				if($.isEmptyObject(data.news)){
					$("#NewsAdminLoadingRow").remove();
					$("#NewsAdminNoNewsFoundRow").css("display", "table-row");
					return;
				}
				
				$("#NewsAdminLoadingRow").remove();
				$("#NewsAdminNoNewsFoundRow").css("display", "none");
				
				var template = $('#tplNewsListItem').html();
				Mustache.parse(template);
				for(Vitem in data.news){
					var tplData = {
							news: data.news[Vitem]
						};
					var newRow = Mustache.render(template, tplData);
					$("#NewsAdminListTable > tbody").append(newRow);
				}
			}
		);
});

$('body').on('click', 'a.NewsAdminNewsDelete', function(){
	var Vrow = $(this).closest("tr");
	
	$.getJSON(
			"%%GLOBAL_AppPath%%/news/remote/",
			{
				w: "deleteNews",
				newsid: Vrow.attr("newsid")
			},
			function(data){
				if(data.success == 1){
					Vrow.remove();
					if($("#NewsAdminListTable > tr").size() == 0){
						$("#NewsAdminNoNewsFoundRow").css("display", "table-row");
					}
				}
				else {
					alert(data.msg);
				}
			}
		);
	
	return false;
});

$(function(){
	$('#NewsAdminListTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});
</script>
</body>