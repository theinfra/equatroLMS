%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="ExamCreate">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/exam/createsubmit" method="POST" id="ExamCreateForm">
	<input type="hidden" name="ExamCreateExamId" id="ExamCreateExamId" value="%%GLOBAL_ExamCreateExamId%%" />
		<table>
			<tr>
				<td>%%LNG_ExamName%%</td>
				<td><input type="text" id="ExamCreateExamName" name="ExamCreateExamName" value="%%GLOBAL_ExamCreateExamName%%" class="ExamCreateExamName FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_ExamTitle%%</td>
				<td><input type="text" id="ExamCreateExamTitle" name="ExamCreateExamTitle" value="%%GLOBAL_ExamCreateExamTitle%%" class="ExamCreateExamTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_ExamDesc%%</td>
				<td><textarea rows="10" cols="80" id="ExamCreateExamDesc" name="ExamCreateExamDesc" class="ExamCreateExamDesc FormField FormFieldTextarea">%%GLOBAL_ExamCreateExamDesc%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_ExamDateStart%%</td>
				<td><input type="text" id="ExamCreateDateStart" name="ExamCreateDateStart" value="%%GLOBAL_ExamCreateDateStart%%" class="ExamCreateDateStart FormField FormFieldText FormFieldDatepicker" /></td>
			</tr>
			<tr>
				<td>%%LNG_ExamDateEnd%%</td>
				<td><input type="text" id="ExamCreateDateEnd" name="ExamCreateDateEnd" value="%%GLOBAL_ExamCreateDateEnd%%" class="ExamCreateDateEnd FormField FormFieldText FormFieldDatepicker" /></td>
			</tr>
			<tr>
				<td>%%LNG_ExamTriesPerUser%%</td>
				<td><select id="ExamCreateTriesPerUser" name="ExamCreateTriesPerUser" class="ExamCreateTriesPerUser FormField FormFieldSelect">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
					<option value="13">13</option>
					<option value="14">14</option>
					<option value="15">15</option>
					<option value="16">16</option>
					<option value="17">17</option>
					<option value="18">18</option>
					<option value="19">19</option>
					<option value="20">20</option>
					<option value="0">%%LNG_Unlimited%%</option>
				</select></td>
			</tr>
			<tr>
				<td>%%LNG_ExamPassingGrade%%</td>
				<td>
					<input type="button" name="ExamCreatePassingGradeSub5" id="ExamCreatePassingGradeSub5" value="&#8609;" class="ExamCreatePassingGradeSub5 FormField FormFieldButton" />
					<input type="button" name="ExamCreatePassingGradeSub1" id="ExamCreatePassingGradeSub1" value="&#8595;"  class="ExamCreatePassingGradeSub1 FormField FormFieldButton" />
					<input type="text" name="ExamCreatePassingGrade" id="ExamCreatePassingGrade" value="%%GLOBAL_ExamCreatePassingGrade%%" size="3"  class="ExamCreatePassingGrade FormField FormFieldButton" onkeypress="return isNumber(event)" />
					<input type="button" name="ExamCreatePassingGradeAdd1" id="ExamCreatePassingGradeAdd1" value="&#8593;"  class="ExamCreatePassingGradeAdd1 FormField FormFieldButton" />
					<input type="button" name="ExamCreatePassingGradeAdd5" id="ExamCreatePassingGradeAdd5" value="&#8607;"  class="ExamCreatePassingGradeAdd5 FormField FormFieldButton" />
				</td>
			</tr>
			<tr>
				<td>%%LNG_ExamDuration%%</td>
				<td>
					<input type="text" id="ExamCreateExamDurationQuantity" name="ExamCreateExamDurationQuantity" value="%%GLOBAL_ExamCreateExamDurationQuantity%%" class="ExamCreateExamDurationQuantity FormField FormFieldText" />
					<select id="ExamCreateExamDurationUnit" name="ExamCreateExamDurationUnit">
						<option value="seconds" %%GLOBAL_ExamCreateExamDurationUnitsecondsselected%%>%%LNG_Seconds%%</option>
						<option value="minutes" %%GLOBAL_ExamCreateExamDurationUnitminutesselected%%>%%LNG_Minutes%%</option>
						<option value="hours" %%GLOBAL_ExamCreateExamDurationUnithoursselected%%>%%LNG_Hours%%</option>
						<option value="days" %%GLOBAL_ExamCreateExamDurationUnitdaysselected%%>%%LNG_Days%%</option>
					</select>
			</tr>
			<tr>
				<td><input type="reset" name="ExamCreateReset" id="ExamCreateReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="ExamCreateSubmit" id="ExamCreateSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
	</table>
</div>
<script lang="text/javascript">
$(".FormFieldDatepicker").datepicker({
			showOn: "button",
	    	buttonImage: "%%GLOBAL_AppPath%%/views/Styles/jquery-ui-datepicker/images/calendar.gif",
	    	buttonImageOnly: true,
	    	buttonText: "Select date",
	    	minDate: 0
	});
	
$("#ExamCreateDateStart").change(function(){
	var VstartDate = $("#ExamCreateDateStart").datepicker("getDate");
	$("#ExamCreateDateEnd").datepicker("option", "minDate", VstartDate);
});

$('body').on('click', '#ExamCreatePassingGradeSub5', function(){
	var Vval = parseInt($("#ExamCreatePassingGrade").val());
	var Vinput = $("#ExamCreatePassingGrade");
	if(Vval > 5){
		Vinput.val(Vval-5);
	}
	else if (Vval <= 5){
		Vinput.val(0);
	}
});

$('body').on('click', '#ExamCreatePassingGradeSub1', function(){
	var Vval = parseInt($("#ExamCreatePassingGrade").val());
	var Vinput = $("#ExamCreatePassingGrade");
	if(Vval > 0){
		Vinput.val(Vval-1);
	}
	else{
		Vinput.val(0);
	}
});

$('body').on('click', '#ExamCreatePassingGradeAdd1', function(){
	var Vval = parseInt($("#ExamCreatePassingGrade").val());
	var Vinput = $("#ExamCreatePassingGrade");
	if(Vval < 100){
		Vinput.val(Vval+1);
	}
	else{
		Vinput.val(100);
	}
});

$('body').on('click', '#ExamCreatePassingGradeAdd5', function(){
	var Vval = parseInt($("#ExamCreatePassingGrade").val());
	var Vinput = $("#ExamCreatePassingGrade");
	if(Vval < 95){
		Vinput.val(Vval+5);
	}
	else if (Vval >= 95) {
		Vinput.val(100);
	}
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>