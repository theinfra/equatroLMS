%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="FileViewContent">
%%GLOBAL_BreadcrumbsList%%
	<h1>%%GLOBAL_FileViewFileTitle%%</h1>
	<p>%%GLOBAL_FileViewFileDesc%%</p>
	<iframe src="%%GLOBAL_AppPath%%/javascript/ViewerJS/#../../%%GLOBAL_FileViewFilePath%%" width='800' height='600' allowfullscreen webkitallowfullscreen></iframe>
</object>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>