<html>
<head>
<link rel="stylesheet" href="%%GLOBAL_AppPath%%/views/Templates/base/exam.pdfviewsubmit.css" />
</head>
<body>
<div class="ExamHeader">
	<h1>%%GLOBAL_ExamViewSubmitExamTitle%%</h1>
	<h3>%%GLOBAL_ExamViewSubmitExamDesc%%</h3>
</div>
<div class="ExamBody">
	<ul class="ExamViewSubmitQuestionList">
	%%GLOBAL_ExamViewSubmitQuestions%%
	</ul>
</div>
</body>
</html>