%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="VideoCreate">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/video/createsubmit" method="POST" id="VideoCreateForm">
		<table>
			<tr>
				<td>%%LNG_VideoExternalId%%</td>
				<td>
					<input type="text" id="VideoCreateVideoExternalId" name="VideoCreateVideoExternalId" value="%%GLOBAL_VideoCreateVideoExternalId%%" class="VideoCreateVideoExternalId FormField FormFieldText" />
					<input type="button" id="VideoCreateGetVideoDetails" name="VideoCreateGetVideoDetails" value="%%LNG_GetVideoDetails%%"/>
					<div class="AjaxLoading" style="display: none">
						<img src="%%GLOBAL_AppPath%%/images/ajax-loader.gif" />
					</div>
				</td>
			</tr>
			<tr>
				<td>%%LNG_VideoTitle%%</td>
				<td><input type="text" id="VideoCreateVideoTitle" name="VideoCreateVideoTitle" value="%%GLOBAL_VideoCreateVideoTitle%%" class="VideoCreateVideoTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_VideoDesc%%</td>
				<td><textarea rows="10" cols="80" id="VideoCreateVideoDesc" name="VideoCreateVideoDesc" class="VideoCreateVideoDesc FormField FormFieldTextarea">%%GLOBAL_VideoCreateVideoDesc%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_VideoThumbnail%%</td>
				<td>
					<div class="VideoCreateVideoThumbnail">
						<img id="VideoCreateVideoThumbnailImg" src="%%GLOBAL_AppPath%%/images/videonothumb.png" />
					</div>
				</td>
			</tr>
			<tr>
				<td>%%LNG_VideoAllowPause%%</td>
				<td>
					<select name="VideoCreateAllowPause" id="VideoCreateAllowPause">
						<option %%GLOBAL_VideoCreateAllowPause0selected%% value="0">%%LNG_LangNo%%</option>
						<option %%GLOBAL_VideoCreateAllowPause1selected%% value="1">%%LNG_LangYes%%</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><input type="reset" name="VideoCreateReset" id="VideoCreateReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="VideoCreateSubmit" id="VideoCreateSubmit" value="%%LNG_Submit%%" disabled="true" /></td>
			</tr>
	</table>
	</form>
</div>
%%Mustache.VideoPickerItem%%
<script lang="text/javascript">
$('body').on('change', '#VideoCreateVideoExternalId', function(){
	$("#VideoCreateSubmit").attr('disabled', true);
});

$('body').on('click', '#VideoCreateGetVideoDetails', function(){
	var apikey = "%%GLOBAL_VideoCreateYouTubeAPIKey%%";
	if(apikey.trim() == ""){
		return true;
	}
	
	if($("#VideoCreateVideoTitle").val().trim() != "" || $("#VideoCreateVideoDesc").val().trim() != ""){
		if(!confirm("%%LNG_WarnLoseCurrentChanges%%")){
			return false;
		}
	}
	
	var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = $("#VideoCreateVideoExternalId").val().match(regExp);
	if (match && match[2].length == 11) {
		$("#VideoCreateVideoExternalId").val(match[2]);
	}
	
	$(".AjaxLoading").show();
	$.getJSON(
		"https://www.googleapis.com/youtube/v3/videos",
		{
			part: "snippet",
			id: $("#VideoCreateVideoExternalId").val(),
			key: apikey 
		},
		function(data){
			$(".AjaxLoading").hide();
			if('error' in data){
				alert("Error!");
			}
			else {
				if(data.items.length == 1 && data.items[0] != null){
					$("#VideoCreateVideoTitle").val(data.items[0].snippet.title);
					$("#VideoCreateVideoDesc").val(data.items[0].snippet.description);
					$("#VideoCreateVideoThumbnailImg").attr("src", data.items[0].snippet.thumbnails.high.url);
					
					$("#VideoCreateSubmit").removeAttr("disabled");
				}
				else {
					alert("%%LNG_VideoIdInvalid%%");
					$("#VideoCreateVideoTitle").val("");
					$("#VideoCreateVideoDesc").val("");
				}
			}
		}	
	);
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>