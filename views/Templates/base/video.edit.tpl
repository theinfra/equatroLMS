%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="VideoEdit">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/video/editsubmit" method="POST" id="VideoEditForm">
	<input type="hidden" name="VideoEditVideoId" id="VideoEditVideoId" value="%%GLOBAL_VideoEditVideoId%%" />
		<table>
			<tr>
				<td>%%LNG_VideoExternalId%%</td>
				<td>
					<input type="text" id="VideoEditVideoExternalId" name="VideoEditVideoExternalId" value="%%GLOBAL_VideoEditVideoExternalId%%" class="VideoEditVideoExternalId FormField FormFieldText" />
					<input type="button" id="VideoEditGetVideoDetails" name="VideoEditGetVideoDetails" value="%%LNG_GetVideoDetails%%"/>
					<div class="AjaxLoading" style="display: none">
						<img src="%%GLOBAL_AppPath%%/images/ajax-loader.gif" />
					</div>
				</td>
			</tr>
			<tr>
				<td>%%LNG_VideoTitle%%</td>
				<td><input type="text" id="VideoEditVideoTitle" name="VideoEditVideoTitle" value="%%GLOBAL_VideoEditVideoTitle%%" class="VideoEditVideoTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_VideoDesc%%</td>
				<td><textarea rows="10" cols="80" id="VideoEditVideoDesc" name="VideoEditVideoDesc" class="VideoEditVideoDesc FormField FormFieldTextarea">%%GLOBAL_VideoEditVideoDesc%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_VideoThumbnail%%</td>
				<td>
					<div class="VideoEditVideoThumbnail">
						<img id="VideoEditVideoThumbnailImg" src="http://img.youtube.com/vi/%%GLOBAL_VideoEditVideoExternalId%%/0.jpg" />
					</div>
				</td>
			</tr>
			<tr>
				<td>%%LNG_VideoAllowPause%%</td>
				<td>
					<select name="VideoEditAllowPause" id="VideoEditAllowPause">
						<option %%GLOBAL_VideoEditAllowPause0selected%% value="0">%%LNG_LangNo%%</option>
						<option %%GLOBAL_VideoEditAllowPause1selected%% value="1">%%LNG_LangYes%%</option>
					</select>
				</td>
			</tr>
			<tr>
				<td><input type="reset" name="VideoEditReset" id="VideoEditReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="VideoEditSubmit" id="VideoEditSubmit" value="%%LNG_Submit%%" disabled="true" /></td>
			</tr>
	</table>
	</form>
</div>
%%Mustache.VideoPickerItem%%
<script lang="text/javascript">
$('body').on('change', '#VideoEditVideoExternalId', function(){
	$("#VideoEditSubmit").attr('disabled', true);
});

$('body').on('click', '#VideoEditGetVideoDetails', function(){
	var apikey = "%%GLOBAL_VideoEditYouTubeAPIKey%%";
	if(apikey.trim() == ""){
		return true;
	}
	
	if($("#VideoEditVideoTitle").val().trim() != "" || $("#VideoEditVideoDesc").val().trim() != ""){
		if(!confirm("%%LNG_WarnLoseCurrentChanges%%")){
			return false;
		}
	}
	
	var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = $("#VideoEditVideoExternalId").val().match(regExp);
	if (match && match[2].length == 11) {
		$("#VideoEditVideoExternalId").val(match[2]);
	}
	
	$(".AjaxLoading").show();
	$.getJSON(
		"https://www.googleapis.com/youtube/v3/videos",
		{
			part: "snippet",
			id: $("#VideoEditVideoExternalId").val(),
			key: apikey 
		},
		function(data){
			$(".AjaxLoading").hide();
			if('error' in data){
				alert("Error!");
			}
			else {
				if(data.items.length == 1 && data.items[0] != null){
					$("#VideoEditVideoTitle").val(data.items[0].snippet.title);
					$("#VideoEditVideoDesc").val(data.items[0].snippet.description);
					$("#VideoEditVideoThumbnailImg").attr("src", data.items[0].snippet.thumbnails.high.url);
					
					$("#VideoEditSubmit").removeAttr("disabled");
				}
				else {
					alert("%%LNG_VideoIdInvalid%%");
					$("#VideoEditVideoTitle").val("");
					$("#VideoEditVideoDesc").val("");
				}
			}
		}	
	);
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>