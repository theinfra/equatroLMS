%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="UserViewPanel">
	<h1>%%LNG_User%%: %%GLOBAL_UserViewSingleUsername%%</h1>
	
	<h2>%%LNG_UserCourses%%</h2>

	<ul class="CourseList">
		%%GLOBAL_UserCoursesList%%
	</ul>
	<div id="UserViewSingleUserAddToCourse" style="%%GLOBAL_UserAddToCourseDisplay%%">
		<form action="%%GLOBAL_AppPath%%/user/addCourse" method="POST">
			<input type="hidden" id="UserViewSingleUserId" name="UserViewSingleUserId" value="%%GLOBAL_UserViewSingleUserId%%" />
			%%LNG_UserAddToCourse%% 
			<select id="UserViewSingleCourseId" name="UserViewSingleCourseId">
				%%GLOBAL_UserViewSingleCourseOptions%%
			</select>
			%%LNG_WithUserAccess%%
			<select name="UserViewSingleAddUserToCourseUserAccess" class="UserViewSingleAddUserToCourseUserAccess">
				<option value="1">%%LNG_CourseStaffLevel1%%</option>
				<option value="10">%%LNG_CourseStaffLevel10%%</option>
				<option value="20">%%LNG_CourseStaffLevel20%%</option>
				<option value="800">%%LNG_CourseStaffLevel800%%</option>
				<option value="999">%%LNG_CourseStaffLevel999%%</option>
			</select>
			<input type="submit" name="UserViewSingleAddUserToCourseSubmit" id="UserViewSingleAddUserToCourseSubmit" value="Agregar" />
		</form>
	</div>
	
	<h2>%%LNG_UserDetails%%</h2>
	<ul>
		<li>%%LNG_FirstName%%: %%GLOBAL_UserViewSingleFirstName%% %%GLOBAL_UserViewSingleLastName%%</li>
		<li>%%LNG_Mail%%: <a href="mailto:%%GLOBAL_UserViewSingleMail%%">%%GLOBAL_UserViewSingleMail%%</a></li>
		<li>%%LNG_Phone%%: %%GLOBAL_UserViewSinglePhone%%</li>
	</ul>
	<ul>
		<li>%%LNG_Status%%: %%GLOBAL_UserViewSingleStatusText%%</li>
		<li>%%LNG_MembershipType%%: %%GLOBAL_UserViewSingleMembershipTypeText%%</li>
		<li>%%LNG_UserGroup%%: %%GLOBAL_UserViewSingleUserGroupText%%</li>
	</ul>
</div>
</div> <!-- WideContent -->
%%Panel.Footer%%
</div> <!-- Container -->
</body>