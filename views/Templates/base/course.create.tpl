%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="CourseCreate">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/course/createsubmit" method="POST" id="CourseCreateForm">
		<table>
			<tr>
				<td>%%LNG_CourseName%%</td>
				<td><input type="text" id="CourseCreateCourseName" name="CourseCreateCourseName" value="%%GLOBAL_CourseCreateCourseName%%" class="CourseCreateCourseName FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_CourseStart%%</td>
				<td><input type="text" id="CourseCreateCourseStart" name="CourseCreateCourseStart" value="%%GLOBAL_CourseCreateCourseStart%%" class="CourseCreateCourseName FormField FormFieldText FormFieldDatepicker" /></td>
			</tr>
			<tr>
				<td>%%LNG_CourseEnd%%</td>
				<td><input type="text" id="CourseCreateCourseEnd" name="CourseCreateCourseEnd" value="%%GLOBAL_CourseCreateCourseEnd%%" class="CourseCreateCourseName FormField FormFieldText FormFieldDatepicker" /></td>
			</tr>
			<tr>
				<td>%%LNG_CourseInstructorName%%</td>
				<td><input type="text" id="CourseCreateCourseInstructorName" name="CourseCreateCourseInstructorName" value="%%GLOBAL_CourseCreateCourseInstructorName%%" class="CourseCreateCourseInstructorName FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_CourseInstructorUserId%%</td>
				<td><input type="text" id="CourseCreateCourseInstructorUserId" name="CourseCreateCourseInstructorUserId" value="%%GLOBAL_CourseCreateCourseInstructorUserId%%" class="CourseCreateCourseInstructorUserId FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_CourseStatus%%</td>
				<td><select name="CourseCreateCourseStatus" id="CourseCreateCourseStatus">
					<option %%GLOBAL_CourseCreateCourseStatus0selected%% value="0">%%LNG_Inactive%%</option>
					<option %%GLOBAL_CourseCreateCourseStatus1selected%% value="1">%%LNG_Active%%</option>
				</select>
			</tr>
			<tr>
				<td><input type="reset" name="CourseCreateReset" id="CourseCreateReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="CourseCreateSubmit" id="CourseCreateSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
<script lang="text/javascript">
$(".FormFieldDatepicker").datepicker({
			showOn: "button",
	    	buttonImage: "%%GLOBAL_AppPath%%/views/Styles/jquery-ui-datepicker/images/calendar.gif",
	    	buttonImageOnly: true,
	    	buttonText: "Select date"
	});
	
$("#CourseCreateCourseInstructorName").autocomplete({
	//appendTo: "#CourseCreateCourseInstructorName",
	autoFocus: true,
	delay: 1000,
	minLength: 2,
	source: function(Vrequest, Vresponse){
		$.getJSON(
			"%%GLOBAL_AppPath%%/user/remote/",
			{
				w: "getusers",
				name_search: Vrequest.term
			},
			function(data){
	           Vresponse($.map(data.users, function (Vvalue, Vkey) {
	                return {
	                    label: Vvalue.name,
	                    value: Vvalue.id
	                };
	            }));
			}
		);
	},
	select: function(Vevent, Vui){
		$("#CourseCreateCourseInstructorName").val(Vui.item.label);
		$("#CourseCreateCourseInstructorUserId").val(Vui.item.value);
		return false;
	}
});

$("#CourseCrerateCourseInstructorUserId").autocomplete({
	//appendTo: "#CourseCreateCourseInstructorUserId",
	autoFocus: true,
	delay: 1000,
	//minLength: 1,
	source: function(Vrequest, Vresponse){
		$.getJSON(
			"%%GLOBAL_AppPath%%/user/remote/",
			{
				w: "getusers",
				id_search: Vrequest.term
			},
			function(data){
	           Vresponse($.map(data.users, function (Vvalue, Vkey) {
	                return {
	                    label: Vvalue.name,
	                    value: Vvalue.id
	                };
	            }));
			}
		);
	},
	select: function(Vevent, Vui){
		$("#CourseCreateCourseInstructorName").val(Vui.item.label);
		$("#CourseCreateCourseInstructorUserId").val(Vui.item.value);
		return false;
	}
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>