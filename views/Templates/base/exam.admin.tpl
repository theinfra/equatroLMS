%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="ExamAdminActions">
%%GLOBAL_BreadcrumbsList%%
	<ul>
		<li><a href="%%GLOBAL_AppPath%%/exam/create">%%LNG_ExamCreate%%</a></li>
	</ul>
</div>
<div class="ExamAdminList">
	%%GLOBAL_ExamAdminList%%
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
$(function(){
	$('#ExamAdminList').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});

$('body').on('click', 'a.ExamAdminUserDelete', function(){
	if(!confirm("%%LNG_ConfirmDeleteElement%%")){
		return false;
	}
});
</script>
</body>