%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="NewsSendemailActions">
%%GLOBAL_BreadcrumbsList%%
</div>
<div class="NewsSendMailResults" style="%%GLOBAL_NewsSendMailResultsShow%%">
	<table id="NewsSendMailResultTable" class="NewsSendMailResultTable">
		<thead>
			<tr>
				<th>Mail</th>
				<th>Msg</th>
			</tr>
		</thead>
		<tbody>
			%%GLOBAL_UserSendMailResultTable%%
		</tbody>
	</table>
</div>
<div class="NewsSendMail">
	<form name="NewsSendMailForm" id="NewsSendMaiForm" method="POST" action="%%GLOBAL_AppPath%%/news/sendemailsubmit">
		<input type="hidden" name="NewsSendMailNewsId" id="NewsSendMailNewsId" value="%%GLOBAL_NewsSendMailNewsId%%" />
		<ul class="NewsSendMailUserGroups">
			%%GLOBAL_NewsSendEmailUserGroups%%
		</ul>
		<input type="submit" name="NewsSendEmailSubmit" id="NewsSendEmailSubmit" value="%%LNG_Submit%%" />
	</form>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">

</script>
</body>