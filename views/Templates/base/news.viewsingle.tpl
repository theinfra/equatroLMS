%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%GLOBAL_BreadcrumbsList%%
<div class="NewsViewSingle NewsItem">
	<div class="NewsTitle"><h3>%%GLOBAL_NewsViewSingleNewsTitle%%</h3></div>
	<h4><span class="NewsAuthor">%%GLOBAL_NewsViewSingleNewsAuthor%%</span> - <span class="NewsDate">%%GLOBAL_NewsViewSingleNewsDate%%</span></h4>
	<div class="NewsContent">%%GLOBAL_NewsViewSingleNewsContent%%</div>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">

</script>
</body>