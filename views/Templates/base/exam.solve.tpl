%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="ExamSolveContent">
%%GLOBAL_BreadcrumbsList%%
	<h1>%%GLOBAL_ExamSolveExamTitle%%</h1>
	<h2 class="ExamSolveQuestionText">%%GLOBAL_ExamSolveQuestionText%%</h2>
	<form id="ExamSolveAnswerList" name="ExamSolveAnswerList" method="POST" action="%%GLOBAL_AppPath%%/exam/solve/%%GLOBAL_ExamSolveExamId%%/%%GLOBAL_ExamSolveQuestionIndexNext%%">
		<p>
			%%GLOBAL_ExamSolveQuestionHelp%%
		</p>
		<ul class="ExamSolveAnswers">
			%%GLOBAL_ExamSolveAnswersOptions%%
		</ul>
		<div class="ExamSolveButtons" id="ExamSolveButtons">
			<input type="button" id="ExamSolvePrev" name="ExamSolvePrev" value="%%LNG_Previous%%" %%GLOBAL_ExamSolvePrevDisabled%% />
			<input type="button" id="ExamSolveNext" name="ExamSolveNext" value="%%LNG_Next%%" />
		</div>
	</form>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
	$(".ExamSolveAnswers li").click(function(){
		$(".ExamSolveAnswers li").css("background-color", "#fff");
		$(this).css("background-color", "#ff0");
		$("input[type=radio]", this).prop("checked", true);
	});


	$("#ExamSolvePrev").click(function(){
		doExamSolvePrev();
	});
	
	$("#ExamSolveNext").click(function(){
		if($(".ExamSolveAnswers :radio:checked").size() == 0){
			// ToDo: Corregir que si se salta una pregunta y el orden es RANDOM se vuelva a sacar la misma pregunta al final en vez de obtener otra
			if(confirm("%%LNG_ExamNoAnswerSelected%%")){
				doExamSolveNext();
			}
		}
		else {
			doExamSolveNext();
		}
	});
	
	function doExamSolvePrev(){
		var questionNum = %%GLOBAL_ExamSolveQuestionIndex%%;
		questionNum--;
		window.location.replace("%%GLOBAL_AppPath%%/exam/solve/%%GLOBAL_ExamSolveExamId%%/"+questionNum);
	}
	
	function doExamSolveNext(){
		var VquestionNum = %%GLOBAL_ExamSolveQuestionIndex%%;
		var VanswerChoice = $(".ExamSolveAnswers :radio:checked").val();
		
		$.getJSON(
				"%%GLOBAL_AppPath%%/exam/remote/",
				{
					w: "saveanswer",
					examid: %%GLOBAL_ExamSolveExamId%%,
					questionid: %%GLOBAL_ExamSolveQuestionId%%,
					answerchoice: VanswerChoice,
				},
				function(data){
					if("correctanswer" in data){
						$("#LiExamSolveAnswer"+data.correctanswerno).effect("highlight", {color: "#00ff00"}, 750).effect("highlight", {color: "#00ff00"}, 750).effect("highlight", {color: "#00ff00"}, 750);
						setTimeout(function() {
								VquestionNum++;
								window.location.replace("%%GLOBAL_AppPath%%/exam/solve/%%GLOBAL_ExamSolveExamId%%/"+VquestionNum);
							}, 3000);
					}
					else {
						VquestionNum++;
						window.location.replace("%%GLOBAL_AppPath%%/exam/solve/%%GLOBAL_ExamSolveExamId%%/"+VquestionNum);
					}
				}
		);
	}
</script>
</body>