%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%GLOBAL_BreadcrumbsList%%
<div class="IndexDashboards Dashboards">
	<div class="Dashboard DashboardMyCourses">
		<h3>%%LNG_MyCourses%%</h3>
		<h4>%%LNG_MyCoursesExplain%%</h4>
		%%GLOBAL_IndexDashboardMyCourses%%
	</div>
	<div class="Dashboard DashboardMyTasks">
		<h3>%%LNG_MyTasks%%</h3>
		<h4>%%LNG_MyTasksExplain%%</h4>
		%%GLOBAL_IndexDashboardMyTasks%%
	</div>
</div>
<div class="IndexNews">
	<h2>%%LNG_LatestNews%%</h2>
	%%GLOBAL_IndexNews%%
	<a href="%%GLOBAL_AppPath%%/news">%%LNG_MoreNews%%</a>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>