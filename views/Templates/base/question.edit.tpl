%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="QuestionEdit">
%%GLOBAL_BreadcrumbsList%%
	<form id="QuestionEditForm" action="%%GLOBAL_AppPath%%/question/editsubmit" method="POST">
	<input type="hidden" id="QuestionEditQuestionId" name="QuestionEditQuestionId" value="%%GLOBAL_QuestionEditQuestionId%%" />
	<input type="hidden" id="QuestionEditExamId" name="QuestionEditExamId" value="%%GLOBAL_QuestionEditExamId%%" />
		<table id="QuestionEditTable">
			<tr>
				<td>%%LNG_QuestionText%%</td>
				<td><textarea id="QuestionEditQuestionText" name="QuestionEditQuestionText" class="QuestionEditQuestionText FormField FormFieldText" rows="10" cols="70">%%GLOBAL_QuestionEditQuestionText%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_QuestionHelp%%</td>
				<td><textarea id="QuestionEditQuestionHelp" name="QuestionEditQuestionHelp" class="QuestionEditQuestionHelp FormField FormFieldText" rows="10" cols="70">%%GLOBAL_QuestionEditQuestionHelp%%</textarea></td>
			</tr>
			
			<tr>
				<td><input type="reset" name="QuestionEditReset" id="QuestionEditReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="QuestionEditSubmit" id="QuestionEditSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
<div class="QuestionEditAnswers">
	<table id="QuestionEditAnswersTable">
		%%GLOBAL_QuestionEditAnswersTable%%
	</table>
</div>
%%Mustache.AnswerRowEdit%%
%%Mustache.AnswerRow%%
%%Mustache.AnswerRowNew%%
<script lang="text/javascript">
function resetRow(Vrow, VAnswerid){
	DisableFormElements("EditRowAnswer");
	$.getJSON(
		"%%GLOBAL_AppPath%%/answer/remote/",
		{
			w: "getAnswer",
			answerid: VAnswerid
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowAnswer");
			}
			else {
				var template = $('#tplAnswerRow').html();
				Mustache.parse(template);
				var vNewRow = Mustache.render(template, data.answer);
				Vrow.replaceWith(vNewRow);
			}
		}
	);	
}

function resetAllRows(){
	if($(".RowAnswerEdited").length > 0){
		if(!confirm("%%LNG_WarnLoseCurrentChanges%%")){
			return false;
		}
	}
	
	$(".NewRowQuestionAnswer").remove();
	$(".EditRowAnswer").each(function() {
		var VAnswerid = $(this).find("#AnswerEditAnswerId").val();
		resetRow($(this), VAnswerid);
	});

	return true;
}

$('body').on('change', '.QuestionEditAnswers .FormField', function(){
	$(this).closest("tr:not(.RowAnswerEdited)").addClass("RowAnswerEdited");
});

$('body').on('click', 'a.RowQuestionEditAnswer', function(){
	if(!resetAllRows()){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var Vanswer = Vrow.attr("answerid");
	$.getJSON(
		"%%GLOBAL_AppPath%%/answer/remote/",
		{
			w: "getAnswer",
			answerid: Vanswer
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				var template = $('#tplAnswerRowEdit').html();
				var SavedRow = Mustache.render(template, data.answer);
				Vrow.replaceWith(SavedRow);
			}
		}
	);
});

$('body').on('click', 'a.EditQuestionAddNewAnswer', function(){
	Vrow = $(this).closest("tr");

	var template = $('#tplAnswerRowNew').html();
	var SavedRow = Mustache.render(template);
	Vrow.replaceWith(SavedRow);
});

$('body').on('click', 'a.NewRowQuestionAnswerSave', function(){
	Vrow = $(this).closest("tr");
	Vorder = Vrow.find("#NewRowQuestionAnswerOrder").val();

	$.getJSON(
			"%%GLOBAL_AppPath%%/answer/remote/",
			{
				w: "addAnswer",
				answertext: $("#NewRowQuestionAnswerText").val(),
				questionid: %%GLOBAL_QuestionEditQuestionId%%,
				examid: %%GLOBAL_QuestionEditExamId%%,
				answerorder: Vorder
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					var template = $('#tplAnswerRow').html();
					var SavedRow = Mustache.render(template, data.answer);
					Vrow.replaceWith(SavedRow);
					refreshOrders();
				}
			}
	);
});

$('body').on('click', 'a.RowQuestionAddAnswer', function(){
	if(!resetAllRows()){
		return false;
	}
	Vrow = $(this).closest("tr");
	var Vorder = Vrow.find("#RowQuestionAnswerOrder").val();
	Vorder++;

	var template = $('#tplAnswerRowNew').html();
	var Vdata = {answerorder: Vorder};
	var SavedRow = Mustache.render(template, Vdata);
	Vrow.after(SavedRow);
});

$('body').on('click', 'a.AnswerEditAnswerCancel', function(){
	var Vrow = $(this).closest("tr");
	var Vanswerid = Vrow.find("#AnswerEditAnswerId").val();

	resetRow(Vrow, Vanswerid);
});

$('body').on('click', 'a.NewRowQuestionAnswerCancel', function(){
	var Vrow = $(this).closest("tr");
	Vrow.remove();
});

$('body').on('click', 'a.RowQuestionDeleteAnswer', function(){
	if(!confirm("%%LNG_ConfirmDeleteElement%%")){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var VanswerId = Vrow.attr("answerid");
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/answer/remote/",
		{
			w: "deleteAnswer",
			answerid: VanswerId
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				Vrow.remove();
			}
		}
	);
});

function refreshOrders(){	
	$.getJSON(
			"%%GLOBAL_AppPath%%/question/remote/",
			{
				w: "getAnswers",
				questionid: %%GLOBAL_QuestionEditQuestionId%%
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					$("#QuestionEditAnswersTable tbody").empty();
					var Vtemplate = $('#tplAnswerRow').html();
					var Vdata = {};
					var SavedRow = "";
					for(Vanswer in data.answers){
						Vdata = { 
							answerid: data.answers[Vanswer].answerid,
							answerorder: data.answers[Vanswer].answerorder,
							answertext: data.answers[Vanswer].answertext
						};
						SavedRow = Mustache.render(Vtemplate, Vdata);
						$("#QuestionEditAnswersTable tbody").append(SavedRow);
					}
				}
			}
	);
}

$('body').on('click', '.RowQuestionCorrectAnswerRadio', function(){
	$(".RowQuestionCorrectAnswerRadio").attr('disabled', true);
	var Vanswerid = $(this).val(); 
	
	$.getJSON(
			"%%GLOBAL_AppPath%%/question/remote/",
			{
				w: "saveCorrectAnswer",
				questionid: %%GLOBAL_QuestionEditQuestionId%%,
				answerid: Vanswerid
			},
			function(data){
				$(".RowQuestionCorrectAnswerRadio").removeAttr('disabled');
				if(data.success == 0){
					alert(data.msg);
				}
			}
	);
});

$("#QuestionEditAnswersTable").children("tbody").sortable({
	items: "> tr",
	axis: "y",
	update: function(event, ui){
		var Vserialized = "";
		var Vindex = 0;
		$("#QuestionEditAnswersTable tr.RowQuestionAnswer").each(function(){
			Vserialized += $(this).attr("answerid") + "=" + Vindex+ "&";
			$(this).find("#RowQuestionAnswerOrder").val(Vindex);
			Vindex++;
		});
		$.getJSON(
			"%%GLOBAL_AppPath%%/question/remote/",
			{
				w: "saveAnswersOrder",
				serialized: Vserialized
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
					return;
				}
			}
		);
	}
});

$('body').on('click', '.AnswerEditAnswerSave', function(){
	var Vrow = $(this).closest("tr");
	var Vanswerid = Vrow.attr("answerid");
	
	$.getJSON(
			"%%GLOBAL_AppPath%%/answer/remote/",
			{
				w: "saveAnswer",
				answerid: Vanswerid,
				answertext: $("#AnswerEditAnswerText").val(),
				questionid: %%GLOBAL_QuestionEditQuestionId%%,
				examid: %%GLOBAL_QuestionEditExamId%%
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					var template = $('#tplAnswerRow').html();
					var SavedRow = Mustache.render(template, data.answer);
					Vrow.replaceWith(SavedRow);
					refreshOrders();
				}
			}
	);
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>