%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="CourseEdit">
%%GLOBAL_BreadcrumbsList%%
	<ul id="tabnav">
		<li><a href="#" class="active" id="tab0" onclick="ShowTab(0)">%%LNG_CourseDetails%%</a></li>
		<li><a href="#" class="active" id="tab1" onclick="ShowTab(1)">%%LNG_CourseMembers%%</a></li>
	</ul>
	<div id="div0">		
		<div class="CourseEditCourseDetails">
			<form action="%%GLOBAL_AppPath%%/course/editsubmit" method="POST" id="CourseEditForm">
			<input type="hidden" name="CourseEditCourseId" id="CourseEditCourseId" value="%%GLOBAL_CourseEditCourseId%%" />
				<table>
					<tr>
						<td>%%LNG_CourseName%%</td>
						<td><input type="text" id="CourseEditCourseName" name="CourseEditCourseName" value="%%GLOBAL_CourseEditCourseName%%" class="CourseEditCourseName FormField FormFieldText" /></td>
					</tr>
					<tr>
						<td>%%LNG_CourseStart%%</td>
						<td><input type="text" id="CourseEditCourseStart" name="CourseEditCourseStart" value="%%GLOBAL_CourseEditCourseStart%%" class="CourseEditCourseName FormField FormFieldText FormFieldDatepicker" /></td>
					</tr>
					<tr>
						<td>%%LNG_CourseEnd%%</td>
						<td><input type="text" id="CourseEditCourseEnd" name="CourseEditCourseEnd" value="%%GLOBAL_CourseEditCourseEnd%%" class="CourseEditCourseName FormField FormFieldText FormFieldDatepicker" /></td>
					</tr>
					<tr>
						<td>%%LNG_CourseInstructorName%%</td>
						<td><input type="text" id="CourseEditCourseInstructorName" name="CourseEditCourseInstructorName" value="%%GLOBAL_CourseEditCourseInstructorName%%" class="CourseEditCourseInstructorName FormField FormFieldText" /></td>
					</tr>
					<tr>
						<td>%%LNG_CourseInstructorUserId%%</td>
						<td><input type="text" id="CourseEditCourseInstructorUserId" name="CourseEditCourseInstructorUserId" value="%%GLOBAL_CourseEditCourseInstructorUserId%%" class="CourseEditCourseInstructorUserId FormField FormFieldText" /></td>
					</tr>
					<tr>
						<td>%%LNG_CourseStatus%%</td>
						<td><select name="CourseEditCourseStatus" id="CourseEditCourseStatus">
							<option %%GLOBAL_CourseEditCourseStatus0selected%% value="0">%%LNG_Inactive%%</option>
							<option %%GLOBAL_CourseEditCourseStatus1selected%% value="1">%%LNG_Active%%</option>
						</select>
					</tr>
					<tr>
						<td><input type="reset" name="CourseEditReset" id="CourseEditReset" value="%%LNG_Reset%%" /></td>
						<td><input type="submit" name="CourseEditSubmit" id="CourseEditSubmit" value="%%LNG_Submit%%" /></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="CourseEditCourseControls">
			<input type="button" name="CourseEditResetCourse" id="CourseEditResetCourse" value="%%LNG_CourseReset%%" />
		</div>
		<div class="CourseEditCourseModules">
			<form id="CourseEditModulesForm" name="CourseEditModulesForm" method="POST" action="#">
			<table id="CourseEditCourseModulesTable">
			<thead>
				<tr>
					<th>%%LNG_CourseModuleName%%</th>
					<th width="140px">%%LNG_CourseModuleType%%</th>
					<th width="132px">%%LNG_CourseModuleObject%%</th>
					<th width="75px">%%LNG_CourseModuleRequired%%</th>
					<th width="90px">%%LNG_CourseModuleStatus%%</th>
					<th width="65px">%%LNG_Action%%</th>
				</tr>
			</thead>
			%%GLOBAL_CourseEditModuleList%%
			</table>
			</form>
		</div>
	</div>
	
	<div id="div1">
		<p>%%LNG_AddCourseUserExplain%%</p>
		<form action="%%GLOBAL_AppPath%%/course/addusermembership" id="CourseEditAddUserMembershipToCourseForm" name="CourseEditAddUserMembershipToCourseForm" method="POST">
		<input type="hidden" name="CourseEditAddUserMembershipToCourseCourseId" id="CourseEditAddUserMembershipToCourseCourseId" value="%%GLOBAL_CourseEditCourseId%%" />
		%%LNG_AddUserMembershipToCourse%%
			<select name="CourseEditAddUserMembershipToCourseMembershipType" id="UserEditMembershipType">
				<option %%GLOBAL_UserEditMembershipType0selected%% value="0">%%LNG_UserMembership0%%</option>
				<option %%GLOBAL_UserEditMembershipType1selected%% value="1">%%LNG_UserMembership1%%</option>
				<option %%GLOBAL_UserEditMembershipType2selected%% value="2">%%LNG_UserMembership2%%</option>
				<option %%GLOBAL_UserEditMembershipType3selected%% value="3">%%LNG_UserMembership3%%</option>
				<option %%GLOBAL_UserEditMembershipType4selected%% value="4">%%LNG_UserMembership4%%</option>
				<option %%GLOBAL_UserEditMembershipType5selected%% value="5">%%LNG_UserMembership5%%</option>
				<option %%GLOBAL_UserEditMembershipType6selected%% value="6">%%LNG_UserMembership6%%</option>
				<option %%GLOBAL_UserEditMembershipType7selected%% value="7">%%LNG_UserMembership7%%</option>
			</select>
			%%LNG_WithUserAccess%%
			<select name="CourseEditAddUserMembershipToCourseUserAccess" class="CourseEditAddUserMembershipToCourseUserAccess">
				<option value="1">%%LNG_CourseStaffLevel1%%</option>
				<option value="10">%%LNG_CourseStaffLevel10%%</option>
				<option value="20">%%LNG_CourseStaffLevel20%%</option>
				<option value="800">%%LNG_CourseStaffLevel800%%</option>
				<option value="999">%%LNG_CourseStaffLevel999%%</option>
			</select>
			<input type="submit" name="CourseEditAddUserMembershipToCourseSubmit" id="CourseEditAddUserMembershipToCourseSubmit" value="Agregar" />
		</form>
		<input type="text" id="CourseEditAddCourseUser" name="CourseEditAddCourseUser" value="" class="CourseEditAddCourseUser FormField FormFieldText" />
		<table id="CourseEditCurrentUsers">
			<thead>
				<tr>
					<th>%%LNG_Username%%</th>
					<th>%%LNG_FirstName%%</th>
					<th>%%LNG_UserPermissions%%
					<th>%%LNG_Action%%</th>
				</tr>
			</thead>
			<tbody>
				%%GLOBAL_CourseEditCurrentMembersTable%%
			</tbody>
		</table>
	</div>
</div>
%%Mustache.NewModuleRow%%
%%Mustache.ModuleRowEdit%%
%%Mustache.ModuleRow%%
%%Mustache.CourseEditNoModulesFoundRow%%
%%Mustache.CourseEditCurrentMembersRow%%
%%Mustache.FormSelect%%
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
var AppPath = "%%GLOBAL_AppPath%%";

$(".FormFieldDatepicker").datepicker({
			showOn: "button",
	    	buttonImage: "%%GLOBAL_AppPath%%/views/Styles/jquery-ui-datepicker/images/calendar.gif",
	    	buttonImageOnly: true,
	    	buttonText: "Select date"
	}); 

$("#CourseEditCourseInstructorName").autocomplete({
	//appendTo: "#CourseEditCourseInstructorName",
	autoFocus: true,
	delay: 1000,
	minLength: 2,
	source: function(Vrequest, Vresponse){
		$.getJSON(
			"%%GLOBAL_AppPath%%/user/remote/",
			{
				w: "getusers",
				name_search: Vrequest.term
			},
			function(data){
	           Vresponse($.map(data.users, function (Vvalue, Vkey) {
	                return {
	                    label: Vvalue.name,
	                    value: Vvalue.id
	                };
	            }));
			}
		);
	},
	select: function(Vevent, Vui){
		$("#CourseEditCourseInstructorName").val(Vui.item.label);
		$("#CourseEditCourseInstructorUserId").val(Vui.item.value);
		return false;
	}
});

$("#CourseEditCourseInstructorUserId").autocomplete({
	//appendTo: "#CourseEditCourseInstructorUserId",
	autoFocus: true,
	delay: 1000,
	//minLength: 1,
	source: function(Vrequest, Vresponse){
		$.getJSON(
			"%%GLOBAL_AppPath%%/user/remote/",
			{
				w: "getusers",
				id_search: Vrequest.term
			},
			function(data){
	           Vresponse($.map(data.users, function (Vvalue, Vkey) {
	                return {
	                    label: Vvalue.name,
	                    value: Vvalue.id
	                };
	            }));
			}
		);
	},
	select: function(Vevent, Vui){
		$("#CourseEditCourseInstructorName").val(Vui.item.label);
		$("#CourseEditCourseInstructorUserId").val(Vui.item.value);
		return false;
	}
});

$("#CourseEditAddCourseUser").autocomplete({
	autoFocus: true,
	delay: 1000,
	minLength: 3,
	source: function(Vrequest, Vresponse){
		$.getJSON(
			"%%GLOBAL_AppPath%%/user/remote/",
			{
				w: "getusers",
				name_search: Vrequest.term
			},
			function(data){
	           Vresponse($.map(data.users, function (Vvalue, Vkey) {
	                return {
	                    label: Vvalue.name,
	                    value: Vvalue.id
	                };
	            }));
			}
		);
	},
	select: function(Vevent, Vui){
		$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "addusertocourse",
				courseid: %%GLOBAL_CourseEditCourseId%%,
				userid: Vui.item.value
			},
			function(data){
				$("#CourseEditAddCourseUser").val("");
				if(data.success == 0){
					alert(data.msg);
					return;
				}
				else {
					$("#CourseEditCurrentMembersNoUsersFoundRow").remove();
					var template = $('#tplCourseEditCurrentMembersRow').html();
					Mustache.parse(template);
					var tplData = {
							user: data.user
						};
					var newRow = Mustache.render(template, tplData);
					$("#CourseEditCurrentUsers > tbody").append(newRow);
				}
			}
		);

		return false;
	}
});

$("#CourseEditCourseModulesTable").children("tbody").sortable({
	items: "> tr",
	axis: "y",
	update: function(event, ui){
		var Vserialized = "";
		var Vindex = 0;
		$("#CourseEditCourseModulesTable tr.RowCourseModule").each(function(){
			Vserialized += $(this).attr("moduleid") + "=" + Vindex+ "&";
			$(this).find("#RowCourseModuleOrder").val(Vindex);
			Vindex++;
		});
		$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "savemodulesorder",
				serialized: Vserialized
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
					return;
				}
			}
		);
	}
});

$('body').on('change', '.FormField', function(){
	$(this).closest("tr:not(.RowModuleEdited)").addClass("RowModuleEdited");
});
	
$('body').on('click', '.CourseEditCourseModules .CourseEditAddFirstModule a', function(){
		var template = $('#tplNewModuleRow').html();
		Mustache.parse(template);
		var tplData = {
				moduleorder: 0,
			};
		var newRow = Mustache.render(template, tplData);
				
		$("#CourseEditCourseModulesTable tr:last").replaceWith(newRow);
		$(this).parent().parent().remove();
});

$('body').on('change', '#EditRowCourseModuleType', function(){
	if($(this).val() == 0){
		$("#EditRowCourseModuleObjectId").empty();
		$("#EditRowCourseModuleObjectId").append("<option value=\"\">%%LNG_SelectTypeFirst%%</option>");
		return;
	}
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		{
			w: "getobjects",
			objecttype: $("#EditRowCourseModuleType").val(),
		},
		function(data){
			$("#EditRowCourseModuleObjectId").empty();
			if(data.success == 0){
				$("#EditRowCourseModuleObjectId").append("<option value=\"\">%%LNG_SelectTypeFirst%%</option>");
				alert(data.msg);
				return;
			}

			if(data.hasOwnProperty("objects")){
				var template = $('#tplFormSelect').html();
				var tpldata = {
					name: "EditRowCourseModuleObjectId",
					options: data.objects
				};
				var Vrendered = Mustache.render(template, tpldata);
				$("#EditRowCourseModuleObjectId").replaceWith(Vrendered);
			}
			else if(data.hasOwnProperty("input")) {
				$("#EditRowCourseModuleObjectId").replaceWith('<input type="text" name="'+data.input.inputname+'" id="'+data.input.inputname+'" />');
			}
		}
	);
});

$('body').on('click', 'a.NewRowCourseModuleSave', function(){
	DisableFormElements("EditRowCourseModule");
	if($("#EditRowCourseModuleObjectId").val() == ""){
		alert("%%LNG_CoursePleaseSelectObject%%");
		EnableFormElements("EditRowCourseModule");
		return;
	}

	var Vparams = {
		w: "savenewmodule",
		courseid: %%GLOBAL_CourseEditCourseId%%,
		modulename: $("#EditRowCourseModuleName").val(),
		moduleorder: $(this).siblings('#EditRowCourseModuleOrder').val(),
		moduletype: $("#EditRowCourseModuleType").val(),
		moduleobjectid: $("#EditRowCourseModuleObjectId").val(),
		modulerequired: $("#EditRowCourseModuleRequired").is(":checked"),
		modulestatus: $("#EditRowCourseModuleStatus").val(),
	};
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		Vparams,
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowCourseModule");
			}
			else {
				var template = $('#tplModuleRow').html();
				var SavedRow = Mustache.render(template, data.module);
				$("tr.NewRowCourseModule").replaceWith(SavedRow);
			}
		}
	);
});

$('body').on('click', 'a.RowCourseModuleDelete', function(){
	if(confirm("%%LNG_WarnConfirmDeleteModule%%")){
		var Vrow = $(this).closest("tr");
		var Vmoduleid = Vrow.attr("moduleid");
		$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "deletemodule",
				moduleid: Vmoduleid
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					Vrow.remove();
					var VChilds = $("#CourseEditCourseModulesTable > tbody").children().length;
					if(VChilds == 0){
						var template = $('#tplCourseEditNoModulesFoundRow').html();
						var NewRow = Mustache.render(template);
						$("#CourseEditCourseModulesTable > tbody").append(NewRow);
					}
				}
			}
		);
	}		
});

$('body').on('click', 'a.RowCourseModuleEdit', function(){
	if(!resetAllRows()){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var Vmoduleid = Vrow.attr("moduleid");
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		{
			w: "getmodulerowedit",
			moduleid: Vmoduleid
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				var template = $('#tplModuleRowEdit').html();
				var tplData = {
						moduleid: data.module.moduleid,
						modulename: data.module.modulename,
						modulerequired_checked: data.module.moduleobjectrequired,
						moduleorder: data.module.moduleorder,
						moduleid: data.module.moduleid,
						object_options: data.objects
				};
				vModuletypeKey = "moduletype_selected"+data.module.moduletype;
				tplData[vModuletypeKey] = "selected=\"selected\"";
				
				vModulestatusKey = "modulestatus_selected"+data.module.modulestatus;
				tplData[vModulestatusKey] = "selected=\"selected\"";
				
				if(data.module.moduleobjectrequired == "1"){
					tplData["modulerequired_checked"] = "checked";
				}
				else {
					tplData["modulerequired_checked"] = "";
				}

				Mustache.parse(template);
				var vNewRow = Mustache.render(template, tplData);
				Vrow.replaceWith(vNewRow);
			}
		}
	);
});

$('body').on('click', 'a.RowCourseModuleAdd', function(){
	if(!resetAllRows()){
		return false;
	}
	
	//$('tr.EditRowCourseModule').remove();
	var template = $('#tplNewModuleRow').html();
	Mustache.parse(template);

	var tplData = {
			moduleorder: parseInt($(this).siblings("#RowCourseModuleOrder").val())+1,
		};
	var newRow = Mustache.render(template, tplData);
			
	$(this).parent().parent().after(newRow);
});

function resetRow(Vrow, Vmoduleid){
	DisableFormElements("EditRowCourseModule");
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		{
			w: "getmodulerow",
			moduleid: Vmoduleid
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowCourseModule");
				return false;
			}
			else {
				var template = $('#tplModuleRow').html();
				var tplData = {
						modulename: data.module.modulename,
						moduletype: data.module.moduletypetext,
						moduleobjectname: data.module.moduleobjectname,
						modulerequired: data.module.modulerobjectequiredtext,
						modulestatus: data.module.modulestatustext,
						moduleorder: data.module.moduleorder,
						moduleid: data.module.moduleid
				};

				Mustache.parse(template);
				var vNewRow = Mustache.render(template, tplData);
				Vrow.replaceWith(vNewRow);
				return true;
			}
		}
	);	
}

function resetAllRows(){
	if($(".RowModuleEdited").length > 0){
		if(!confirm("%%LNG_WarnLoseCurrentChanges%%")){
			return false;
		}
	}
	
	$(".NewRowCourseModule").remove();
	$(".EditRowCourseModule").each(function() {
		var Vmoduleid = $(this).find("#EditRowCourseModuleID").val();
		resetRow($(this), Vmoduleid);
	});
	
	return true;
}

$('body').on('click', 'a.EditRowCourseModuleCancel', function(){
	var Vrow = $(this).closest("tr");
	var Vmoduleid = $(this).siblings("#EditRowCourseModuleID").val();
	resetRow(Vrow, Vmoduleid)
});

$('body').on('click', 'a.EditRowCourseModuleSave', function(){
	var Vrow = $(this).closest("tr");
	DisableFormElements("EditRowCourseModule");
	var Vmoduleid = $(this).siblings("#EditRowCourseModuleID").val();
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		{
			w: "savemodule",
			moduleid: Vmoduleid,
			courseid: %%GLOBAL_CourseEditCourseId%%,
			modulename: $("#EditRowCourseModuleName").val(),
			moduleorder: $(this).siblings('#EditRowCourseModuleOrder').val(),
			moduletype: $("#EditRowCourseModuleType").val(),
			moduleobjectid: $("#EditRowCourseModuleObjectId").val(),
			modulerequired: $("#EditRowCourseModuleRequired").is(":checked"),
			modulestatus: $("#EditRowCourseModuleStatus").val()
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowCourseModule");
			}
			else {
				var template = $('#tplModuleRow').html();

				Mustache.parse(template);
				var vNewRow = Mustache.render(template, data.module);
				Vrow.replaceWith(vNewRow);
			}
		}
	);	
});

$('body').on('click', 'a.NewRowCourseModuleCancel', function(){
	$(this).parent().parent().remove();
});

// Administracion de Usuarios

$('body').on('click', 'a.CourseEditRemoveCourseUser', function(){
	var Vrow = $(this).closest("tr");
	var vUserId = Vrow.attr("coursemembersuserid");

	$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "deleteuserfromcourse",
				courseid: %%GLOBAL_CourseEditCourseId%%,
				userid: vUserId
			},
			function(data){
				if(data.success = 0){
					alert(data.msg);
				}
				else {
					Vrow.remove();
				}
			}
	);
});

$('body').on('change', '.CourseEditCurrentUserAccess', function(){
	var Vrow = $(this).closest("tr");
	var vUserId = Vrow.attr("coursemembersuserid");
	var vVal = $(this).val();

	$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "changeusercourseaccess",
				courseid: %%GLOBAL_CourseEditCourseId%%,
				userid: vUserId,
				course_user_access: vVal
			},
			function(data){
				if(data.success = 0){
					alert(data.msg);
				}
			}
	);
});

$('body').on('click', 'a#tab1', function(){
	$("#CourseEditCurrentUsers > tbody").empty();
	$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote/",
			{
				w: "getCourseUsers",
				courseid: %%GLOBAL_CourseEditCourseId%%
			},
			function(data){
				if(data.success = 0){
					alert(data.msg);
				}
				else {
					for(Vuser in data.users){
						var template = $('#tplCourseEditCurrentMembersRow').html();
						Mustache.parse(template);
						var tplData = {
								user: data.users[Vuser]
							};
						var newRow = Mustache.render(template, tplData);
						$("#CourseEditCurrentUsers > tbody").append(newRow);
					}
				}
			}
	);
});

$('body').on('click', '#CourseEditResetCourse', function(){
	if(confirm("%%LNG_WarnResetCourse%%")){
		$.getJSON(
			"%%GLOBAL_AppPath%%/course/remote",
			{
				w: "resetCourse",
				courseid: %%GLOBAL_CourseEditCourseId%%
			},
			function(data) {
				alert(data.msg);
			}
		);
	}
});

ShowTab(0);
</script>
</body>