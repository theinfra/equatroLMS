%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="VideoAdminActions">
%%GLOBAL_BreadcrumbsList%%
	<ul>
		<li><a href="%%GLOBAL_AppPath%%/video/create">%%LNG_VideoCreate%%</a></li>
	</ul>
</div>
<div class="VideoAdminVideoList">
	<table id="VideoAdminVideoListTable">
		<thead>
			<tr>
				<th>%%LNG_VideoId%%</th>
				<th>%%LNG_VideoTitle%%</th>
				<th>%%LNG_VideoThumbnail%%</th>
				<th>%%LNG_Action%%</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="4">%%LNG_LoadingTable%%</td>
			</tr>
		</tbody>
	</table>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
%%Mustache.VideoAdminListItem%%
<script lang="text/javascript">
$( document ).ready(function(){
	$.getJSON(
		"%%GLOBAL_AppPath%%/video/remote/",
		{
			w: "getVideos",
			q: "*"
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				$("#VideoAdminVideoListTable > tbody").empty();
				if("msg" in data){
					$("#VideoAdminVideoListTable > tbody").append("<tr><td colspan='4'>"+data.msg+"</td></tr>");
				}

				var template = $('#tplVideoAdminListItem').html();
				Mustache.parse(template);
				
				$.each(
					data.videos,
					function(index, value){
						var newRow = Mustache.render(template, value);
						$("#VideoAdminVideoListTable > tbody").append(newRow);
					}
				);
			}
		}
	);
});

$(function(){
	$('#VideoAdminVideoListTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});

$('body').on('click', 'a.VideoAdminDelete', function(){
	if(confirm("%%LNG_WarnDeleteVideo%%")){
		Vrow = $(this).closest("tr");
		$.getJSON(
			"%%GLOBAL_AppPath%%/video/remote",
			{
				w: "deleteVideo",
				videoid: Vrow.attr("videoid")
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					Vrow.remove();
				}
				return false;
			}
		);
	}
	return false;
});

</script>
</body>