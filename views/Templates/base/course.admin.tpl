%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="CourseAdminActions">
%%GLOBAL_BreadcrumbsList%%
	<ul>
		<li><a href="%%GLOBAL_AppPath%%/course/create">%%LNG_CourseCreate%%</a></li>
	</ul>
</div>
<div class="CourseAdminCourseList">
	%%GLOBAL_CourseAdminCourseList%%
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
$(function(){
	$('#CourseAdminTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});
</script>
</body>