%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
%%GLOBAL_BreadcrumbsList%%
	<h2>%%LNG_ExamSubmitSuccess%%</h2>
	<!-- ToDo: Agregar secciones aqui para mostrar la calificacion si asi esta configurado -->
	<h4><a href="%%GLOBAL_AppPath%%/">%%LNG_ReturnHome%%</a></h4>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>