%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<form id="ReportUserExamsForm" name="ReportUserExamsForm" method="GET" action="%%GLOBAL_AppPath%%/report/remote/">
	<input type="hidden" name="w" value="getUserExams" />
	<input type="hidden" name="format" value="excel" />
	<div class="ReportUserExamsActions">
		<ul id="ReporUserExamsActionsList" class="ReporUserExamsActionsList">
			<li>
				<span class="ReportUserExamsUserFilter ReportFilter">%%LNG_SearchUsers%%: </span>
				<input type="text" name="user_filter" id="ReporUserExamsUserFilter" size="20" value="" class="ReporUserExamsUserFilter FormField FormFieldText FormFieldText20" />
			</li>
			<li>
				<span class="ReportUserExamsUserMembershipFilter ReportFilter">%%LNG_SearchUsersByMembership%%: </span>
				<select name="membership_filter" id="ReportUserExamsUserMembershipFilter">
					%%GLOBAL_ReportUserExamsUserGroupOptions%%
				</select>
			</li>
			<li>
				<span class="ReportUserExamsDateFromFilter ReportFilter">%%LNG_From%%: </span>
				<input type="text" id="ReportUserExamsDateFromFilter" name="datefrom_filter" value="" class="ReportUserExamsDateFromFilter FormField FormFieldText FormFieldDatePicker" />
			</li>
			<li>
				<span class="ReportUserExamsDateToFilter ReportFilter">%%LNG_To%%: </span>
				<input type="text" id="ReportUserExamsDateToFilter" name="dateto_filter" value="" class="ReportUserExamsDateToFilter FormField FormFieldText FormFieldDatePicker" />
			</li>
		</ul>
		
		<ul>
			<li>
				<input type="submit" name="submit_excel" id="submit_excel" value="Exportar a Excel" />
			</li>
		</ul>
	</div>
</form>
<div class="ReportUserExams">
	<table id="ReportUserexamsTable" class="ReportUserexamsTable">
		<thead>
			<tr>
				<th>%%LNG_ExamName%%</th>
				<th>%%LNG_Username%%</th>
				<th>%%LNG_Name%%</th>
				<th>%%LNG_ExamTryNo%%</th>
				<th>%%LNG_Date%%</th>
				<th>%%LNG_ExamGrade%%</th>
				<th>%%LNG_Action%%</th>
			</tr>
		</thead>
		<tbody>
			<tr id="ReportUserExamsNoExamsFoundRow" style="display: none">
				<td colspan="6">%%LNG_NoExamsFound%%</td>
			<tr>
			<tr id="ReportUserExamsLoadingRow">
				<td colspan="6">%%LNG_LoadingList%%</td>
			<tr>
		</tbody>
	</table>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
%%Mustache.ExamUserItem%%
<script lang="text/javascript">
$( document ).ready(function(){
	$.getJSON(
			"%%GLOBAL_AppPath%%/report/remote/",
			{
				w: "getUserExams"
			},
			function(data){
				if($.isEmptyObject(data.user_exam)){
					$("#ReportUserExamsLoadingRow").remove();
					$("#ReportUserExamsNoExamsFoundRow").css("display", "table-row");
					return;
				}
				
				$("#ReportUserExamsLoadingRow").remove();
				$("#ReportUserExamsNoExamsFoundRow").css("display", "none");
				
				var template = $('#tplExamUserItem').html();
				Mustache.parse(template);
				for(Vitem in data.user_exam){
					data.user_exam[Vitem].grade = function() {
						return ( (this.correct_answers / this.total_questions).toFixed(2) ) * 100;
					};
					var newRow = Mustache.render(template, data.user_exam[Vitem]);
					$("#ReportUserexamsTable > tbody").append(newRow);
				}
			}
		);
});

$(".FormFieldDatepicker").datepicker({
	/*showOn: "button",*/
	buttonImage: "%%GLOBAL_AppPath%%/views/Styles/jquery-ui-datepicker/images/calendar.gif",
	buttonImageOnly: true,
	buttonText: "%%LNG_SelectDate%%",
	/*minDate: 0,*/
	dateFormat: 'dd-mm-yy'
});

function searchUserExams(){
	$.getJSON(
			"%%GLOBAL_AppPath%%/report/remote/",
			{
				w: "getUserExams",
				user_filter: $("#ReporUserExamsUserFilter").val(),
				membership_filter: $("#ReportUserExamsUserMembershipFilter").val(),
				datefrom_filter: $('#ReportUserExamsDateFromFilter').val(),
				dateto_filter: $('#ReportUserExamsDateToFilter').val(),
			},
			function(data) {
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					$("#ReportUserexamsTable > tbody").children(":not(#ReportUserExamsNoExamsFoundRow)").remove();
					if($.isEmptyObject(data.user_exam)){
						$("#ReportUserExamsNoExamsFoundRow").css("display", "table-row");
						return;
					}
					
					$("#ReportUserExamsNoExamsFoundRow").css("display", "none");
					
					var template = $('#tplExamUserItem').html();
					Mustache.parse(template);
					for(Vitem in data.user_exam){
						var newRow = Mustache.render(template, data.user_exam[Vitem]);
						$("#ReportUserexamsTable > tbody").append(newRow);
					}
				}
			}
		);
}

;(function($){
    $.fn.extend({
        donetyping: function(callback,timeout){
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function(el){
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function(i,el){
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste',function(e){
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type=='keyup' && e.keyCode!=8) return;
                    
                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function(){
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur',function(){
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });
})(jQuery);

	
	$('#ReporUserExamsUserFilter').donetyping(function(){	
		searchUserExams();
		});
	
$('body').on('change', '#ReportUserExamsUserMembershipFilter, #ReportUserExamsDateFromFilter, #ReportUserExamsDateToFilter', function(){
	searchUserExams();
});

</script>
</body>