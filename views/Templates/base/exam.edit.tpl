%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="ExamEdit">
%%GLOBAL_BreadcrumbsList%%
	<ul id="tabnav">
		<li><a href="#" class="active" id="tab0" onclick="ShowTab(0)">%%LNG_ExamDetails%%</a></li>
		<li><a href="#" class="active" id="tab1" onclick="ShowTab(1)">%%LNG_ExamQuestions%%</a></li>
	</ul>
	<div id="div0" class="ExamEditExamDetails">
		<form action="%%GLOBAL_AppPath%%/exam/editsubmit" method="POST" id="ExamEditForm">
			<input type="hidden" name="ExamEditExamId" id="ExamEditExamId" value="%%GLOBAL_ExamEditExamId%%" />
			<table>
				<tr>
					<td>%%LNG_ExamName%%</td>
					<td><input type="text" id="ExamEditExamName" name="ExamEditExamName" value="%%GLOBAL_ExamEditExamName%%" class="ExamEditExamName FormField FormFieldText" /></td>
				</tr>
				<tr>
					<td>%%LNG_ExamTitle%%</td>
					<td><input type="text" id="ExamEditExamTitle" name="ExamEditExamTitle" value="%%GLOBAL_ExamEditExamTitle%%" class="ExamEditExamTitle FormField FormFieldText" /></td>
				</tr>
				<tr>
					<td>%%LNG_ExamDesc%%</td>
					<td><textarea rows="10" cols="80" id="ExamEditExamDesc" name="ExamEditExamDesc" class="ExamEditExamDesc FormField FormFieldTextarea">%%GLOBAL_ExamEditExamDesc%%</textarea></td>
				</tr>
				<tr>
					<td>%%LNG_ExamDateStart%%</td>
					<td><input type="text" id="ExamEditDateStart" name="ExamEditDateStart" value="%%GLOBAL_ExamEditDateStart%%" class="ExamEditDateStart FormField FormFieldText FormFieldDatepicker" /></td>
				</tr>
				<tr>
					<td>%%LNG_ExamDateEnd%%</td>
					<td><input type="text" id="ExamEditDateEnd" name="ExamEditDateEnd" value="%%GLOBAL_ExamEditDateEnd%%" class="ExamEditDateEnd FormField FormFieldText FormFieldDatepicker" /></td>
				</tr>
				<tr>
					<td>%%LNG_ExamTriesPerUser%%</td>
					<td><select id="ExamEditTriesPerUser" name="ExamEditTriesPerUser" class="ExamEditTriesPerUser FormField FormFieldSelect">
						<option %%GLOBAL_ExamEditTriesPerUser0selected%% value="0">%%LNG_Unlimited%%</option>
						<option %%GLOBAL_ExamEditTriesPerUser1selected%% value="1">1</option>
						<option %%GLOBAL_ExamEditTriesPerUser2selected%% value="2">2</option>
						<option %%GLOBAL_ExamEditTriesPerUser3selected%% value="3">3</option>
						<option %%GLOBAL_ExamEditTriesPerUser4selected%% value="4">4</option>
						<option %%GLOBAL_ExamEditTriesPerUser5selected%% value="5">5</option>
						<option %%GLOBAL_ExamEditTriesPerUser6selected%% value="6">6</option>
						<option %%GLOBAL_ExamEditTriesPerUser7selected%% value="7">7</option>
						<option %%GLOBAL_ExamEditTriesPerUser8selected%% value="8">8</option>
						<option %%GLOBAL_ExamEditTriesPerUser9selected%% value="9">9</option>
						<option %%GLOBAL_ExamEditTriesPerUser10selected%% value="10">10</option>
						<option %%GLOBAL_ExamEditTriesPerUser11selected%% value="11">11</option>
						<option %%GLOBAL_ExamEditTriesPerUser12selected%% value="12">12</option>
						<option %%GLOBAL_ExamEditTriesPerUser13selected%% value="13">13</option>
						<option %%GLOBAL_ExamEditTriesPerUser14selected%% value="14">14</option>
						<option %%GLOBAL_ExamEditTriesPerUser15selected%% value="15">15</option>
						<option %%GLOBAL_ExamEditTriesPerUser16selected%% value="16">16</option>
						<option %%GLOBAL_ExamEditTriesPerUser17selected%% value="17">17</option>
						<option %%GLOBAL_ExamEditTriesPerUser18selected%% value="18">18</option>
						<option %%GLOBAL_ExamEditTriesPerUser19selected%% value="19">19</option>
						<option %%GLOBAL_ExamEditTriesPerUser20selected%% value="20">20</option>
					</select></td>
				</tr>
				<tr>
					<td>%%LNG_ExamShowAnswers%%</td>
					<td><select name="ExamEditShowAnswers" id="ExamEditShowAnswers">
							<option value="no" %%GLOBAL_ExamEditShowAnswersnoselected%%>%%LNG_LangNo%%</option>
							<option value="question" %%GLOBAL_ExamEditShowAnswersquestionselected%%>%%LNG_ExamShowAnswersPerQuestion%%</option>
							<option value="end" %%GLOBAL_ExamEditShowAnswersenselected%%>%%LNG_ExamShowAnswersEnd%%</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>%%LNG_ExamPassingGrade%%</td>
					<td>
						<input type="button" name="ExamEditPassingGradeSub5" id="ExamEditPassingGradeSub5" value="&#8609;" class="ExamEditPassingGradeSub5 FormField FormFieldButton" />
						<input type="button" name="ExamEditPassingGradeSub1" id="ExamEditPassingGradeSub1" value="&#8595;"  class="ExamEditPassingGradeSub1 FormField FormFieldButton" />
						<input type="text" name="ExamEditPassingGrade" id="ExamEditPassingGrade" value="%%GLOBAL_ExamEditPassingGrade%%" size="3"  class="ExamEditPassingGrade FormField FormFieldButton" onkeypress="return isNumber(event)" />
						<input type="button" name="ExamEditPassingGradeAdd1" id="ExamEditPassingGradeAdd1" value="&#8593;"  class="ExamEditPassingGradeAdd1 FormField FormFieldButton" />
						<input type="button" name="ExamEditPassingGradeAdd5" id="ExamEditPassingGradeAdd5" value="&#8607;"  class="ExamEditPassingGradeAdd5 FormField FormFieldButton" />
					</td>
				</tr>
				<tr>
					<td>%%LNG_ExamDuration%%</td>
					<td>
						<input type="text" id="ExamEditExamDurationQuantity" name="ExamEditExamDurationQuantity" value="%%GLOBAL_ExamEditExamDurationQuantity%%" class="ExamEditExamDurationQuantity FormField FormFieldText" />
						<select id="ExamEditExamDurationUnit" name="ExamEditExamDurationUnit">
							<option value="seconds" %%GLOBAL_ExamEditExamDurationUnitsecondsselected%%>%%LNG_Seconds%%</option>
							<option value="minutes" %%GLOBAL_ExamEditExamDurationUnitminutesselected%%>%%LNG_Minutes%%</option>
							<option value="hours" %%GLOBAL_ExamEditExamDurationUnithoursselected%%>%%LNG_Hours%%</option>
							<option value="days" %%GLOBAL_ExamEditExamDurationUnitdaysselected%%>%%LNG_Days%%</option>
						</select>
				</tr>
			</table>
	</div>
	<div id="div1" class="ExamEditQuestionList">
		<table id="ExamEditQuestionOptions">
			<tr>
				<td>%%LNG_ExamNumQuestions%%:</td>
				<td> 
					<input type="button" name="ExamEditNumQuestionsSub1" id="ExamEditNumQuestionsSub1" value="&#8595;"  class="ExamEditNumQuestionsSub1 FormField FormFieldButton" />
					<input type="text" name="ExamEditNumQuestions" id="ExamEditNumQuestions" value="%%GLOBAL_ExamEditNumQuestions%%" size="3"  class="ExamEditNumQuestions FormField FormFieldButton" onkeypress="return isNumber(event)" />
					<input type="button" name="ExamEditNumQuestionsAdd1" id="ExamEditNumQuestionsAdd1" value="&#8593;"  class="ExamEditNumQuestionsAdd1 FormField FormFieldButton" />
				</td>
			</tr>
			<tr>
				<td>%%LNG_ExamQuestionOrder%%:</td>
				<td>
					<select name="ExamEditQuestionOrder" id="ExamEditQuestionOrder">
						<option value="defined" %%GLOBAL_ExamEditQuestionOrderdefinedselected%%>%%LNG_AsDefined%%</option>
						<option value="random" %%GLOBAL_ExamEditQuestionOrderrandomselected%%>%%LNG_Random%%</option>
					</select>
				</td>
			</tr>
		</table>
		<table id="ExamEditQuestionListTable">
			<thead>
				<tr>
					<th>%%LNG_QuestionText%%</th>
					<th width="65px">%%LNG_Answers%%</th>
					<th width="65px">%%LNG_Action%%</th>
				</tr>
			</thead>
			<tbody>
				%%GLOBAL_ExamEditQuestionsTable%%
			</tbody>
		</table>
	</div>
	<table style="width: 80%">
				<tr>
					<td><input type="reset" name="ExamEditReset" id="ExamEditReset" value="%%LNG_Reset%%" /></td>
					<td><input type="submit" name="ExamEditSubmit" id="ExamEditSubmit" value="%%LNG_Submit%%" /></td>
				</tr>
	</table>
	</form>
</div>

%%Mustache.NewQuestionRow%%
%%Mustache.QuestionRow%%
%%Mustache.QuestionRowEdit%%
%%Mustache.AnswersTableRow%%

<script src="%%GLOBAL_AppPath%%/javascript/jquery-ui-datepicker.min.js"></script>
<script lang="text/javascript">

function resetRow(Vrow, Vquestion){
	DisableFormElements("EditRowExamQuestion");
	$.getJSON(
		"%%GLOBAL_AppPath%%/exam/remote/",
		{
			w: "getquestionrow",
			questionid: Vquestion
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowExamQuestion");
			}
			else {
				var template = $('#tplQuestionRow').html();

				Mustache.parse(template);
				var vNewRow = Mustache.render(template, tplData.question);
				Vrow.replaceWith(vNewRow);
			}
		}
	);	
}

$(".FormFieldDatepicker").datepicker({
			showOn: "button",
	    	buttonImage: "%%GLOBAL_AppPath%%/views/Styles/jquery-ui-datepicker/images/calendar.gif",
	    	buttonImageOnly: true,
	    	buttonText: "Select date",
	    	minDate: 0
	});
	
$("#ExamEditDateStart").change(function(){
	var VstartDate = $("#ExamEditDateStart").datepicker("getDate");
	$("#ExamEditDateEnd").datepicker("option", "minDate", VstartDate);
});

$('body').on('click', '.ExamEditQuestionList .CourseEditAddFirstQuestion a', function(){
	var template = $('#tplNewQuestionRow').html();
	Mustache.parse(template);
	var tplData = {
			moduleorder: 0,
		};
	var newRow = Mustache.render(template, tplData);
			
	$("#ExamEditQuestionListTable tr:last").replaceWith(newRow);
	$(this).parent().parent().remove();
});

$('body').on('change', '.FormField', function(){
	$(this).closest("tr:not(.RowQuestionEdited)").addClass("RowQuestionEdited");
});

$('body').on('click', 'a.NewRowExamQuestionSave', function(){
	DisableFormElements("NewRowExamQuestion");

	var Vparams = {
		w: "savenewquestion",
		examid: %%GLOBAL_ExamEditExamId%%,
		questiontext: $("#EditRowExamQuestionText").val(),
	};
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/exam/remote/",
		Vparams,
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("NewRowExamQuestion");
			}
			else {
				var template = $('#tplQuestionRow').html();
				var SavedRow = Mustache.render(template, data.question);
				$("tr.NewRowExamQuestion").replaceWith(SavedRow);
			}
		}
	);
});

function resetRow(Vrow, Vquestionid){
	DisableFormElements("EditRowQuestion");
	$.getJSON(
		"%%GLOBAL_AppPath%%/exam/remote/",
		{
			w: "getQuestionRow",
			questionid: Vquestionid
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowQuestion");
			}
			else {
				var template = $('#tplQuestionRow').html();
				Mustache.parse(template);
				var vNewRow = Mustache.render(template, data.question);
				Vrow.replaceWith(vNewRow);
			}
		}
	);	
}

function resetAllRows(){
	if($(".RowQuestionEdited").length > 0){
		if(!confirm("%%LNG_WarnLoseCurrentChanges%%")){
			return false;
		}
	}
	
	$(".NewRowExamQuestion").remove();
	$(".EditRowQuestion").each(function() {
		var Vquestionid = $(this).find("#EditRowExamQuestionId").val();
		resetRow($(this), Vquestionid);
	});
}

$('body').on('change', '.FormField', function(){
	$(this).closest("tr:not(.RowQuestionEdited)").addClass("RowQuestionEdited");
});

$('body').on('click', 'a.RowExamQuestionEdit', function(){
	// Esta muy complicado esto de hacerlo todo asincrono, por el momento voy a mandar a question/edit y luego lo quito
	return true;
	if(!resetAllRows()){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var VquestionId = Vrow.attr("questionid");

	$.getJSON(
			"%%GLOBAL_AppPath%%/exam/remote/",
			{
				w: "getQuestionRow",
				questionid: VquestionId
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					var template = $('#tplQuestionRowEdit').html();
					var SavedRow = Mustache.render(template, data.question);
					Vrow.replaceWith(SavedRow);
				}
			}
	);
});

$('body').on('click', 'a.RowExamQuestionAdd', function(){
	var Vrow = $(this).closest("tr");
	var Vorder = Vrow.find("#RowExamQuestionOrder").val();
	var template = $('#tplNewQuestionRow').html();
	Mustache.parse(template);
	var tplData = {
			moduleorder: Vorder++,
		};
	var newRow = Mustache.render(template, tplData);
			
	Vrow.after(newRow);
});

$('body').on('click', 'a.EditRowQuestionCancel', function(){
	var Vrow = $(this).closest("tr");
	var Vquestion = $(this).siblings("#EditRowExamQuestionId").val();
	resetRow(Vrow, Vquestion)
});

$('body').on('click', 'a.EditRowQuestionSave', function(){
	var Vrow = $(this).closest("tr");
	DisableFormElements("EditRowQuestion");
	var Vquestionid = $(this).siblings("#EditRowExamQuestionId").val();
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/exam/remote/",
		{
			w: "editQuestion",
			questionid: Vquestionid,
			questiontext: $("#EditRowQuestionText").val()
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
				EnableFormElements("EditRowQuestion");
			}
			else {
				var template = $('#tplQuestionRow').html();
				Mustache.parse(template);
				var vNewRow = Mustache.render(template, data.question);
				Vrow.replaceWith(vNewRow);
			}
		}
	);	
});

$('body').on('click', 'a.EditRowQuestionEditAnswers', function(){
	// Esta muy complicado esto de hacerlo todo asincrono, por el momento voy a mandar a question/edit y luego lo quito
	return true;
	
	VquestionId = $(this).closest("tr").attr("questionid");
	var Vrow = $(this).closest("tr");

	$.getJSON(
			"%%GLOBAL_AppPath%%/exam/remote/",
			{
				w: "getAnswers",
				questionid: VquestionId
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
				}
				else {
					var template = $('#tplAnswersTableRow').html();
					Mustache.parse(template);
					var vNewRow = Mustache.render(template, data);
					Vrow.after(vNewRow);
				}
			}
	);
});

$("#ExamEditQuestionListTable").children("tbody").sortable({
	items: "> tr",
	axis: "y",
	update: function(event, ui){
		var Vserialized = "";
		var Vindex = 0;
		$("#ExamEditQuestionListTable tr.RowExamQuestionList").each(function(){
			Vserialized += $(this).attr("questionid") + "=" + Vindex+ "&";
			$(this).find("#RowExamQuestionOrder").val(Vindex);
			Vindex++;
		});
		$.getJSON(
			"%%GLOBAL_AppPath%%/exam/remote/",
			{
				w: "saveQuestionsOrder",
				serialized: Vserialized
			},
			function(data){
				if(data.success == 0){
					alert(data.msg);
					return;
				}
			}
		);
	}
});

$('body').on('click', '#ExamEditPassingGradeSub5', function(){
	var Vval = parseInt($("#ExamEditPassingGrade").val());
	var Vinput = $("#ExamEditPassingGrade");
	if(Vval > 5){
		Vinput.val(Vval-5);
	}
	else if (Vval <= 5){
		Vinput.val(0);
	}
});

$('body').on('click', '#ExamEditPassingGradeSub1', function(){
	var Vval = parseInt($("#ExamEditPassingGrade").val());
	var Vinput = $("#ExamEditPassingGrade");
	if(Vval > 0){
		Vinput.val(Vval-1);
	}
	else{
		Vinput.val(0);
	}
});

$('body').on('click', '#ExamEditPassingGradeAdd1', function(){
	var Vval = parseInt($("#ExamEditPassingGrade").val());
	var Vinput = $("#ExamEditPassingGrade");
	if(Vval < 100){
		Vinput.val(Vval+1);
	}
	else{
		Vinput.val(100);
	}
});

$('body').on('click', '#ExamEditPassingGradeAdd5', function(){
	var Vval = parseInt($("#ExamEditPassingGrade").val());
	var Vinput = $("#ExamEditPassingGrade");
	if(Vval < 95){
		Vinput.val(Vval+5);
	}
	else if (Vval >= 95) {
		Vinput.val(100);
	}
});

$('body').on('click', '#ExamEditNumQuestionsSub1', function(){
	var Vval = parseInt($("#ExamEditNumQuestions").val());
	var Vinput = $("#ExamEditNumQuestions");
	if(Vval > 0){
		Vinput.val(Vval-1);
	}
	else{
		Vinput.val(0);
	}
});

$('body').on('click', '#ExamEditNumQuestionsAdd1', function(){
	var Vval = parseInt($("#ExamEditNumQuestions").val());
	var vNumQuestions = $(".RowExamQuestionList").size();
	var Vinput = $("#ExamEditNumQuestions");
	if(Vval < vNumQuestions){
		Vinput.val(Vval+1);
	}
	else{
		Vinput.val(vNumQuestions);
	}
});

$('body').on('click', 'a.RowExamQuestionDelete', function(){
	if(!confirm("%%LNG_ConfirmDeleteElement%%")){
		return false;
	}
	
	var Vrow = $(this).closest("tr");
	var VquestionId = Vrow.attr("questionid");
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/question/remote/",
		{
			w: "deleteQuestion",
			questionid: VquestionId
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				Vrow.remove();
			}
		}
	);
});

ShowTab(0);
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>