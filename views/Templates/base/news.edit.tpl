%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="NewsEdit">
%%GLOBAL_BreadcrumbsList%%
	<form action="%%GLOBAL_AppPath%%/news/editsubmit" method="POST" id="NewsEditForm">
		<input type="hidden" name="NewsEditNewsId" id="NewsEditNewsId" value="%%GLOBAL_NewsEditNewsId%%" />
		<table>
			<tr>
				<td>%%LNG_NewsTitle%%</td>
				<td><input type="text" id="NewsEditNewsTitle" name="NewsEditNewsTitle" value="%%GLOBAL_NewsEditNewsTitle%%" class="NewsEditNewsTitle FormField FormFieldText" /></td>
			</tr>
			<tr>
				<td>%%LNG_NewsContent%%</td>
				<td><textarea id="NewsEditNewsContent" name="NewsEditNewsContent" class="NewsEditNewsContent FormField FormFieldText" rows="40" cols="120">%%GLOBAL_NewsEditNewsContent%%</textarea></td>
			</tr>
			<tr>
				<td>%%LNG_NewsStatus%%</td>
				<td><select name="NewsEditNewsStatus" id="NewsEditNewsStatus">
					<option %%GLOBAL_NewsEditNewsStatus0selected%% value="0">%%LNG_Inactive%%</option>
					<option %%GLOBAL_NewsEditNewsStatus1selected%% value="1">%%LNG_Active%%</option>
				</select>
			</tr>
			<tr>
				<td><input type="reset" name="NewsEditReset" id="NewsEditReset" value="%%LNG_Reset%%" /></td>
				<td><input type="submit" name="NewsEditSubmit" id="NewsEditSubmit" value="%%LNG_Submit%%" /></td>
			</tr>
		</table>
	</form>
</div>
<script lang="text/javascript">
tinymce.init({
    selector: "textarea",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    language: 'es'
});
</script>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
</body>