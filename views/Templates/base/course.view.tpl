%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="CourseView">
%%GLOBAL_BreadcrumbsList%%
	<h1>%%GLOBAL_CourseViewCourseName%%</h1>
	<h3>%%LNG_CourseStaff%%</h3>
	<ul class="CourseViewCourseStaff">
		%%GLOBAL_CourseViewCourseStaffList%%
	</ul>
	<div class="CourseViewCourseModules">
		<div id="CourseViewCourseModuleList" class="CourseViewCourseModules">
			<p>%%LNG_LoadingCourseModules%%</p>
		</div>
	</div>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
%%Mustache.CourseModuleListItem%%
%%Mustache.CourseModuleListItemDetails%%
%%Mustache.CourseModuleListItemDetailsfile%%
%%Mustache.CourseModuleListItemDetailsvideo%%
%%Mustache.CourseModuleListItemDetailsexam%%
<script lang="text/javascript">
$( document ).ready(function(){
	$("#CourseViewCourseModuleList").empty();
	$.getJSON(
		"%%GLOBAL_AppPath%%/course/remote/",
		{
			w: "getModules",
			courseid: %%GLOBAL_CourseViewCourseId%%
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				var template = $('#tplCourseModuleListItem').html();
				Mustache.parse(template);
				for(Vmodule in data.modules){
					var tplData = {
							module: data.modules[Vmodule]
						};
					var newRow = Mustache.render(template, tplData);
					$("#CourseViewCourseModuleList").append(newRow);
				}
			}
		}
	);
});

$('body').on('click touchstart', '.CourseModule', function(){
	var VmoduleId = $(this).attr("moduleid");
	var Vchild = $(this).find(".CourseModuleDetails");
	
	$.getJSON(
		"%%GLOBAL_AppPath%%/coursemodule/remote/",
		{
			w: "getModuleDetails",
			moduleid: VmoduleId
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				var template = $('#tplCourseModuleListItemDetails').html();
				Mustache.parse(template);
				
				var detailsTemplate = $('#tplCourseModuleListItemDetails'+data.coursemodule.moduletypesmalltext).html();
				Mustache.parse(detailsTemplate);
				
				var tplData = {
						coursemodule: data.coursemodule,
						module: data.module
				};
				
				var newDiv = Mustache.render(template, tplData, {
						moduledetails: detailsTemplate
				});
				
				$(".CourseModuleDetails").empty();
				Vchild.append(newDiv);
				Vchild.show();
			}
		}
	);
});
</script>
</body>