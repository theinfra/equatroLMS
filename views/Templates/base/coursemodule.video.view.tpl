%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="VideoViewContent">
%%GLOBAL_BreadcrumbsList%%
	<h1>%%GLOBAL_VideoViewVideoTitle%%</h1>
	<div class="VideoEmbed">
		<div class="VideoPlayerContainer" id="VideoPlayerContainer" style="pointer-events: none">
			<div class="VideoOverlay"><img src="http://img.youtube.com/vi/%%GLOBAL_VideoViewVideoExternalId%%/maxresdefault.jpg" /></div>
			<iframe id="EmbedVideo" width="100%" height="720" src="https://www.youtube.com/embed//%%GLOBAL_VideoViewVideoExternalId%%?autohide=1&fs=0&modestbranding=1&rel=0&showinfo=0&controls=0&enablejsapi=1&html5=1" frameborder="0" webkitAllowFullScreen mozAllowFullScreen allowFullScreen></iframe>
		</div>
		<div class="VideoControls">
			<div class="VideoButtons">
				<div id="rewind-button" class="RewindButton"></div>
				<div id="play-button" class="PlayButton"></div>
				<div id="forward-button" class="ForwardButton"></div>
			</div>
			<div id="VideoProgressBarOutside">
				<div id="VideoProgressBarInside"></div>
			</div>
			<div id="player-status">
				<span id="VideoStatusCurrent">-:--</span><span>/</span><span id="VideoStatusTotal">-:--</span>
			</div>
		</div>
	</div>
	<p>%%GLOBAL_VideoViewVideoDesc%%</p>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
<script lang="text/javascript">
var tag = document.createElement('script');
tag.src = "http://www.youtube.com/player_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player, vInterval;

function onYouTubePlayerAPIReady() {
	  // create the global player from the specific iframe (#video)
	  player = new YT.Player('EmbedVideo', {
        width: 853,
        height: 480,
        videoId: '%%GLOBAL_VideoViewVideoExternalId%%',
	    events: {
	      // call this function when player is ready to use
	      'onReady': onPlayerReady
	    }
	  });
	}

function startPlaybackStatusInterval(){
	vInterval = window.setInterval(function () {
		var vCurtime = roundUp(player.getCurrentTime(), 0);
		$("#VideoStatusCurrent").html(""+formatTimeMinSec(vCurtime));
	}, 500);
}

function playToggle(){
    var Vstate = player.getPlayerState();
    
    if(Vstate == 0 || Vstate == 2 || Vstate == 3 || Vstate == 5){
    	player.playVideo();
        setTimeout(function () {
        	$('#play-button').closest(".VideoControls").siblings(".VideoPlayerContainer").children(".VideoOverlay").remove();
        }, 3000);
    	$('#play-button').removeClass("PlayButton");
    	$('#play-button').addClass("PauseButton");
    	
    	if(vInterval === undefined || vInterval === false){
    		startPlaybackStatusInterval();
    	}
    }
    
    if(Vstate == 1){
    	player.pauseVideo();
    	$('#play-button').removeClass("PauseButton");
    	$('#play-button').addClass("PlayButton");
    	
    	window.clearInterval(vInterval);
    	vInterval = false;
    }
};

function playerS(vTime){
	
}

function onPlayerReady(event) {
	  
	$('#play-button').on('click', function () {
		playToggle();
	});
	
	$('#rewind-button').on('click', function () {
		var Vcurrenttime = player.getCurrentTime();
		if(Vcurrenttime < 5){
			Vcurrenttime = 0;
		}
		else {
			Vcurrenttime = Vcurrenttime - 5;
		}
		player.seekTo(Vcurrenttime, true);
	});
	
	$('#forward-button').on('click', function () {
		var Vcurrenttime = player.getCurrentTime();
		player.seekTo(Vcurrenttime + 5, true);
	});
	
	var duration = player.getDuration();
	$('#VideoStatusTotal').html(""+formatTimeMinSec(duration));
	
	$("#VideoProgressBarOutside").click(function(e) {
			var pct = Math.floor((e.offsetX / this.offsetWidth) * 100);
			var seconds = roundUp(duration * (pct / 100), 0);
			player.seekTo(seconds, true);

			$("#VideoProgressBarInside").css("width", e.offsetX + "px");
	});
}
</script>
</body>