%%Panel.HTMLHead%%
<body>
<div id="Container">
%%Panel.LeftColumn%%
<div class="WideContent">
<div class="ExamViewContent">
%%GLOBAL_BreadcrumbsList%%
	<h1>%%GLOBAL_ExamViewExamTitle%%</h1>
	<p>%%GLOBAL_ExamViewExamDesc%%</p>
	<div class="ExamViewSubmitHistory">
		<h3>%%LNG_ExamSubmitHistory%%</h3>
		<h4>%%LNG_ExamSubmitHistoryDetails%%</h4>
		<table id="ExamViewSubmitHistoryTable" class="ExamViewSubmitHistoryTable">
			<thead>
				<tr>
					<th>%%LNG_ExamTryNo%%</th>
					<th>%%LNG_ExamSubmitDate%%</th>
					<th>%%LNG_ExamCorrectAnswers%%</th>
					<th>%%LNG_ExamGrade%%</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="">
						%%LNG_LoadingTable%%
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<a href="%%GLOBAL_AppPath%%/exam/solve/%%GLOBAL_ExamViewExamId%%">%%LNG_BeginExam%%</a>
</div>
</div><!-- WideContent -->
%%Panel.Footer%%
</div><!-- Container -->
%%Mustache.ExamViewSubmitHistoryRow%%
%%Mustache.ExamViewSubmitHistoryRowEmpty%%
<script lang="text/javascript">
$( document ).ready(function(){
	$("#ExamViewSubmitHistoryTable > tbody").empty();
	$.getJSON(
		"%%GLOBAL_AppPath%%/exam/remote/",
		{
			w: "getUserExamSubmits",
			examid: %%GLOBAL_ExamViewExamId%%
		},
		function(data){
			if(data.success == 0){
				alert(data.msg);
			}
			else {
				var template = $('#tplExamViewSubmitHistoryRow').html();
				Mustache.parse(template);
				
				if($.isEmptyObject(data.results)){
					var templateEmpty = $('#tplExamViewSubmitHistoryRowEmpty').html();
					Mustache.parse(template);
					var newRow = Mustache.render(templateEmpty);
					$("#ExamViewSubmitHistoryTable > tbody").append(newRow);
				}
				else {
					for(Vrow in data.results){
						var tplData = data.results[Vrow];
						var newRow = Mustache.render(template, tplData);
						
						$("#ExamViewSubmitHistoryTable > tbody").append(newRow);
					}
				}
			}
		}
	);
});

$(function(){
	$('#ExamViewSubmitHistoryTable').tablesorter({
		widgets        : ['zebra', 'columns'],
		usNumberFormat : false,
		sortReset      : true,
		sortRestart    : true,
		theme : 'blue'
	});
});
</script>
</body>