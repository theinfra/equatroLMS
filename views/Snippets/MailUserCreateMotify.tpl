<h1>%%LNG_NewUserCreated%%</h1>

<p>%%LNG_NewUserCreatedText%%</p>

<ul>
	<li>%%LNG_URL%%: <a href="%%GLOBAL_AppPath%%">%%GLOBAL_AppPath%%</a></li>
	<li>%%LNG_Username%%: %%GLOBAL_Username%%</li>
	<li>%%LNG_Password%%: %%GLOBAL_Password%%</li>
</ul>

