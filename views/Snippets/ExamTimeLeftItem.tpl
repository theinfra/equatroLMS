ExamId %%GLOBAL_ExamTimeLeftExamId%%:<span id="ExamTimeLeftTimerExamId%%GLOBAL_ExamTimeLeftExamId%%" class="ExamTimeLeftTimer"></span>
<script lang="text/javascript">
$(function(){
	$('#ExamTimeLeftTimerExamId%%GLOBAL_ExamTimeLeftExamId%%').countdowntimer({
		timeUp: "alertExamTime",
		borderColor: "#000000",
		dateAndTime: "%%GLOBAL_ExamEndTime%%"
	});
});
</script>
