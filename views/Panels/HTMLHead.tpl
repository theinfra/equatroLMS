<head>
<title>%%GLOBAL_TemplateTitle%%</title>

<link rel="SHORTCUT ICON" href="%%GLOBAL_AppPath%%/images/favicon.ico">

<link rel="stylesheet" href="%%GLOBAL_AppPath%%/views/Styles/enginestyle.css" />
<link rel="stylesheet" href="%%GLOBAL_AppPath%%/views/Styles/basestyle.css" />
%%GLOBAL_ViewStylesheet%%
<script src="%%GLOBAL_AppPath%%/javascript/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="%%GLOBAL_AppPath%%/views/Styles/jquery-countdown/jquery.countdownTimer.css">
%%GLOBAL_ViewScripts%%
<script src="%%GLOBAL_AppPath%%/javascript/mustache.min.js"></script>
<script src="%%GLOBAL_AppPath%%/javascript/views.js"></script>

<link rel="stylesheet" href="%%GLOBAL_AppPath%%/views/Styles/jquery-tablesorter/theme.default.min.css">
<script src="%%GLOBAL_AppPath%%/javascript/jquery-tablesorter/jquery.tablesorter.min.js"></script>
</head>