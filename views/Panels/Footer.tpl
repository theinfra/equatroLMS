<br class="Clear" />
<div class="Footer">
	<div class="Left">
		<p>Dudas y Reporte de errores: <a href="mailto:soporte@equatro.net">soporte@equatro.net</a></p>	
	</div>
	<div class="Center">
		<p><a href="%%GLOBAL_AppPath%%/privacidad">%%LNG_PrivacyNotice%%</a></p>
	</div>
	<div class="Right">
		<p>&copy; e-quatro</p>
		<!-- <p><a href="http://equatro.net">http://equatro.net</a> -->
	</div>
</div>