<div class="LeftColumn">
	<div class="LeftColumnLogo">
		<img src="%%GLOBAL_AppPath%%/images/%%GLOBAL_AppLogoFilename%%" />
	</div>
	%%Panel.MainMenuVertical%%
	<div class="InfoLinks">
		<p>%%LNG_SendQuestionsAndErrorsTo%%: <a href="mailto:soporte@equatro.net">soporte@equatro.net</a></p>
		<p><a href="%%GLOBAL_AppPath%%/privacidad">%%LNG_PrivacyNotice%%</a></p>
	</div>
</div>
%%Panel.LeftColumnFlashMessages%%