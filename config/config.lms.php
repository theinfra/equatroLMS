<?php

$GLOBALS['APP_CONFIG']["MainMenu"] = array(
		"Home" => "index",
		"News" => "news",
		"Catalogs" => array(
			"Courses" => "course/admin",
			"Exams" => "exam/admin",
			"Videos" => "video/admin",
			"News" => "news/admin",
			"Files" => "file/admin",
		),
		"Admin" => array(
			"Users" => "user/admin",
			"UserGroups" => "usergroup/admin",
			"Logs" => "log",
		),
		"Reports" => array(
			"ReportUsersExams" => "report/userexams"	
		),
		"Contact" => "contact",
		"LogInOut" => "user/login",
);

/*
 * INSERT INTO `user` (`firstname`, `lastname`, `mail`, `username`, `password`, `salt`, `phone`, `status`, `membershiptype`, `usergroup`) VALUES ('Admin', 'Admin', 'admin@localhost', 'admin', '5d38559629b3a4241e77d09699e4fe8f5580eebed5a18593f97e053c40f58183edda8a2de0dcec1e9fbeb7a65b6139a583d8599022f10640489632b088a749d1', '3a287ab2b87e0e99', '0', 2, 0, 3);
*/