<?php

class PANEL_MAINMENUVERTICAL extends AppPanel {
	
	public function SetPanelSettings(){
		$user = getUserData();
		if(!$user){
			$GLOBALS["ShowUserMenu"] = "display: none;";
			$GLOBALS["LoggedUserName"] = "";
		}
		else {
			$GLOBALS["ShowUserMenu"] = "";
			$GLOBALS["LoggedUserName"] = $user["username"];
		}
		
		$menu_items = GetConfig("MainMenu");
		
		if(isset($menu_items["LogInOut"])){
			unset($menu_items["LogInOut"]);
			if(getUserData()){
				$menu_items["LogOut"] = "user/logout";
			}
			else {
				$menu_items["LogIn"] = "user/login";
			}
		}
		
		$GLOBALS["MainMenuVertical"] = renderMenu($menu_items, "#", "MainMenuVerticalMenu");
	}
	
} 