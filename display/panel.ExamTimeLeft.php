<?php

class PANEL_EXAMTIMELEFT extends AppPanel {
	
	public function SetPanelSettings(){
		if(!isset($_SESSION["answers_submit"]) || (!$examid = key($_SESSION["answers_submit"]))){
			$this->DontDisplay = true;
			return;
		}

		$GLOBALS["ExamsTimeLeft"] = "";
		$exam_model = getModel("exam");
		foreach($_SESSION["answers_submit"] as $examid => $details){
			if(isset($details["exam_start"]) && is_float($details["exam_start"]) && $details["exam_start"] > 0){
				$exam = $exam_model->get(array("examid" => $examid));
				if(!$exam || !is_numeric($exam["examduration"]) || $exam["examduration"] == 0) {
					continue;
				}
				
				$GLOBALS["ExamStartTime"] = date("Y/m/d G:i:s", $details["exam_start"]);
				$GLOBALS["ExamEndTime"] = date("Y/m/d G:i:s", $details["exam_start"] + $exam["examduration"]);
				$GLOBALS["ExamTimeLeftExamId"] = $examid;
				
				$GLOBALS["ExamsTimeLeft"] .= $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("ExamTimeLeftItem");
			}
		}
		
	}
}