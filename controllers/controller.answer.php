<?php

class APPCONTROLLER_ANSWER extends APP_BASE {

	public $menu = array(
			"remote_getAnswer" => 2,
			"remote_addAnswer" => 2,
			"remote_deleteAnswer" => 2,
			"remote_saveAnswer" => 2,
	);
	
	function remote_getAnswer(){
		if(!isset($_GET['answerid']) || !isId($_GET['answerid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/getAnswer', 'answerid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$answer_model = getModel("answer");
		$answer = $answer_model->get(array("answerid" => $_GET["answerid"]));
		
		if(!$answer){
			AddLog(sprintf(GetLang("ErrorGettingAnswer"), 'Answer/getAnswer', 'answerid')." Error: ".$answer_model->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "answer" => $answer));
			exit;
		}
	}
	
	function remote_addAnswer(){
		if(!isset($_GET['examid']) || !isId($_GET['examid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveNewAnswer', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveNewAnswer', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		if(!isset($_GET['answertext']) || trim($_GET['answertext']) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveNewAnswer', 'answertext'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		$answer_model = getModel("answer");
		
		if(!isset($_GET["answerorder"]) || !isId($_GET["answerorder"])){
			$order_result = $answer_model->getSingleResultSet(0, 1, array("questionid" => $_GET["questionid"]), array(), array("answerorder" => "ifnull(max(answerorder)+1, 0)"));
			$order = $order_result["answerorder"];
		}
		else {
			$order = $_GET["answerorder"];
			$update = $GLOBALS['APP_CLASS_DB']->UpdateQuery($answer_model->getTableName(), "answerorder = answerorder+1", "answerorder >= '".$_GET["answerorder"]."' AND questionid = '".$_GET["questionid"]."'");
			if(!$update){
				AddLog(GetLang("ErrorAddingAnswer"). "Error: ".$GLOBALS['APP_CLASS_DB']->GetError());
				echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
				exit;
			}
		}
		
		$data = array(
				"answertext" => $_GET["answertext"],
				"questionid" => $_GET["questionid"],
				"examid" => $_GET["examid"],
				"answerorder" => $order,
		);

		$answerid = $answer_model->add($data);
		
		if(!isId($answerid)){
			AddLog(GetLang("ErrorAddingAnswer"). "Error: ".$answer_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "answer" => array("answerid" => $answerid, "answertext" => $_GET["answertext"], "answerorder" => $order)));
			exit;
		}
	}
	
	function remote_deleteAnswer(){
		if(!isset($_GET['answerid']) || !isId($_GET['answerid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/RemoteDeleteAnswer', 'answerid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		$answer_model = getModel("answer");
	
		$answer = $answer_model->get(array("answerid" => $_GET["answerid"]));
		$success = $answer_model->delete(array("answerid" => $_GET["answerid"]));
	
		if(!$success){
			AddLog(GetLang("ErrorDeleteAnswer"). "Error: ".$answer_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			$user = getUserData();
			AddLogSuccess(sprintf(GetLang("SuccessUserDeleteAnswer"), $user["username"], substr($answer["answertext"], 0, 25), $_GET["answerid"], $answer["questionid"], $answer["examid"]));
			echo app_json_encode(array("success" => 1));
			exit;
		}
	}
	
	function remote_saveAnswer(){
		if(!isset($_GET['answerid']) || !isId($_GET['answerid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveAnswer', 'answerid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET['examid']) || !isId($_GET['examid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveAnswer', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveAnswer', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET['answertext']) || trim($_GET['answertext']) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/SaveAnswer', 'answertext'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}

		$answer_model = getModel("answer");
		
		$data = array(
				"answertext" => $_GET["answertext"],
				"questionid" => $_GET["questionid"],
				"examid" => $_GET["examid"],
		);
		
		$answerid = $answer_model->edit($data, array("answerid" => $_GET["answerid"]));
		if(!$answerid){
			AddLog(GetLang("ErrorAddingAnswer"). "Error: ".$answer_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "answer" => array("answerid" => $answerid, "answertext" => $_GET["answertext"])));
			exit;
		}
	}
}