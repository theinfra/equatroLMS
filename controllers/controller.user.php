<?php

include_once 'controllerbase.user.php';

class APPCONTROLLER_USER extends APPCONTROLLERBASE_USER {	
	public function __construct(){
		$this->menu = array_merge($this->menu, array(
			"courses" => 2,
			"addCourse" => 2,
		));
	}
	
	public function courses(){
		$request = parseGetVars();
		
		if(isset($request["2"]) && isId($request["2"])){
			return $this->viewCoursesSingle();
		}
		
		$user = getUserData();
		$this->breadcrumbs = array("user/view" => GetLang("User"), "#" => GetLang("UserCourses"));
		
		$user_courses = $this->getUserCourses($user["userid"]);
		
		$GLOBALS["UserCoursesList"] = "";
		
		if(empty($user_courses)){
			return;
		}
		
		foreach($user_courses as $course){
			$GLOBALS["UserCoursesListCourseId"] = $course["courseid"];
			$GLOBALS["UserCoursesListCourseName"] = $course["coursename"];
			$GLOBALS["UserCoursesList"] .= $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("UserCoursesList"); 
		}
	}
	
	private function viewCoursesSingle(){
		$request = parseGetVars();
		

	}
	
	private function getUserCourses($userid = 0){
		$course_user_model = getModel('course_user');
		
		$courses_user = $course_user_model->getResultSet(
				0,
				"*",
				"userid = '".$userid."' AND course_user_access >= 20"
			);

		$courses = array();
		
		if(empty($courses_user)){
			return $courses;
		}
			
		$course_model = getModel("course");
		foreach($courses_user as $courseid){
			$course = $course_model->get($courseid["courseid"]);
			$courses[$courseid["courseid"]] = $course;
		}

		return $courses;
	}
	
	function viewsingle(){
		parent::viewsingle();
		
		$user = getUserData();
		$request = parseGetVars();
		
		if(!isset($request[2]) || !isId($request[2])){
			flashMessage(GetLang("ErrorUserNotFound"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["UserViewSingleUserId"] = $request[2];
		
		$user_model = getModel("user");
		$user_request = $user_model->get(array("userid" => $request["2"]));
		
		if(!$user_request){
			flashMessage(GetLang("ErrorUserNotFound"), APP_SEVERITY_ERROR);;
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$user_courses = $this->getUserCourses($user_request["userid"]);
		
		$GLOBALS["UserCoursesList"] = "";
		
		if(!empty($user_courses)){
			foreach($user_courses as $course){
				$GLOBALS["UserCoursesListCourseId"] = $course["courseid"];
				$GLOBALS["UserCoursesListCourseName"] = $course["coursename"];
				$GLOBALS["UserCoursesList"] .= $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("UserCoursesList");
			}
		}
		
		if($user["usergroup"] >= 2){
			$GLOBALS["UserAddToCourseDisplay"] = "";
			$course_model = getModel("course");
			$courses = $course_model->getResultSet(0, "*", array("coursestatus" => "1"), array("coursename" => "ASC"));
			
			$GLOBALS["UserViewSingleCourseOptions"] = "";

			if(!$courses || empty($courses)){
				$GLOBALS["UserViewSingleCourseOptions"] = "<option>".GetLang("NoCoursesFound")."</option>";
			}
			
			$GLOBALS["UserViewSingleCourseOptions"] = "<option>".GetLang("SelectOne")."</option>";
			foreach($courses as $course){
				$GLOBALS["UserViewSingleCourseOptions"] .= "<option value=\"".$course["courseid"]."\">".$course["coursename"]."</option>";
			}
		}
		else {
			$GLOBALS["UserAddToCourseDisplay"] = "display: none";
		}
	}
	
	function addCourse(){
		if(!isset($_POST["UserViewSingleUserId"]) || !isId($_POST["UserViewSingleUserId"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "User/ViewSingle", "UserId"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/user/admin");
			exit;
		}
		
		$userid = $_POST["UserViewSingleUserId"];
		
		if(!isset($_POST["UserViewSingleCourseId"]) || !isId($_POST["UserViewSingleCourseId"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "User/ViewSingle", "CourseId"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/user/view/".$userid);
			exit;
		}
		
		if(!isset($_POST["UserViewSingleAddUserToCourseUserAccess"]) || !isId($_POST["UserViewSingleAddUserToCourseUserAccess"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "User/ViewSingle", "UserPermissions"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/user/view".$userid);
			exit;
		}
		
		$course_user_model = getModel("course_user");
		$user_course = $course_user_model->get(array("courseid" => $_POST["UserViewSingleCourseId"], "userid" => $_POST["UserViewSingleUserId"]));
		
		/*
		if($user_course && !empty($user_course)){
			flashMessage(GetLang("InfoUserAlreadyInCourse"), APP_SEVERITY_NOTICE);
			header("Location: ".$GLOBALS["AppPath"]."/user/view/".$userid);
			exit;
		}
		*/
		
		if($user_course && !empty($user_course)){
			$success = $course_user_model->edit(
			array(
				"course_user_access" => $_POST["UserViewSingleAddUserToCourseUserAccess"],
			),
			array(
				"courseid" => $_POST["UserViewSingleCourseId"],
				"userid" => $userid,
			));
		}
		else {
			$success = $course_user_model->add(array(
					"courseid" => $_POST["UserViewSingleCourseId"],
					"userid" => $userid,
					"course_user_access" => $_POST["UserViewSingleAddUserToCourseUserAccess"],
			));
		}

		if(!$success){
			flashMessage(GetLang("ErrorAddingUserToCourse"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/user/view/".$userid);
			exit;
		}
		else {
			flashMessage(GetLang("SuccessAddUserToCourse"), APP_SEVERITY_SUCCESS);
			header("Location: ".$GLOBALS["AppPath"]."/user/view/".$userid);
			exit;
		}
	}
}