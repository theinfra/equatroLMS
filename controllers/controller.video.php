<?php

class APPCONTROLLER_VIDEO extends APP_BASE {

	public $menu = array(
			"view" => 1,
			"admin" => 2,
			"create" => 2,
			"createsubmit" => 2,
			"edit" => 2,
			"editsubmit" => 2,
			"remote_getVideos" => 2,
			"remote_deleteVideo" => 2,
	);
	
	function admin(){
		$this->breadcrumbs = array("video/admin" => GetLang("Videos"), "#" => GetLang("Admin"));
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";
	}
	
	function create(){
		$this->breadcrumbs = array("video/admin" => GetLang("Videos"), "#" => GetLang("Create"));
		$GLOBALS["VideoCreateYouTubeAPIKey"] = GetConfig("YouTubeAPIKey");
	}
	
	function createsubmit(){
		$postFields = array(
				"VideoCreateVideoExternalId",
				"VideoCreateVideoTitle",
				"VideoCreateVideoDesc",
				"VideoCreateAllowPause"
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Video/Create', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "create";
				overwritePostToGlobalVars();
				return;
			}
		}
		
		if(trim($_POST["VideoCreateVideoExternalId"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("VideoExternalId")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["VideoCreateAllowPause"])){
			$_POST["VideoCreateAllowPause"] = 0;
		}
		
		$new_video = array(
			"videoexternalid" => $_POST["VideoCreateVideoExternalId"],
			"videotitle" => $_POST["VideoCreateVideoTitle"],
			"videodesc" => $_POST["VideoCreateVideoDesc"],
			"allowpause" => (int)$_POST["VideoCreateAllowPause"],
		);

		$video_model = getModel("video");
		$videoid = $video_model->add($new_video);
		
		if(!$videoid){
			AddLog(sprintf(GetLang("ErrorCreatingVideo") . ". Error: ".$video_model->getError().".- Array[".print_r($_POST, true)."]", $new_video["videotitle"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}

		flashMessage(GetLang("VideoCreatedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/video/admin");
		return;
	}
	
	function remote_getVideos(){
		if(!isset($_GET["q"]) || $_GET["q"] != "*"){
			$_GET["q"] = 10;
		}
		
		if(!isset($_GET["o"]) || !isId($_GET["o"])){
			$_GET["o"] = 0;
		}
		
		$video_model = getModel("video");
		$videos = $video_model->getResultSet(
			$_GET["o"], 
			$_GET["q"],
			array(
				
			), //where
			array(
				"videoid" => "ASC",
			), //order
			array(
					
			) //columns
		);
		
		if(!is_array($videos)){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
			
		if(empty($videos)){
			echo app_json_encode(array("success" => 1, "msg" => GetLang("NoVideosFound")));
			exit;
		}
		
		echo app_json_encode(array("success" => 1, "videos" => $videos));
		exit;		
	}
	
	function edit(){
		$this->breadcrumbs = array("video/admin" => GetLang("Videos"), "#" => GetLang("Edit"));
		$GLOBALS["VideoEditYouTubeAPIKey"] = GetConfig("YouTubeAPIKey");
		
		$request = parseGetVars();
		if(!isset($_GET["videoid"]) || !isId($_GET["videoid"])){
			flashMessage(GetLang("VideoInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/video/admin");
			exit;
		}
		
		$video_model = getModel("video");
		$video = $video_model->get(array("videoid" => $_GET["videoid"]));
		
		overwritePostToGlobalVars(array(
			"VideoEditVideoId" => $video["videoid"],
			"VideoEditVideoExternalId" => $video["videoexternalid"],
			"VideoEditVideoTitle" => $video["videotitle"],
			"VideoEditVideoDesc" => $video["videodesc"],
			"VideoEditAllowPause" => $video["allowpause"],
		));
	}
	
	function editsubmit(){
		$postFields = array(
				"VideoEditVideoId",
				"VideoEditVideoExternalId",
				"VideoEditVideoTitle",
				"VideoEditVideoDesc",
				"VideoEditAllowPause"
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Video/Edit', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "edit";
				overwritePostToGlobalVars();
				return;
			}
		}
		
		if(trim($_POST["VideoEditVideoExternalId"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("VideoExternalId")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["VideoEditAllowPause"])){
			$_POST["VideoEditAllowPause"] = 0;
		}
		
		$new_video = array(
				"videoexternalid" => $_POST["VideoEditVideoExternalId"],
				"videotitle" => $_POST["VideoEditVideoTitle"],
				"videodesc" => $_POST["VideoEditVideoDesc"],
				"allowpause" => (int)$_POST["VideoEditAllowPause"],
		);
		
		$video_model = getModel("video");
		$videoid = $video_model->edit($new_video, array("videoid" => $_POST["VideoEditVideoId"]));
		
		if(!$videoid){
			AddLog(sprintf(GetLang("ErrorEditingVideo") . ". Error: ".$video_model->getError().".- Array[".print_r($_POST, true)."]", $new_video["videotitle"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		flashMessage(GetLang("VideoEditedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/video/admin");
		return;
	} 
	
	function remote_deleteVideo(){
		if(!isset($_GET["videoid"]) || !isId($_GET["videoid"])){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("VidoInvalid")));
			exit;			
		}
		
		$video_model = getModel("video");
		$success = $video_model->delete(array("videoid" => $_GET["videoid"]));
		
		if(!$success){
			AddLog(sprintf(GetLang("ErrorDeletingVideo"), $_GET["videoid"]), ". ".$video_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;			
		}
		else {
			echo app_json_encode(array("success" => 1));
			exit;			
		}
	}
}