<?php

class APPCONTROLLER_COURSEMODULE extends APP_BASE {

	public $menu = array(
			"view" => 1,
			"remote_getModuleDetails" => 1,
	);
	
	//cada KEY es la posible extension de archivo que se puede manejar
	//su correspondiente VALUE es el nombre de handler, debe de existir un metodo en esta clase llamado "file_view_VALUE"
	private $file_view_handlers = array(
		"pdf" => "pdf",
		"ppt" => "ppt",
		"pptx" => "ppt",
		"pptm" => "ppt",
	);
	
	private function getModuleTypeSmalltext($moduletype){
		if(!isId($moduletype)){
			return false;
		}
		
		switch($moduletype){
			case 1:
				return "file";
			break;
			case 2:
				return "exam";
			break;
			case 3:
				return "video";
			break;
			default:
				return false;
			break;
		}
	}
	
	private function view_exam($coursemodule){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";
		
		$examid = $coursemodule["moduleobjectid"];
		$GLOBALS["ExamViewExamId"] = $examid;
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $examid));
		
		if(!$exam){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ExamViewExamTitle"] = $exam["examtitle"];
		$GLOBALS["ExamViewExamDesc"] = htmlspecialchars_decode($exam["examdesc"]);
		
		$this->UpdateCourseModuleUserStatus($coursemodule, 1);
	}
	
	private function view_video($coursemodule){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("VideoInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$videoid = $coursemodule["moduleobjectid"];
		$GLOBALS["VideoViewVideoId"] = $videoid;
		$video_model = getModel("video");
		$video = $video_model->get(array("videoid" => $videoid));
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $coursemodule["courseid"]));
		
		$this->breadcrumbs = array("course/view/".$coursemodule["courseid"] => $course["coursename"], "#" => $video["videotitle"]);
		
		if(!$video){
			flashMessage(GetLang("VideoInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		if($video["allowpause"] == 1){
			$GLOBALS["VideoViewPointerEvents"] = "";
		}
		else {
			$GLOBALS["VideoViewPointerEvents"] = "pointer-events: none;";
		}
		$GLOBALS["VideoViewVideoTitle"] = $video["videotitle"];
		$GLOBALS["VideoViewVideoDesc"] = htmlspecialchars_decode($video["videodesc"]);
		$GLOBALS["VideoViewVideoExternalId"] = htmlspecialchars_decode($video["videoexternalid"]);
		
		$this->UpdateCourseModuleUserStatus($coursemodule, 10);
	}
	
	private function getFileViewHandler($extension){
		if(!is_string((string)$extension)){
			return false;
		}
		
		if(array_key_exists($extension, $this->file_view_handlers)){
			return $this->file_view_handlers[$extension];
		}
		else {
			return "other";
		}
	}
	
	private function view_file_pdf($file){
		
	}
	
	private function view_file_ppt($file){
	
	}
	
	private function view_file_other($file){
		
	}
	
	private function view_file($coursemodule){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("FileInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$fileid = $coursemodule["moduleobjectid"];
		$GLOBALS["fileViewFileId"] = $fileid;
		$file_model = getModel("file");
		$file = $file_model->get(array("fileid" => $fileid));
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $coursemodule["courseid"]));
		
		$this->breadcrumbs = array("course/view/".$coursemodule["courseid"] => $course["coursename"], "#" => $file["filetitle"]);
		
		if(!$file){
			flashMessage(GetLang("FileInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$file_details = pathinfo(APP_BASE_PATH.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR.$file["fileid"].DIRECTORY_SEPARATOR.$file["filepath"]);
		if(!file_exists($file_details["dirname"].DIRECTORY_SEPARATOR.$file_details["basename"])){
			flashMessage(GetLang("FileInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$file["filedetails"] = $file_details;
		$GLOBALS["FileViewFileTitle"] = $file["filetitle"];
		$GLOBALS["FileViewFileDesc"] = htmlspecialchars_decode($file["filedesc"]);
		$GLOBALS["FileViewFilePath"] = "files/".$fileid."/".$file["filepath"];
		
		$handler = $this->getFileViewHandler($file_details["extension"]);
		$method_name = "view_file_".$handler;
		if(method_exists($this, $method_name)){
			$this->$method_name($file);
		}
		
		$GLOBALS['AppRequestVars'][0] = $GLOBALS['AppRequestVars'][0].".".$handler;
		$this->UpdateCourseModuleUserStatus($coursemodule, 10);
	} 
	
	function view(){
		$request = parseGetVars();
		
		if(!isset($request[2]) || !isId($request[2])){
			flashMessage(GetLang("CourseModuleInvalid"), APP_SEVERITY_ERROR);
			return;
		}
		
		$coursemoduleid = $request[2];
		$coursemodule_model = getModel("coursemodule");
		$coursemodule = $coursemodule_model->get(array("moduleid" => $coursemoduleid));
		
		$moduletype = $this->getModuleTypeSmalltext($coursemodule["moduletype"]);
		$GLOBALS['AppRequestVars'][0] = $GLOBALS['AppRequestVars'][0].".".$moduletype;
		
		$view_function_name = "view_".$moduletype;
		if(method_exists($this, $view_function_name)){
			$this->$view_function_name($coursemodule);
		}
	}
	
	private function UpdateCourseModuleUserStatus($module, $status){
		if(!isId($status)){
			AddLog(GetLang("ErrorUpdateUserModuleStatusNotNumeric"));
			return false;
		}
		
		$user = getUserData();
		
		$model = getModel("coursemodule_user");
		$coursemodule_user = $model->get(array("userid" => $user["userid"], "moduleid" => $module["moduleid"]));

		if(!$coursemodule_user || !is_array($coursemodule_user) || empty($coursemodule_user)){
			$success = $model->add(array(
				"moduleid" => $module["moduleid"],
				"userid" => $user["userid"],
				"courseid" => $module["courseid"],
				"dateupdated" => time(),
				"module_user_status" => $status,
			));
			
			if(!$success){
				AddLog(sprintf(GetLang("ErrorUpdatingCourseModule"), $module["moduleid"], $user["userid"]));
			}
		}
		else if(is_array($coursemodule_user) && isset($coursemodule_user["moduleid"]) && isId($coursemodule_user["moduleid"]) && $coursemodule_user["module_user_status"] < $status){
			$success = $model->edit(
					array(
							"dateupdated" => time(),
							"module_user_status" => $status,
					),
					array(
							"moduleid" => $coursemodule_user["moduleid"]
					)
				);
			
			if(!$success){
				AddLog(sprintf(GetLang("CourseModuleUserNotLogged"), $module["moduleid"], $user["userid"]));
			}
		}		
	}
	
	private function getUserLastModule($courseid){

		if(!isId($courseid)){
			return false;

		}

		$coursemodule_model = getModel("coursemodule");
		$coursemodules = $coursemodule_model->getResultSet(
			0, 
			"*",
			array(
				"courseid" => $courseid,
			), //where
			array(
				"moduleorder" => "ASC",
			), //order
			array(
					
			) //columns
		);
		
		if(!$coursemodules || empty($coursemodules)){
			return false;
		}

		$user = getUserData();
		
		if(!is_array($user) || empty($user)){
			return false;
		}
		
		$coursemodule_user_model = getModel("coursemodule_user");
		$coursemodule_user = $coursemodule_user_model->getResultSet(
			0, 
			"*",
			array(
				"userid" => $user["userid"],
				"courseid" => $courseid,
			), //where
			array(
				
			), //order
			array(
					
			) //columns
		);
		
		$new_array = array();
		foreach($coursemodule_user as $v){
			$new_array[$v["moduleid"]] = $v;
		}
		$coursemodule_user = $new_array;
		unset($new_array);

		$lastkey = 0;
		$returned_module = array();
		$complete = true;

		foreach($coursemodules as $key => $module){
			$lastkey = $key;
			if(array_key_exists($module["moduleid"], $coursemodule_user) && (int)$coursemodule_user[$module["moduleid"]]["module_user_status"] == 10){
				continue;
			}
			else {
				$complete = false;
				break;
			}
		}

		if($complete){
			return array("last" => $coursemodules[$lastkey], "next" => "complete");
		}
		elseif($lastkey > 0){
			return array("last" => $coursemodules[$lastkey-1], "next" => $coursemodules[$lastkey]);
		}
		else {
			return array("last" => false, "next" => $coursemodules[0]);
		}
		
		
	}
	
	function remote_getModuleDetails(){
		if(!isset($_GET["moduleid"]) || trim($_GET["moduleid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'CourseModule/getModuleDetails', 'moduleid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$coursemodule_model = getModel("coursemodule");
		$coursemodule = $coursemodule_model->get(array("moduleid" => $_GET["moduleid"]));
		
		$moduletypetext = $this->getModuleTypeSmalltext($coursemodule["moduletype"]);		
		$module_model = getModel($moduletypetext);
		$module_keyname = $module_model->getPrimaryKeyName();
		$module_keyname = $module_keyname[0];
		
		$module = $module_model->getSingleResultSet(
			0, 
			"*",
			array(
				$module_keyname => $coursemodule["moduleobjectid"],
			), //where
			array(
				
			), //order
			array(
					
			) //columns
		);
		
		$coursemodule["moduletypesmalltext"] = $this->getModuleTypeSmalltext($coursemodule["moduletype"]);
		$coursemodule["moduletypetext"] = GetLang("CourseModuleType".$coursemodule["moduletype"]);
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $coursemodule["courseid"]));
		
		if($course["courseinorder"] == 0){
			$coursemodule["ShowGoButton"] = true;
		}
		else {
			$userLastModule = $this->getUserLastModule($course["courseid"]);
			if($coursemodule["moduleorder"] > $userLastModule["next"]["moduleorder"]){
				$coursemodule["ShowGoButton"] = false;
			}
			else {
				$coursemodule["ShowGoButton"] = true;
			}
		}
		
		echo app_json_encode(array("success" => 1, "coursemodule" => $coursemodule,  "module" => $module));
		exit;
	}
}