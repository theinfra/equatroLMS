<?php

class APPCONTROLLER_EXAM extends APP_BASE {
	
	public $menu = array(
			"view" => 0,
			"admin" => 2,
			"create" => 2,
			"createsubmit" => 2,
			"submit" => 0,
			"edit" => 2,
			"editsubmit" => 2,
			"solve" => 1,
			"remote_addQuestion" => 2,
			"remote_editQuestion" => 2,
			"remote_getAnswers" => 2,
			"remote_getQuestionRow" => 2,
			"remote_getUserExamSubmits" => 1,
			"remote_saveanswer" => 1,
			"remote_savenewquestion" => 2,
			"remote_saveQuestionsOrder" => 2,
			"viewsubmit" => 2,
			"delete" => 2,
	);
	
	function view(){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ExamViewExamId"] = $request[2];
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $request[2]));
		
		if(!$exam){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ExamViewExamTitle"] = $exam["examtitle"];
		$GLOBALS["ExamViewExamDesc"] = htmlspecialchars_decode($exam["examdesc"]);
	}
	
	function submit(){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $request[2]));
		
		if(!$this->verifyCompletedExam($exam)){
			flashMessage(GetLang("ErrorSubmitExamNotComplete"));
			header("Location: ".$GLOBALS["AppPath"]."/exam/solve/".$request[2]);
			exit;
		}
		
		if(!isset($_SESSION["answers_submit"][$exam["examid"]]) || !is_array($_SESSION["answers_submit"][$exam["examid"]]) || empty($_SESSION["answers_submit"][$exam["examid"]])){
			flashMessage(GetLang("ErrorSubmitExamEmpty"));
			header("Location: ".$GLOBALS["AppPath"]."/exam/solve/".$request[2]);
			exit;			
		}

		$user = getUserData();
		$exam_user_model = getModel("exam_user");
		
		$tryno = $exam_user_model->getSingleResultSet(
			0, 
			1,
			array(
				"examid" => $request[2],
				"userid" => $user["userid"],
			), //where
			array(
				
			), //order
			array(
				"tryno" => "ifnull(max(exam_user_tryno), 0)+1",	
			) //columns
		);

		if(!$tryno){
			$tryno["tryno"] = 1;
		}

		foreach($_SESSION["answers_submit"][$exam["examid"]]["answers"] as $question => $answer){
			$exam_user = array(
				"examid" => $request[2],
				"userid" => $user["userid"],
				"exam_user_tryno" => $tryno["tryno"],
				"exam_user_submitdate" => time(),
				"questionid" => $question,
				"answerid_selected" => $answer,
			);
			
			$exam_user_success = $exam_user_model->add($exam_user);
			if(!$exam_user_success){
				AddLog(GetLang("ErrorSavingExamSubmit"). "Error: ".$exam_user_model->GetError());
				header("Location: ".$GLOBALS["AppPath"]."/");
				exit;
			}
			
			unset($_SESSION["answers_submit"][$exam["examid"]]["answers"][$question]);
		}

		unset($_SESSION["answers_submit"][$exam["examid"]]);
		if(empty($_SESSION["answers_submit"])){
			unset($_SESSION["answers_submit"]);	
		}
	}
	
	private function verifyCompletedExam($exam){
		if(is_numeric($exam["examnumquestions"]) && $exam["examnumquestions"] > 0){
			if(isset($_SESSION["answers_submit"][$exam["examid"]]["answers"]) && count($_SESSION["answers_submit"][$exam["examid"]]["answers"]) >= $exam["examnumquestions"]){
				return true;
			}
			else {
				return false;
			}
		}
		else {
			$questions_model = getModel("question");
			$questions = $questions_model->getResultSet(0, "*", array("examid" => $exam["examid"]), array("questionorder" => "ASC"));

			if(count($questions) == 0){
				flashMessage(GetLang("ErrorSubmitExamEmpty"));
				return false;
			}
			
			foreach($questions as $question){
				if(!isset($_SESSION["answers_submit"][$exam["examid"]]["answers"][$question["questionid"]]) || !is_numeric($_SESSION["answers_submit"][$exam["examid"]]["answers"][$question["questionid"]])){
					return false;
				}
			}

			return true;
		}
	}
	
	private function getNextQuestion($exam){
		$question_model = getModel("question");
		
		$request = parseGetVars();
		if(!isset($request[3]) || trim($request[3]) == "" || !isId($request[3])){
			$request[3] = 1;
		}

		if($exam["examquestionorder"] == "random"){
			$where = 'examid = "'.$exam["examid"].'"';
			if(isset($_SESSION["answers_submit"][$exam["examid"]]['answers']) && is_array($_SESSION["answers_submit"][$exam["examid"]]['answers']) && !empty($_SESSION["answers_submit"][$exam["examid"]]['answers'])){
				$answered_questions = array();
				foreach($_SESSION["answers_submit"][$exam["examid"]]['answers'] as $questionid => $answer){
					$answered_questions[] = $questionid;
				}
				
				$where .=  ' AND questionid NOT IN ('.implode($answered_questions).')';
			}
			
			$question = $question_model->getSingleResultSet(
				0, 
				1,
				$where, //where
				"RAND()"
				, //order
				array(
						
				) //columns
			);
		}
		else /*if($exam["examquestionorder"] == "defined")*/{ // defined es el default, aqui puede ser nulo
			$question = $question_model->getSingleResultSet($request[3]-1, 1, array("examid" => $exam["examid"]), array("questionorder" => "ASC"));
			
			if(!$question){
				$questions = $question_model->getResultSet(
					0, 
					"*",
					array(
						"examid" => $exam["examid"],
					), //where
					array(
						"questionorder" => "ASC",
					), //order
					array(
							
					) //columns
				);
				
				foreach($questions as $question){
					if(!isset($_SESSION["answers_submit"][$exam["examid"]][$question["questionid"]])){
						return $question;
					}
				}
			}
		}
		
		return $question;
	}
	
	function solve(){
		$request = parseGetVars();
		
		if(!isset($request[2]) ||  trim($request[2]) == "" || !isId($request[2])){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/exam/view/".$request[2]);
			exit;
		}
				
		$examid = $request[2];

		if(isset($_SESSION["answers_submit"]) && !isset($_SESSION["answers_submit"][$examid]) && count($_SESSION["answers_submit"]) > 0){
			flashMessage(sprintf(GetLang("ExamErrorCurrentExamDifferent"), $GLOBALS["AppPath"], key($_SESSION["answers_submit"])));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-effects/jquery-ui-effects.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-effects.min.js\"></script>";
		
		$GLOBALS["ExamSolveExamId"] = $examid;
		
		if(!isset($request[3]) || trim($request[3]) == "" || !isId($request[3])){
			$request[3] = 1;
		}
		
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $examid));

		if(!$exam){
			flashMessage(GetLang("ExamInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["ExamViewExamTitle"] = $exam["examtitle"];
		$GLOBALS["ExamViewExamDesc"] = htmlspecialchars_decode($exam["examdesc"]);
		
		if($this->verifyCompletedExam($exam)){
			header("Location: ".$GLOBALS["AppPath"]."/exam/submit/".$examid);
			exit;
		}
		
		$question = $this->getNextQuestion($exam);

		if(!$question || empty($question)){
			flashMessage(GetLang("ErrorFindingQuestion"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}

		if(isset($_SESSION["answers_submit"][$examid][$question["questionid"]])){
			//Cambio esto para que no deje contestar la misma pregunta si se regresa
			//unset($_SESSION["answers_submit"][$examid][$question["questionid"]]);
			header("Location: ".$GLOBALS["AppPath"]."/exam/solve/".$examid."/".$request[3]+=1);
			exit;
		}
		
		if(!isset($_SESSION["answers_submit"][$examid]["exam_start"])){
			$_SESSION["answers_submit"][$examid]["exam_start"] = microtime(true);
		}

		$GLOBALS["ExamSolveQuestionId"] = $question["questionid"];
		$GLOBALS["ExamSolveQuestionIndex"] = $request[3];
		$GLOBALS["ExamSolveQuestionIndexNext"] = $request[3]+1;
		$GLOBALS["ExamSolveQuestionHelp"] = $question["questionhelp"];
		
		$GLOBALS["ExamSolveQuestionText"] = $question["questiontext"];
		$GLOBALS["ExamSolvePrevDisabled"] = ($question["questionorder"] == 1) ? "disabled=\"disabled\"" : "";
		$GLOBALS["ExamSolvePrevDisabled"] = ($exam["examshowanswers"]) ? "disabled=\"disabled\"" : "";
		
		$answer_model = getModel("answer");
		$answers = $answer_model->getResultSet(0, "*", array("examid" => $examid, "questionid" => $question["questionid"]), array("answerorder" => "ASC"));

		if(!$answers || empty($answers)){
			flashMessage(GetLang("ErrorFindingAnswers"));
			$GLOBALS["AppRequestVars"][1] = "view";
			return;
		}
		
		$GLOBALS["ExamSolveAnswersOptions"] = "";
		foreach($answers as $answer){
			$GLOBALS["ExamSolveAnswersOptions"] .= "<li id=\"LiExamSolveAnswer".$answer["answerorder"]."\"><input type=\"radio\" id=\"ExamSolveAnswer".$answer["answerorder"]."\" name=\"ExamSolveAnswers\" value=\"".$answer["answerid"]."\" class=\"ExamSolveAnswers FormField FormFieldRadio\" /><label for=\"ExamSolveAnswer".$answer["answerorder"]."\">".$answer["answertext"]."</label></li>";
		}
	}
	
	function admin(){
		$this->breadcrumbs = array("exam/admin" => GetLang("Exams"), "#" => GetLang("Admin"));
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";
		
		$exam_model = getModel("exam");
		$exams = $exam_model->getResultSet(0, "*");
		
		if(empty($exams)){
			$GLOBALS["ExamAdminList"] = GetLang("NoExamsFound");
			return;
		}
		
		$GLOBALS["ExamAdminList"] = "<table id=\"ExamAdminList\">";
		$GLOBALS["ExamAdminList"] .= "<thead>";
		$GLOBALS["ExamAdminList"] .= "<tr>";
		$GLOBALS["ExamAdminList"] .= "<th>".GetLang("ExamName")."</th>";
		$GLOBALS["ExamAdminList"] .= "<th>".GetLang("ExamDateStart")."</th>";
		$GLOBALS["ExamAdminList"] .= "<th>".GetLang("ExamDateEnd")."</th>";
		$GLOBALS["ExamAdminList"] .= "<th>".GetLang("Action")."</th>";
		$GLOBALS["ExamAdminList"] .= "</tr>";
		$GLOBALS["ExamAdminList"] .= "</thead>";
		foreach($exams as $exam){
			$GLOBALS["ExamAdminList"] .= "<tr>
					<td>".$exam["examname"]."</td>
					<td>".formatDateSpanish($exam["examdatestart"], true, true)."</td>
					<td>".formatDateSpanish($exam["examdateend"], true, true)."</td>
					<td><a class=\"ExamAdminUserDelete\" href=\"".$GLOBALS['AppPath']."/exam/delete?examid=".$exam['examid']."\">Eliminar</a> | <a href=\"".$GLOBALS['AppPath']."/exam/edit?examid=".$exam['examid']."\">Editar</a></td>
					</tr>";
		}
		$GLOBALS["ExamAdminList"] .= "</table>";
	}
	
	function create(){
		if(!isset($_POST["ExamCreatePassingGrade"]) || !is_numeric($_POST["ExamCreatePassingGrade"])){
			$_POST["ExamCreatePassingGrade"] = 60;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-datepicker/jquery-ui-datepicker.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-datepicker.min.js\"></script>";
		
		overwritePostToGlobalVars();
	}
	
	function createsubmit(){
		$postFields = array(
			"ExamCreateExamName",
			"ExamCreateExamTitle",
			"ExamCreateExamDesc",
			"ExamCreateDateStart",
			"ExamCreateDateEnd",
			"ExamCreateTriesPerUser",
			//"ExamCreateNumQuestions",
			"ExamCreatePassingGrade",
			"ExamCreateExamDurationQuantity",
			"ExamCreateExamDurationUnit",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/Create', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "create";
				overwritePostToGlobalVars();
				return;
			}
		}
		
		if(trim($_POST["ExamCreateExamName"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("ExamName")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["ExamCreateTriesPerUser"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamTriesPerUser")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["ExamCreatePassingGrade"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamPassingGrade")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(trim($_POST["ExamCreateExamDurationQuantity"]) != "" && !is_numeric($_POST["ExamCreateExamDurationQuantity"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamCreateExamDurationQuantity")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		/*
		if(!is_numeric($_POST["ExamCreateNumQuestions"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamNumQuestions")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		*/
		$exam_model = getModel("exam");
		$user = getUserData();
		
		switch($_POST["ExamCreateExamDurationUnit"]){
			case "days":
				$_POST["ExamCreateExamDurationQuantity"] = $_POST["ExamCreateExamDurationQuantity"] * (60 * 60 * 24);
				break;
			case "hours":
				$_POST["ExamCreateExamDurationQuantity"] = $_POST["ExamCreateExamDurationQuantity"] * (60 * 60);
				break;
			case "minutes":
				$_POST["ExamCreateExamDurationQuantity"] = $_POST["ExamCreateExamDurationQuantity"] * (60);
				break;
		}
		
		$new_exam = array(
			"examname" => $_POST["ExamCreateExamName"],
			"examtitle" => $_POST["ExamCreateExamTitle"],
			"examdesc" => $_POST["ExamCreateExamDesc"],
			"examtriesperuser" => $_POST["ExamCreateTriesPerUser"],
			"examdatecreated" => time(),
			"examcreator" => $user["userid"],
			"examnumquestions" => 0,
			"exampassinggrade" => $_POST["ExamCreatePassingGrade"],
			"examduration" => $_POST["ExamCreateExamDurationQuantity"],
		);
		
		if(trim($_POST["ExamCreateDateStart"]) != ""){
			$new_exam["examdatestart"] = strtotime($_POST["ExamCreateDateStart"]);
		}

		if(trim($_POST["ExamCreateDateEnd"]) != ""){
			$new_exam["examdateend"] = strtotime($_POST["ExamCreateDateEnd"]);
		}
		
		$examid = $exam_model->add($new_exam);

		if(!$examid){
			AddLog(sprintf(GetLang("ErrorCreatingExam") . ". Error: ".$exam_model->getError().".- Array[".print_r($_POST, true)."]", $new_exam["examname"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		flashMessage(GetLang("ExamCreatedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/exam/admin");
		exit;
	}
	
	function edit(){
		if(!isset($_GET['examid']) || !isId($_GET['examid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/Edit', 'ExamEditExamId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.structure.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.theme.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-sortable/jquery-ui.min.js\"></script>";
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-datepicker/jquery-ui-datepicker.min.css\">";
		
		$examid = $_GET['examid'];
		$GLOBALS["ExamEditExamId"] = $examid;
		
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $examid));
		
		if(!$exam){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/EditSubmit', 'ExamEditExamId'));
			flashMessage(GetLang('ErrorNoExamFound'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		
		$this->breadcrumbs = array("exam/admin" => GetLang("Exams"), "#" => $exam["examname"]);

		if(($exam["examduration"] / (60 * 60 * 24)) > 1){
			$examduration_unit = "days";
			$examduration_quantity = $exam["examduration"] / (60 * 60 * 24);
		}
		
		else if(($exam["examduration"] / (60 * 60)) > 1){
			$examduration_unit = "hours";
			$examduration_quantity = $exam["examduration"] / (60 * 60);
		}
		
		else if(($exam["examduration"] / 60) > 1){
			$examduration_unit = "minutes";
			$examduration_quantity = $exam["examduration"] / 60;
		}
		
		else {
			$examduration_unit = "seconds";
			$examduration_quantity = $exam["examduration"];
		}
		
		overwritePostToGlobalVars(array(
			"ExamEditExamName" => $exam["examname"],
			"ExamEditExamTitle" => $exam["examtitle"],
			"ExamEditExamDesc" => $exam["examdesc"],
			"ExamEditDateStart" => date("m/d/Y", $exam["examdatestart"]),
			"ExamEditDateEnd" => date("m/d/Y", $exam["examdateend"]),
			"ExamEditTriesPerUser" => $exam["examtriesperuser"],
			"ExamEditShowAnswers" => $exam["examshowanswers"],
			"ExamEditPassingGrade" => $exam["exampassinggrade"],
			"ExamEditNumQuestions" => $exam["examnumquestions"],
			"ExamEditQuestionOrder" => $exam["examquestionorder"],
			"ExamEditExamDurationQuantity" => $examduration_quantity,
			"ExamEditExamDurationUnit" => $examduration_unit,
		));
		
		$question_model = getModel("question");
		// ToDo: Hacer esto con Join con answers para sacar el total de respuestas de la pregunta y no tener que hacer otra consulta adentro del foreach
		$questions = $question_model->getResultSet(0, "*", array("examid" => $examid), array("questionorder" => "ASC"));
		
		if(empty($questions)){
			$GLOBALS["ExamEditQuestionsTable"] = "<tr align=\"center\" class=\"CourseEditAddFirstQuestion\"><td colspan=\"3\">".GetLang("ExamNoQuestionsFound"). " "."<a href=\"#\"><img src=\"".$GLOBALS["AppPath"]."/images/addicon.png\" /></a></td></tr>";
		}
		else {
			$GLOBALS["ExamEditQuestionsTable"] = "";
			$answer_model = getModel("answer");
			
			foreach($questions as $question){
				$answers = $answer_model->getSingleResultSet(0, "*", array("questionid" => $question["questionid"], "examid" => $examid), array(), array("answerstotal" => "ifnull(count(answerid), 0)"));

				$row = '<tr questionid="'.$question["questionid"].'" class="RowExamQuestionList">
							<td>'.$question["questiontext"].'</td>
							<td><a href="'.$GLOBALS["AppPath"].'/question/edit?questionid='.$question["questionid"].'" class="EditRowQuestionEditAnswers">'.$answers["answerstotal"].'</a></td>
							<td>
								<input type="hidden" name="RowExamQuestionOrder" id="RowExamQuestionOrder" value="'.$question["questionorder"].'" />
								<a class=RowExamQuestionEdit href='.$GLOBALS["AppPath"].'/question/edit?questionid='.$question["questionid"].'><img src=%%GLOBAL_AppPath%%/images/icon-edit.png /></a> 
								<a class=RowExamQuestionAdd href=#><img src=%%GLOBAL_AppPath%%/images/addicon.png /></a> 
								<a class=RowExamQuestionDelete href=#><img src=%%GLOBAL_AppPath%%/images/delicon.png /></a>
							</td>
						</tr>';
				
				$GLOBALS["ExamEditQuestionsTable"] .= $row;
			}
		}
	}
	
	function editsubmit(){
		if(!isset($_POST['ExamEditExamId']) || !isId($_POST['ExamEditExamId'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/EditSubmit', 'ExamEditExamId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			$GLOBALS['AppRequestVars'][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		$examid = $_POST['ExamEditExamId'];
		$GLOBALS["ExamEditExamId"] = $examid;
		
		$postFields = array(
				"ExamEditExamName",
				"ExamEditExamTitle",
				"ExamEditExamDesc",
				"ExamEditDateStart",
				"ExamEditDateEnd",
				"ExamEditTriesPerUser",
				"ExamEditShowAnswers",
				"ExamEditPassingGrade",
				"ExamEditNumQuestions",
				"ExamEditQuestionOrder",
				"ExamEditExamDurationQuantity",
				"ExamEditExamDurationUnit",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/EditSubmit', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				//$GLOBALS['AppRequestVars'][1] = "edit";
				overwritePostToGlobalVars();
				header("Location: ".$GLOBALS['AppPath']."/exam/edit?examid=".$_POST['ExamEditExamId']);
				exit;
			}
		}
		
		if(trim($_POST["ExamEditExamName"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("ExamName")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["ExamEditTriesPerUser"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamTriesPerUser")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["ExamEditPassingGrade"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamPassingGrade")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(trim($_POST["ExamEditExamDurationQuantity"]) != "" && !is_numeric($_POST["ExamEditExamDurationQuantity"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamEditExamDuration")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["ExamEditNumQuestions"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("ExamNumQuestions")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		$exam_model = getModel("exam");
		
		switch($_POST["ExamEditExamDurationUnit"]){
			case "days":
				$_POST["ExamEditExamDurationQuantity"] = $_POST["ExamEditExamDurationQuantity"] * (60 * 60 * 24);
				break;
			case "hours":
				$_POST["ExamEditExamDurationQuantity"] = $_POST["ExamEditExamDurationQuantity"] * (60 * 60);
				break;
			case "minutes":
				$_POST["ExamEditExamDurationQuantity"] = $_POST["ExamEditExamDurationQuantity"] * (60);
				break;
		}
		
		$exam = array(
				"examname" => $_POST["ExamEditExamName"],
				"examtriesperuser" => $_POST["ExamEditTriesPerUser"],
				"examshowanswers" => $_POST["ExamEditShowAnswers"],
				"exampassinggrade" => $_POST["ExamEditPassingGrade"],
				"examnumquestions" => $_POST["ExamEditNumQuestions"],
				"examquestionorder" => $_POST["ExamEditQuestionOrder"],
				"examduration" => $_POST["ExamEditExamDurationQuantity"],
				);
		
		if(trim($_POST["ExamEditExamTitle"]) != ""){
			$exam["examtitle"] = $_POST["ExamEditExamTitle"];
		}
		
		if(trim($_POST["ExamEditExamDesc"]) != ""){
			$exam["examdesc"] = $_POST["ExamEditExamDesc"];
		}
		
		if(trim($_POST["ExamEditDateStart"]) != ""){
			$exam["examdatestart"] = strtotime($_POST["ExamEditDateStart"]);
		}
		
		if(trim($_POST["ExamEditDateEnd"]) != ""){
			$exam["examdateend"] = strtotime($_POST["ExamEditDateEnd"]);
		}
		
		$success = $exam_model->edit($exam, array("examid" => $examid));
		
		if(!$success){
			AddLog(sprintf(GetLang("ErrorEditingExam") . ". Error: ".$exam_model->getError().".- Array[".print_r($_POST, true)."]", $exam["examname"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "edit";
			$_GET["examid"] = $examid;
			overwritePostToGlobalVars();
			return;
		}
		
		flashMessage(GetLang("ExamEditedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/exam/admin");
		return;
	}
	
	function remote_saveanswer(){
		if(!isset($_GET["examid"]) || !is_numeric($_GET["examid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveAnswer', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}		
		
		/* Quito esto porque el usuario puede dejar la respuesta en blanco, solamente se sigue.
		if(!isset($_GET["answerchoice"]) || trim($_GET["answerchoice"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveAnswer', 'answerchoice'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		*/
		
		if(!isset($_GET["questionid"]) || trim($_GET["questionid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveAnswer', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$response = array("success" => 1);
		
		$exam_model = getmodel("exam");
		$exam = $exam_model->get(array($_GET["examid"]));
		
		if(!$exam){
			logAdd(GetLang("ErrorSavingAnswer"). "Error: ".$exam->GetError()." GET: ".print_array($_GET, true, true));
			echo app_json_encode(array("success" => 0, "msg" => "ErrorMsgGeneric"));
			exit;
		}
/*
		if(!isset($_SESSION["answers_submit"]) || !is_array($_SESSION["answers_submit"])){
			$_SESSION["answers_submit"] = array();
		}
		*/
		if(!isset($_SESSION["answers_submit"][$_GET["examid"]]["answers"]) || !is_array($_SESSION["answers_submit"][$_GET["examid"]]["answers"])){
			$_SESSION["answers_submit"][$_GET["examid"]]["answers"] = array();
		}
		
		$_SESSION["answers_submit"][$_GET["examid"]]["answers"][$_GET["questionid"]] = $_GET["answerchoice"];
		
		if($exam["examshowanswers"] == "question"){
			$question_model = getModel("question");
			$question = $question_model->getResultSet(0, 1, array("examid" => $_GET["examid"], "questionid" => $_GET["questionid"]));

			if(!$question || empty($question)){
				AddLog(GetLang("ErrorSavingAnswer"). ". GET: ".print_array($_GET, true, true));
				echo app_json_encode(array("success" => 0, "msg" => "ErrorMsgGeneric"));
				exit;
			}
			
			$question = $question[0];
			
			$response["correctanswer"] = ($_GET["answerchoice"] == $question["correctanswer"]) ? true : false;
			$response["correctanswerno"] = $question["correctanswer"];
		}

		echo app_json_encode($response);
		exit;
	}
	
	function remote_savenewquestion(){
		if(!isset($_GET["examid"]) || !is_numeric($_GET["examid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveNewQuestion', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["questiontext"]) || trim($_GET["questiontext"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveNewQuestion', 'questiontext'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$question_model = getModel("question");
		$order_result = $question_model->getSingleResultSet(0, 1, array("examid" => $_GET["examid"]), array(), array("questionorder" => "ifnull(max(questionorder)+1, 0)"));
		$order = $order_result["questionorder"];
		
		$data = array(
				"examid" => $_GET["examid"],
				"questionorder" => $order,
				"questiontext" => $_GET["questiontext"],
				"questionhelp" => "",
				"correctanswer" => 0,
				"answerstotal" => 0,
		);
		$questionid = $question_model->add($data);
		$data["questionid"] = $questionid;
		
		if(!isId($questionid)){
			AddLog(sprintf(GetLang("ErrorCreateQuestion"), 'Exam/SaveNewQuestion', 'questiontext'). ". ".$question_model->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		echo app_json_encode(array("success" => 1, "question" => $data));
		exit;
	}
	
	function remote_addQuestion(){
		if(!isset($_GET["examid"]) || !is_numeric($_GET["examid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveNewQuestion', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["questiontext"]) || trim($_GET["questiontext"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveNewQuestion', 'questiontext'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$question_model = getModel("question");
		if(!isset($_GET["questionorder"]) || !is_numeric($_GET["questionorder"])){
			$order_result = $question_model->getSingleResultSet(0, 1, array("examid" => $_GET["examid"]), array(), array("questionorder" => "ifnull(max(questionorder)+1, 0)"));
			$_GET["questionorder"] = $order_result["questionorder"];
		}
		else {
			$update = $GLOBALS['APP_CLASS_DB']->UpdateQuery($question_model->getTableName(), "questionorder = questionorder+1", "questionorder > '".$_GET["questionorder"]."' AND examid = '".$_GET["examid"]."'");
			if(!isId($questionid)){
				AddLog(sprintf(GetLang("ErrorAddQuestion"), 'Exam/AddQuestion', 'questionorder'). ". ".$question_model->getError());
				echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
				exit;
			}
		}
		
		$data = array(
				"examid" => $_GET["examid"],
				"questionorder" => $_GET["questionorder"],
				"questiontext" => $_GET["questiontext"],
				"questionhelp" => "",
				"correctanswer" => 0,
				"answerstotal" => 0,
		);
		$questionid = $question_model->add($data);
		$data["questionid"] = $questionid;
		
		if(!isId($questionid)){
			AddLog(sprintf(GetLang("ErrorCreateQuestion"), 'Exam/SaveNewQuestion', 'questiontext'). ". ".$question_model->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		echo app_json_encode(array("success" => 1, "question" => $data));
		exit;
	}
	
	function remote_getQuestionRow(){
		if(!isset($_GET["questionid"]) || !is_numeric($_GET["questionid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/GetQuestionRow', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$question_model = getModel("question");
		$question = $question_model->get($_GET["questionid"]);
		
		if(!$question){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgNoQuestionFound")));
			exit;
		}

		$answer_model = getModel("answer");
		// ToDo: Necesitamos filtrar por examid aqui o solo con la questionid?
		$answers = $answer_model->getSingleResultSet(0, "*", array("questionid" => $_GET["questionid"]), array(), array("answerstotal" => "ifnull(count(answerid), 0)"));
		if($answers) {
			$question["answerstotal"] = $answers["answerstotal"];
		}
		else {
			$question["answerstotal"] = 0;
		}
		
		if($question){
			echo app_json_encode(array("success" => 1, "question" => $question));
			exit;
		}		
	}
	
	function remote_editQuestion(){
		if(!isset($_GET["questionid"]) || !is_numeric($_GET["questionid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/GetQuestionRow', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["questiontext"]) || trim($_GET["questiontext"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/GetQuestionRow', 'questiontext'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$question_model = getModel("question");
		$data = array(
			"questiontext" => $_GET["questiontext"],
		);
		
		$success = $question_model->edit($data, array("questionid" => $_GET["questionid"]));
		
		if($success){
			$question = $question_model->get(array("questionid" => $_GET["questionid"]));
			echo app_json_encode(array("success" => 1, "question" => $question));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	}
	
	function remote_getAnswers(){
		if(!isset($_GET["questionid"]) || !is_numeric($_GET["questionid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/GetQuestionRow', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$answer_model = getModel("answer");
		$answers = $answer_model->getResultSet(0, "*", array("questionid" => $_GET["questionid"]), array("answerorder" => "ASC"), array("answerid", "answerorder", "answertext"));
		
		if(!$answers || empty($answers)) {
			$answers = array();
		}

		echo app_json_encode(array("success" => 1, "answers" => $answers));
		exit;
	}
	
	function remote_saveQuestionsOrder(){
		if(!isset($_GET["serialized"]) || trim($_GET["serialized"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/SaveQuestionsOrder', 'serialized'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		$tmp = explode("&", $_GET["serialized"]);
	
		$values = array();
		foreach($tmp as $value){
			$parts = explode("=", $value);
			if(isset($parts[1])) {
				$values[$parts[0]] = $parts[1];
			}
		}
	
		$question_model = GetModel("question");
	
		foreach($values as $key => $value){
			$question = $question_model->edit(array("questionorder" => $value), array("questionid" => $key));
		}
	
		echo app_json_encode(array("success" => 1));
		exit;
	}
	
	function remote_getUserExamSubmits(){
		if(!isset($_GET["examid"]) || trim($_GET["examid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/getUserExamSubmits', 'examid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$user = getUserData();
		if(!$user){
			AddLog(GetLang("UserNotLoggedIn"));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$exam_user_model = getModel("exam_user");
		$exams_user = $exam_user_model->getResultSet(
			0, 
			"*",
			array(
				"userid" => $user["userid"],
				"examid" => $_GET["examid"],
			), //where
			array(
				"exam_user_tryno" => "ASC",
			), //order
			array(
				"*",
				"FROM_UNIXTIME(exam_user_submitdate) AS submitdate_date",
			) //columns
		);
		
		$question_model = getModel("question");
		$questions = $question_model->getResultSet(
			0, 
			"*",
			array(
				"examid" => $_GET["examid"],
			), //where
			array(
				
			), //order
			array(
					
			) //columns
		);
		
		$new_array = array();
		$total_numquestions = 0;
		foreach($questions as $question){
			$new_array[$question["questionid"]] = $question;
			$total_numquestions++;
		}
		$questions = $new_array;
		unset($new_array);
		
		$exams_result = array();
		foreach($exams_user as $exam){
			if(!isset($exams_result[$exam["exam_user_tryno"]])){
				$exams_result[$exam["exam_user_tryno"]] = array(
						"correct_answers" => 0,
						"exam_user_tryno" => $exam["exam_user_tryno"],
						"exam_user_submitdate_unix" => $exam["exam_user_submitdate"],
						"exam_user_submitdate" => $exam["submitdate_date"],
				);
			}
			
			if($exam["answerid_selected"] == $questions[$exam["questionid"]]["correctanswer"]){
				$exams_result[$exam["exam_user_tryno"]]["correct_answers"]++;
			}
		}
		
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $_GET["examid"]));
		
		if(!is_int($exam["examnumquestions"]) || $exam["examnumquestions"] <= 0 || $exam["examnumquestions"] > $total_numquestions){
			$exam["examnumquestions"] = $total_numquestions;
		}
		
		foreach($exams_result as $key => $result){
			if($exam["examshowanswers"] != "no"){
				$exams_result[$key]["grade"] = round($result["correct_answers"] / $exam["examnumquestions"], 1, PHP_ROUND_HALF_DOWN) * 10;
				$exams_result[$key]["totalquestions"] = $exam["examnumquestions"];
			}
			else {
				$exams_result[$key]["grade"] = "?";
				$exams_result[$key]["totalquestions"] = "?";
				$exams_result[$key]["correct_answers"] = "?";
			}
		}
		
		
		echo app_json_encode(array("success" => 1, "results" => $exams_result));
		exit;
	}
	
	public function viewsubmit(){
		if(!isset($_GET["examid"]) || !isId($_GET["examid"])){
			flashMessage(GetLang("ExamInvalid"));
			header('Location: '.$GLOBALS["AppPath"]."/");
			exit;
		}
		
		if(!isset($_GET["userid"]) || !isId($_GET["userid"])){
			flashMessage(GetLang("ErrorUserNotFound"));
			header('Location: '.$GLOBALS["AppPath"]."/");
			exit;
		}
		
		if(!isset($_GET["tryno"]) || !isId($_GET["tryno"])){
			flashMessage(GetLang("ErrorExamTryNoInvalid"));
			header('Location: '.$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$pdflib = getLib('dompdf');
		
		if(isset($_GET["viewhtml"]) && $_GET["viewhtml"] == 1){
			$pdflib->setReturn("html");
		}
		
		$exam_model = getModel('exam');
		$question_model = getModel('question');
		$answer_model = getModel('answer');
		
		$exam = $exam_model->get($_GET["examid"]);
		
		if(!$exam || empty($exam)){
			flashMessage(GetLang("ExamInvalid"));
			header('Location: '.$GLOBALS["AppPath"]."/");
		}
		
		$GLOBALS["ExamViewSubmitExamTitle"] = $exam["examtitle"];
		$GLOBALS["ExamViewSubmitExamDesc"] = $exam["examdesc"];
		
		$questions = $question_model->getResultSet(0, "*", array("examid" => $_GET["examid"]), array("questionorder" => "ASC"));
		
		if(!$questions || empty($questions)){
			$pdflib->renderPDF("exam.pdfviewsubmit");
			exit;
		}
		
		$answer_model = getModel('answer');
		
		$exam_user_model = getModel('exam_user');
		$exam_user = $exam_user_model->getResultSet(0, "*", array("examid" => $_GET["examid"], "userid" => $_GET["userid"], "exam_user_tryno" => $_GET["tryno"]));
		
		if(!$exam_user || empty($exam_user)){
			$pdflib->renderPDF("exam.pdfviewsubmit");
			exit;
		}
		
		$exam_user_new = array();
		foreach($exam_user as $exam_user_item){
			$exam_user_new[$exam_user_item["questionid"]] = $exam_user_item;
		}
		$exam_user = $exam_user_new;
		unset($exam_user_new);
		
		$GLOBALS["ExamViewSubmitQuestions"] = "";
		foreach($questions as $index => $question){
			$GLOBALS["ExamViewSubmitQuestionIndex"] = $index+1;
			$GLOBALS["ExamViewSubmitQuestionText"] = $question["questiontext"];
			$GLOBALS["ExamViewSubmitQuestionHelp"] = $question["questionhelp"];
			
			$answers = $answer_model->getResultSet(0, "*", array("examid" => $_GET["examid"], "questionid" => $question["questionid"]), array("answerorder" => "ASC"));
			
			if(!$answers || empty($answers)){
				$GLOBALS["ExamViewSubmitAnswers"] = GetLang("QuestionHasNoAnswers");
			}
			else {
				$GLOBALS["ExamViewSubmitAnswers"] = "";
				
				foreach($answers as $index => $answer){
					$GLOBALS["ExamViewSubmitAnswerIndex"] = chr(65+$index);
					$GLOBALS["ExamViewSubmitAnswerText"] = $answer["answertext"];
					
					if($question["correctanswer"] == $answer["answerid"]){
						$GLOBALS["ExamViewSubmitCorrectAnswer"] = "CorrectAnswer"; 
					}
					else {
						$GLOBALS["ExamViewSubmitCorrectAnswer"] = "";
					}
					
					if($answer["answerid"] == $exam_user[$question["questionid"]]["answerid_selected"]){
						$GLOBALS["ExamViewSubmitSelectedAnswer"] = "SelectedAnswer";
					}
					else {
						$GLOBALS["ExamViewSubmitSelectedAnswer"] = "";
					}
						
					$GLOBALS["ExamViewSubmitAnswers"] .= $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("ExamViewSubmitAnswer");
				}
			}
			
			$GLOBALS["ExamViewSubmitQuestionAnswers"] = $GLOBALS["ExamViewSubmitAnswers"];
			$GLOBALS["ExamViewSubmitQuestions"] .= $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("ExamViewSubmitQuestion");
		}
		
		if(isset($_GET["viewhtml"]) && $_GET["viewhtml"] == 1){
			$GLOBALS["AppRequestVars"][1] = "pdfviewsubmit";
			return;
		}
		else {
			$pdflib->renderPDF("exam.pdfviewsubmit");
			exit;
		}
	}
	
	function delete(){
		if(!isset($_GET["examid"]) || !isId($_GET["examid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/RemoteDeleteQuestion', 'examid'));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		
		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		
		$question_model = getModel("question");
		$answer_model = getModel("answer");
		$questions = $question_model->getResultSet(0, "*", array("examid" => $_GET["examid"]));
		if(is_array($questions) && !empty($questions)){
			foreach($questions as $question){
				$answers = $answer_model->getResultSet(0, "*", array("questionid" => $question["questionid"]));
				if(is_array($answers) && !empty($answers)){
					foreach($answers as $answer){
						if(!$answer_model->delete(array("answerid" => $answer["answerid"]))){
							$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
							AddLog(GetLang("ErrorDeleteAnswer"). "Error: ".$answer_model->GetError());
							echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
							exit;
						}
					}
				}
				
				if(!$question_model->delete(array("questionid" => $question["questionid"]))){
					$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
					AddLog(GetLang("ErrorDeleteQuestion"). "Error: ".$question_model->GetError());
					echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
					exit;
				}
			}
		}
		
		$exam_model = getModel("exam");
		
		$exam = $exam_model->get(array("examid" => $_GET["examid"]));
		$success = $exam_model->delete(array("examid" => $_GET["examid"]));
		
		if(!$success){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(GetLang("ErrorDeleteExam"). ". Error: ".$exam_model->GetError());
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		else {
			$user = getUserData();
			AddLogSuccess(sprintf(GetLang("SuccessUserDeleteExam"), $user["username"], substr($exam["examname"], 0, 25), $_GET["examid"]));
			flashMessage(GetLang("SuccessExamDelete"), APP_SEVERITY_SUCCESS);
			$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
	}
}