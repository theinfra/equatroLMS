<?php

class APPCONTROLLER_QUESTION extends APP_BASE {

	public $menu = array(
			"edit" => 2,
			"editsubmit" => 2,
			"remote_saveAnswersOrders" => 2,
			"remote_getAnswers" => 2,
			"remote_saveCorrectAnswer" => 2,
			"remote_deleteQuestion" => 2,
	);
	
	function edit(){
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Answer/Edit', 'questionid'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.structure.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.theme.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-sortable/jquery-ui.min.js\"></script>";
		
		$question_model = getModel("question");
		$question = $question_model->get(array("questionid" => $_GET["questionid"]));
		
		$exam_model = getModel("exam");
		$exam = $exam_model->get(array("examid" => $question["examid"]));
		
		$this->breadcrumbs = array("exam/admin" => GetLang("Exams"), "exam/edit/?examid=".$question["examid"] => $exam["examname"], "#" => getLang("EditQuestion"));

		overwritePostToGlobalVars(array(
			"QuestionEditQuestionId" => $question["questionid"],
			"QuestionEditExamId" => $question["examid"],
			"QuestionEditQuestionOrder" => $question["questionorder"],
			"QuestionEditQuestionText" => $question["questiontext"],
			"QuestionEditQuestionHelp" => $question["questionhelp"],
			"QuestionEditCorrectAnswer" => $question["correctanswer"],				
		));
		
		$answer_model = getModel("answer");
		$answers = $answer_model->getResultSet(0, "*", array("questionid" => $_GET["questionid"]), array("answerorder" => "ASC"));
		$answers_table = "";
		
		if(empty($answers)){
			$answers_table .= '<tr><td>'.GetLang('NoAnswersFound').' <a class="EditQuestionAddNewAnswer" href="#"><img src="'.$GLOBALS["AppPath"].'/images/addicon.png" /></a></td></tr>';
		}
		else {
			foreach($answers as $answer){
				if($question["correctanswer"] == $answer["answerid"]){
					$answer_selected = 'checked="checked"';
				}
				else {
					$answer_selected = '';
				}
				
				$answers_table .= '<tr answerid="'.$answer["answerid"].'" class="RowQuestionAnswer">
						<td><input type="radio" name="RowQuestionCorrectAnswer" value="'.$answer["answerid"].'" class="RowQuestionCorrectAnswerRadio" '.$answer_selected.' /></td>
						<td style="width: 600px;">'.$answer["answertext"].'</td>
						<td>
							<input type="hidden" name="RowQuestionAnswerOrder" id="RowQuestionAnswerOrder" value="'.$answer["answerorder"].'" />
							<a class="RowQuestionEditAnswer" href=#><img src=%%GLOBAL_AppPath%%/images/icon-edit.png /></a> 
							<a class="RowQuestionAddAnswer" href=#><img src=%%GLOBAL_AppPath%%/images/addicon.png /></a> 
							<a class="RowQuestionDeleteAnswer" href=#><img src=%%GLOBAL_AppPath%%/images/delicon.png /></a>
						</td>
					</tr>';
			}
		}
		
		$GLOBALS["QuestionEditAnswersTable"] = $answers_table;
	}
	
	function editsubmit(){
		if(!isset($_POST['QuestionEditQuestionId']) || !isId($_POST['QuestionEditExamId'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Exam/EditSubmit', 'QuestionEditExamId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			$GLOBALS['AppRequestVars'][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
		
		$questionid = $_POST['QuestionEditQuestionId'];
		$GLOBALS["QuestionEditQuestionId"] = $questionid;
		
		$postFields = array(
				"QuestionEditQuestionText",
				"QuestionEditQuestionHelp",
				"QuestionEditExamId",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/EditSubmit', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				//$GLOBALS['AppRequestVars'][1] = "edit";
				overwritePostToGlobalVars();
				header("Location: ".$GLOBALS['AppPath']."/question/edit?questionid=".$_POST['QuestionEditExamId']);
				exit;
			}
		}
		
		if(trim($_POST["QuestionEditQuestionText"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("QuestionEditQuestionText")));
			header("Location: ".$GLOBALS['AppPath']."/question/edit?questionid=".$_POST['QuestionEditExamId']);
			exit;
		}
		
		if(!is_numeric($_POST["QuestionEditExamId"])){
			flashMessage(GetLang(["ErrorMsgGeneric"], APP_SEVERITY_ERROR));
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/EditSubmit', $field));
			header("Location: ".$GLOBALS['AppPath']."/question/edit?questionid=".$_POST['QuestionEditExamId']);
			return;
		}
		
		$question_model = getModel("question");
		
		$question = array(
				"questiontext" => $_POST["QuestionEditQuestionText"],
				"questionhelp" => $_POST["QuestionEditQuestionHelp"],
		);
		
		$success = $question_model->edit($question, array("questionid" => $questionid));
		
		if(!$success){
			AddLog(sprintf(GetLang("ErrorEditingQuestion") . ". Error: ".$question_model->getError().".- Array[".print_r($_POST, true)."]", $question["questiontext"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "edit";
			$_GET["questionid"] = $questionid;
			overwritePostToGlobalVars();
			return;
		}
		
		flashMessage(GetLang("QuestionEditedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/exam/edit?examid=".$_POST["QuestionEditExamId"]);
		return;
	}
	
	function remote_getAnswers(){
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/getAnswers', 'questionid'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/exam/admin");
			exit;
		}
		
		$answer_model = getModel("answer");
		$answers = $answer_model->getResultSet(
			0, 
			"*",
			array(
				"questionid" => $_GET["questionid"],	
			), //where
			array(
				"answerorder" => "ASC",
			), //order
			array(
				"answerid",
				"answerorder",
				"answertext",
			) //columns
		);
		
		if(trim($answer_model->GetError()) != ""){
			AddLog(GetLang("ErrorGetAnswerOrders"). "Error: ".$answer_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "answers" => $answers));
			exit;
		}
	}
	
	function remote_saveAnswersOrder(){
		if(!isset($_GET["serialized"]) || trim($_GET["serialized"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/SaveAnswersOrder', 'serialized'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$tmp = explode("&", $_GET["serialized"]);
		
		$values = array();
		foreach($tmp as $value){
			$parts = explode("=", $value);
			if(isset($parts[1])) {
				$values[$parts[0]] = $parts[1];
			}
		}
		
		$answer_model = GetModel("answer");
		
		foreach($values as $key => $value){
			$answer = $answer_model->edit(array("answerorder" => $value), array("answerid" => $key));
		}
		
		echo app_json_encode(array("success" => 1));
		exit;
	}
	
	function remote_saveCorrectAnswer(){
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/SaveCorrectAnswer', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET['answerid']) || !isId($_GET['answerid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/SaveCorrectAnswer', 'answerid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$question_model = getModel("question");
		$success = $question_model->edit(array("correctanswer" => $_GET["answerid"]), array("questionid" => $_GET["questionid"]));
		
		if(!$success){
			AddLog(GetLang("ErrorSavingCorrectAnswer"). "Error: ".$question_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1));
			exit;
		}
	}
	
	function remote_deleteQuestion(){
		if(!isset($_GET['questionid']) || !isId($_GET['questionid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Question/RemoteDeleteQuestion', 'questionid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$answer_model = getModel("answer");
		
		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		
		$answers = $answer_model->getResultSet(0, "*", array("questionid" => $_GET["questionid"]));
		if(is_array($answers) && !empty($answers)){
			foreach($answers as $answer){
				if(!$answer_model->delete(array("answerid" => $answer["answerid"]))){
					$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
					AddLog(GetLang("ErrorDeleteAnswer"). "Error: ".$answer_model->GetError());
					echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
					exit;
				}
			}
		}
	
		$question_model = getModel("question");
	
		$question = $question_model->get(array("questionid" => $_GET["questionid"]));
		$success = $question_model->delete(array("questionid" => $_GET["questionid"]));
	
		if(!$success){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(GetLang("ErrorDeleteQuestion"). "Error: ".$question_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
			$user = getUserData();
			AddLogSuccess(sprintf(GetLang("SuccessUserDeletedQuestion"), $user["username"], substr($question["questiontext"], 0, 25), $_GET["questionid"], $question["examid"]));
			echo app_json_encode(array("success" => 1));
			exit;
		}
	}
}