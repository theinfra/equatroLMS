<?php

class APPCONTROLLERBASE_USERGROUP extends APP_BASE {
	
	public $menu = array(
		"admin" => 3,
		"remote_getUserGroups" => 3,
		"create" => 3,
		"createsubmit" => 3,
		"edit" => 3,
		"editsubmit" => 3,
		"remote_deleteGroup" => 3,
	);

	public function admin(){
		
	}
	
	public function remote_getUserGroups(){
		$usergroup_model = getModel("usergroup");
		
		$usergroups = $usergroup_model->getResultSet(0, "*");
		
		if(empty($usergroups)){
			echo app_json_encode(array("success" => 1, "msg" => GetLang("NoUserGroupsFound")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "usergroups" => $usergroups));
			exit;
		}
	}
	
	public function create(){
		
	}
	
	public function createsubmit(){
		if(!isset($_POST["UsergroupCreateUsername"]) || trim($_POST["UsergroupCreateUsername"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "UserGroup/CreateSubmit", "UsergroupCreateUsername"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		
		$usergroup_model = getModel("usergroup");
		
		$success = $usergroup_model->add(array("groupname" => $_POST["UsergroupCreateUsername"]));
		
		if(!$success){
			AddLog(GetLang("ErrorCreatingUserGroup"). "Error: ".$usergroup_model->GetError());
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		else {
			flashMessage(GetLang("SuccessCreateUserGroup"), APP_SEVERITY_SUCCESS);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
	}
	
	public function edit(){
		if(!isset($_GET["groupid"]) || !isId($_GET["groupid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "UserGroup/Edit", "groupid"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		
		$usergroup_model = getModel("usergroup");
		$usergroup = $usergroup_model->get(array("groupid" => $_GET["groupid"]));
		
		$GLOBALS["UsergroupEditGroupid"] = $_GET["groupid"];
		$GLOBALS["UsergroupEditUsername"] = $usergroup["groupname"];
	}
	
	public function editsubmit(){
		if(!isset($_POST["UsergroupEditGroupid"]) || !isId($_POST["UsergroupEditGroupid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "UserGroup/Edit", "groupid"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		
		if(!isset($_POST["UsergroupEditUsername"]) || trim($_POST["UsergroupEditUsername"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "UserGroup/CreateSubmit", "UsergroupEditUsername"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		
		$usergroup_model = getModel("usergroup");
		
		$success = $usergroup_model->edit(array("groupname" => $_POST["UsergroupEditUsername"]), array("groupid" => $_POST["UsergroupEditGroupid"]));
		
		if(!$success){
			AddLog(GetLang("ErrorEditUserGroup"). "Error: ".$usergroup_model->GetError());
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
		else {
			flashMessage(GetLang("SuccessEditUserGroup"), APP_SEVERITY_SUCCESS);
			header("Location: ".$GLOBALS["AppPath"]."/usergroup/admin");
			exit;
		}
	}
	
	public function remote_deleteGroup(){
		if(!isset($_GET["usergroupid"]) || !isId($_GET["usergroupid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), "UserGroup/RemoteDelete", "usergroupid"));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$user_model = getModel("user");
		$users = $user_model->getResultSet(0, "*", array("membershiptype" => $_GET["usergroupid"]));
		
		if(count($users) > 0){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorGroupHasUsers")));
			exit;
		}
		
		$usergroup_model = getModel("usergroup");
		$success = $usergroup_model->delete(array("groupid" => $_GET["usergroupid"]));
		
		if($success){
			echo app_json_encode(array("success" => 1));
			exit;
		}
		else {
			AddLog(sprintf(GetLang("ErrorDeleteUserGroup"), $_GET["usergroupid"]). "Error: ".$usergroup_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	}
}