<?php

//Para hacer el controlador del indice es necesario copiar este archivo, renombrarlo a controller.index.php, descomentar la siguiente linea y renombrar la clase a APPCONTROLLER_INDEX
//include_once APP_BASE_PATH.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR.'controllerbase.index.php';

class APPCONTROLLER_INDEX extends APP_BASE {

	public $menu = array(
			"view" => 0,
	);
	
	function view(){
		$user = getUserData();
		
		if(!$user || !is_array($user)){
			$GLOBALS['AppRequestVars'][0] = "user";
			$GLOBALS['AppRequestVars'][1] = "login";
			return;
		}
		
		//$GLOBALS["AppRequestVars"][0] = "index.dashboards";
		$dashboard_controller = getController("dashboard");
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Templates/base/dashboards.css\">";
		
		$GLOBALS["IndexDashboardMyCourses"] = $dashboard_controller->renderDashboard("MyCourses");
		$GLOBALS["IndexDashboardMyTasks"] = $dashboard_controller->renderDashboard("MyTasks");
		
		$news_controller = getController("news");
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Templates/base/news.css\">";
		$GLOBALS["IndexNews"] = $news_controller->renderNews();
	}
}