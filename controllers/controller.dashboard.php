<?php

class APPCONTROLLER_DASHBOARD extends APP_BASE {

	public $menu = array(

	);

	public function renderDashboard($name){
		if(trim($name) == "" || !is_string($name)){
			return GetLang("ErrorNoDashboardSpecified");
		}
		
		$func_name = "renderDashboard_".strtolower($name);
		if(method_exists($this, $func_name)){
			return $this->$func_name();
		}
		else {
			return GetLang("ErrorNoDashboardFound");
		}
	}
	
	private function renderDashboard_mycourses(){
		$user = getUserData();
		if(!$user){
			return GetLang("ErrorCreatingDashboard");		
		}
		
		$course_user_model = getModel("course_user");
		$courses_user = $course_user_model->getResultSet(0, "*", array("userid" => $user["userid"]));
		
		if(empty($courses_user)){
			return GetLang("UserNoCoursesFound");
		}
		else {
			$dashboard = '<ul class="CourseList">';
			$course_model = getModel("course");
			foreach($courses_user as $course){
				$course_details = $course_model->get(array("courseid" => $course["courseid"]));
				$dashboard .= '<li><a href="'.$GLOBALS["AppPath"].'/course/view/'.$course["courseid"].'">'.$course_details["coursename"]."</a></li>";
			}
			
			$dashboard .= "</ul>";
			return $dashboard;
		}
	}
	
	private function renderDashboard_mytasks(){
		$user = getUserData();
		if(!$user){
			return GetLang("ErrorCreatingDashboard");
		}
		
		$course_user_model = getModel("course_user");
		$courses_user = $course_user_model->getResultSet(0, 10, array("userid" => $user["userid"]));
		
		if(empty($courses_user)){
			return GetLang("UserNoTasksFound");
		}
		
		$coursemodule_model = getModel("coursemodule");
		$coursemodule_user_model = getModel("coursemodule_user");
		
		$tasks = array();
		
		foreach($courses_user as $course){
			$coursemodules = $coursemodule_model->getResultSet(
				0, 
				"*",
				array(
					"courseid" => $course["courseid"],	
				), //where
				array(
					"moduleorder" => "ASC",
				), //order
				array(
						
				) //columns
			);
			
			$new = array();
			foreach($coursemodules as $coursemodule){
				$new[$coursemodule["moduleid"]] = $coursemodule;
			}
			$coursemodules = $new;
			unset($new);
			
			$coursemodules_user = $coursemodule_user_model->getResultSet(
				0, 
				"*",
				array(
					"userid" => $user["userid"],
					"courseid" => $course["courseid"],	
				), //where
				array(
					
				), //order
				array(
						
				) //columns
			);

			foreach($coursemodules_user as $cmu){
				if(array_key_exists($cmu["moduleid"], $coursemodules) && $cmu["module_user_status"] == 10){
					unset($coursemodules[$cmu["moduleid"]]);
				}
				else /*if($cmu["module_user_status"] > 0)*/{
					$coursemodules[$cmu["moduleid"]]["module_user_status"] = $cmu["module_user_status"]; 
				}
			}
			
			$tasks= array_merge($tasks, $coursemodules);
		}

		$dashboard = '<ul class="TaskList">';
		foreach($tasks as $task){
			if(!isset($task["module_user_status"])){
				$task["module_user_status"] = 0;
			}
			$dashboard .= '<li class="CourseTaskStatus'.$task["module_user_status"].'"><a href="'.$GLOBALS["AppPath"].'/coursemodule/view/'.$task["moduleid"].'">'.$task["modulename"]."</a></li>";			
		}
		$dashboard .= "</ul>";
		
		return $dashboard;
	}
}