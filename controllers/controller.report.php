<?php

class APPCONTROLLER_REPORT extends APP_BASE {

	public $menu = array(
		"userexams" => 3,
		"remote_getUserExams" => 3,
	);
	
	function userexams(){
		$usergroup_model = getModel("usergroup");
		$usergroups = $usergroup_model->getResultSet(0, "*");
		$GLOBALS["ReportUserExamsUserGroupOptions"] = "";
		if($usergroups && !empty($usergroups)){
			foreach($usergroups as $usergroup){
				if(isset($_POST["ReportUserExamsUserMembershipFilter"]) && $_POST["ReportUserExamsUserMembershipFilter"] == $usergroup["groupid"]){
					$GLOBALS["ReportUserExamsUserGroupOptions"] .= '<option value="'.$usergroup["groupid"].'" selected="selected">'.$usergroup["groupname"].'</option>';
				}
				else {
					$GLOBALS["ReportUserExamsUserGroupOptions"] .= '<option value="'.$usergroup["groupid"].'">'.$usergroup["groupname"].'</option>';
				}
			}
		}
		
		$GLOBALS["ViewScripts"] .= '<script src="'.$GLOBALS["AppPath"].'/javascript/jquery-ui-datepicker.min.js"></script>';
		$GLOBALS["ViewStylesheet"] .= '<link rel="stylesheet" href="'.$GLOBALS["AppPath"].'/views/Styles/jquery-ui-datepicker/jquery-ui-datepicker.min.css">';		
	}
	
	function remote_getUserExams(){
		if(!isset($_REQUEST["offset"]) || !isId($_REQUEST["offset"])){
			$_REQUEST["offset"] = 0;
		}
		
		if(!isset($_REQUEST["limit"]) || !isId($_REQUEST["limit"])){
			$_REQUEST["limit"] = 10;
		}
		
		$where = "WHERE 1=1";
		if(isset($_REQUEST["user_filter"]) && trim($_REQUEST["user_filter"]) != ""){
			$users_filter = GetUsersByFilter($_REQUEST["user_filter"]);
			if(!empty($users_filter)){
				$where .= " AND (1=1 ";
				foreach($users_filter as $user){
					$where .='AND u.userid = "'.$user.'"';
				}
				$where .= ")";
			}
			else {
				echo app_json_encode(array("success" => 1, "user_exam" => array()));
				exit;
			}
		}
		
		if(isset($_REQUEST["membership_filter"]) && trim($_REQUEST["membership_filter"]) != ""){
			$where .= " AND u.membershiptype = '".$_REQUEST["membership_filter"]."'";
		}
		
		if(isset($_REQUEST["datefrom_filter"]) && trim($_REQUEST["datefrom_filter"]) != ""){
			$where .= " AND eu.exam_user_submitdate >= '".strtotime($_REQUEST["datefrom_filter"])."'";
		}
		
		if(isset($_REQUEST["dateto_filter"]) && trim($_REQUEST["dateto_filter"]) != ""){
			$where .= " AND eu.exam_user_submitdate <= '".strtotime($_REQUEST["dateto_filter"])."'";
		}
		
		$query = 'SELECT 
					eu.examid,
					eu.exam_user_tryno, 
					eu.exam_user_submitdate,
					 FROM_UNIXTIME(eu.exam_user_submitdate) AS "exam_date",
					e.examname,
					eu.userid,
					u.username,
					u.firstname,
					u.lastname 
				FROM
					exam_user eu
				JOIN exam e ON (eu.examid=e.examid) 
				JOIN user u ON (eu.userid=u.userid)
					'.$where.' 
				GROUP BY eu.userid, eu.exam_user_tryno
							';

		$exam_user = $GLOBALS["APP_CLASS_DB"]->getResultAsArray($query);
		
		$exams = array();
		$exam_model = getModel('exam');
		$question_model = getModel('question');
		$exam_user_model = getModel('exam_user');
		$result = array();
		foreach($exam_user as $examtry){
			if(!isset($exams[$examtry["examid"]])){
				$exams[$examtry["examid"]] = $exam_model->get($examtry["examid"]);
				$questions = $question_model->getResultSet(
						0, 
						"*",
						array(
							"examid" => $examtry["examid"],
						)
				);
				
				foreach($questions as $question){
					$exams[$examtry["examid"]]['questions'][$question["questionid"]] = $question;
				}
			}
			
			$result_line = array(
				"examid" => $examtry["examid"],
				"exam_user_tryno" => $examtry["exam_user_tryno"],
				"exam_user_submitdate" => $examtry["exam_user_submitdate"],
				"exam_date" => $examtry["exam_date"],
				"examname" => $examtry["examname"],
				"userid" => $examtry["userid"],
				"username" => $examtry["username"],
				"firstname" => $examtry["firstname"],
				"lastname" => $examtry["lastname"],
				"total_questions" => ($exams[$examtry["examid"]]["examnumquestions"]>0)?$exams[$examtry["examid"]]["examnumquestions"]:count($exams[$examtry["examid"]]["questions"]),
				"correct_answers" => 0,
			);

			$answers_submitted = $exam_user_model->getResultSet(0, "*", array(
				"userid" => $examtry["userid"],
				"examid" => $examtry["examid"],
				"exam_user_tryno" => $examtry["exam_user_tryno"],
			));
			
			foreach($answers_submitted as $answer){
				if($answer["answerid_selected"] == $exams[$examtry["examid"]]["questions"][$answer["questionid"]]["correctanswer"]){
					$result_line["correct_answers"]++;
				}
			}
			$result[] = $result_line;
		}
		
		foreach($result as $index => $entry){
			if( number_format($entry["correct_answers"] / $entry["total_questions"], 2) * 100 > $exams[$entry["examid"]]["exampassinggrade"] ){
				$result[$index]["grade_class"] = "passes";
				$result[$index]["grade_text"] = GetLang("LangYes");
			}
			else {
				$result[$index]["grade_class"] = "fails";
				$result[$index]["grade_text"] = GetLang("LangNo");
			}
		}

		if(isset($_REQUEST["format"]) && $_REQUEST["format"] == "excel"){
			$data = array(0 => [
					GetLang("ExamID"),
					GetLang("ExamTryNo"),
					GetLang("Date"),
					GetLang("ExamName"),
					GetLang("Username"),
					GetLang("FirstName"),
					GetLang("LastName"),
					GetLang("Questions"),
					GetLang("ExamCorrectAnswers"),
					GetLang("ExamGrade"),
			]);
			foreach($result as $line){
				$data[] = array(
						"examid" => $line["examid"],
						"exam_user_tryno" => $line["exam_user_tryno"],
						"exam_date" => $line["exam_date"],
						"examname" => $line["examname"],
						"username" => $line["username"],
						"firstname" => $line["firstname"],
						"lastname" => $line["lastname"],
						"total_questions" => $line["total_questions"],
						"correct_answers" => $line["correct_answers"],
						"grade_text" => $line["grade_text"],
				);
			}
			
			$lib = getLib("excelexporter");
			$lib->loadData($data);
			$lib->download("report_".GetLang('ReportUsersExamsFileName').'_'.date('d-M-Y').'.xlsx');
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "user_exam" => $result));
			exit;
		}
	}
}