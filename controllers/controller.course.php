<?php

class APPCONTROLLER_COURSE extends APP_BASE {
	
	public $menu = array(
		"view" => 1,
		"admin" => 2,
		"create" => 2,
		"createsubmit" => 2,
		"edit" => 2,
		"editsubmit" => 2,
		"remote_addusertocourse" => 2,
		"remote_getobjects" => 2,
		"remote_changeusercourseaccess" => 2,
		"remote_deletemodule" => 2,
		"remote_deleteuserfromcourse" => 2,
		"remote_getCourseUsers" => 2,
		"remote_getmodulerow" => 2,
		"remote_getmodulerowedit" => 2,
		"remote_getModules" => 1,
		"remote_savemodule" => 2,
		"remote_savemodulesorder" => 2,
		"remote_savenewmodule" => 2,
		"viewlist" => 1,
		"remote_resetCourse" => 2,
		"addusermembership" => 2,
	);
	
	//key is numeric identifier for database and forms, value is name of the model used to store
	private $validModuleTypes = array(
		1 => "file",
		2 => "exam",
		3 => "video",
	);
	
	function view(){
		$request = parseGetVars();		
		if(isset($request[2]) && isId($request[2])){
			return $this->viewsingle($request[2]);
		}
		
		flashMessage(GetLang("NotPermitted"));
		header("Location: ".$GLOBALS["AppPath"]."/");
		exit;
	}
	
	private function viewsingle($courseid){
		if(!isId($courseid)){
			flashMessage(GetLang("CourseInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/");
			exit;
		}
		
		$GLOBALS["CourseViewCourseId"] = $courseid;
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $courseid));
		
		$this->breadcrumbs = array("course/admin" => GetLang("Courses"), "#" => $course["coursename"]);
		
		$GLOBALS["CourseViewCourseName"] = $course["coursename"];
		
		$user_controller = getController("user");
		$GLOBALS["CourseViewCourseStaffList"] = '<li class="CourseInstructor">'.GetLang("CourseInstructor").': '.$user_controller->_buildUserLink($course["courseinstructoruserid"], "CourseInstructor", true).'</li>';
		
		$course_user_model = getModel("course_user");
		$course_users = $course_user_model->getResultSet(
			0, 
			"*",
			'courseid = "'.$courseid.'" AND course_user_access >= 800'
		);
		
		if(is_array($course_users) && !empty($course_users)){
			foreach($course_users as $user){
				$GLOBALS["CourseViewCourseStaffList"] .= '<li class="CourseUser'.$user["course_user_access"].'">'.GetLang("CourseStaffLevel".$user["course_user_access"]).': '.$user_controller->_buildUserLink($user["userid"], "CourseStaff CourseStaffLevel".$user["course_user_access"], true).'</li>';
			}
		}
		
		$GLOBALS["CourseViewCourseModulesList"] = '<tr class="CourseEditCurrentMembersNoUsersFoundRow"><td colspan="3">'.GetLang('LoadingList').'</td></tr>';
	}
	
	function viewlist(){
		
	}
	
	function admin(){
		$this->breadcrumbs = array("course/admin" => GetLang("Courses"), "#" => GetLang("Admin"));
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";
		
		$course_model = getModel("course");
		$courses = $course_model->GetResultSet(0, "*");
		
		if(empty($courses)){
			$GLOBALS["CourseAdminCourseList"] = GetLang("NoCoursesFound");
			return;
		}
		
		$GLOBALS['CourseAdminCourseList'] = "<table width=\"95%\" align=\"center\" id=\"CourseAdminTable\" class=\"CourseAdminTable\">
	<thead>
		<tr>
			<th width=\"55%\">Nombre</th>
			<th>Inicia</th>
			<th>Termina</th>
			<th width=\"200px\">Instructor</th>
			<th>Accion</th>
		</tr>
	</thead>
	<tbody>";
		
		$coursestable = '';
		foreach($courses as $course){
			$coursestable .= "<tr>";
			$coursestable .= "<td><a href=\"".$this->makeCourseLink($course["courseid"], $course["coursename"])."\">" . $course['coursename'] . "</a></td>";
			$coursestable .= "<td>" . formatDateSpanish($course["coursestart"], true, true) . "</td>";
			$coursestable .= "<td>" . formatDateSpanish($course["courseend"], true, true)  . "</td>";
			$coursestable .= "<td>" . $this->makeUserLink($course["courseinstructoruserid"], $course["courseinstructorname"]) . "</td>";
			$coursestable .= "<td><a class=\"CourseAdminUserDelete\" href=\"".$GLOBALS['AppPath']."/course/delete?courseid=".$course['courseid']."\">Eliminar</a> | <a href=\"".$GLOBALS['AppPath']."/course/edit?courseid=".$course['courseid']."\">Editar</a></td>";
			$coursestable .= "</tr>";
		}
		
		$GLOBALS['CourseAdminCourseList'] .= $coursestable . '</tbody>
</table>';
	}
	
	private function makeCourseLink($courseid, $coursename){
		return $GLOBALS["AppPath"]."/course/view/".$courseid."/".urlencode($coursename);
	}
	
	//ToDo: Mover esto a CONTROLLERBASE_USER
	private function makeUserLink($userid, $userlabel){
		if($user = getUser($userid)){
			return "<a target=\"_blank\" href=\"".$GLOBALS["AppPath"]."/user/view/".$userid."/\">".$userlabel."</a>";
		}
		else {
			return $userlabel;
		}
	}
	
	function create(){
		$this->breadcrumbs = array("course" => GetLang("Courses"), "#" => GetLang("Create"));
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.structure.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.theme.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-autocomplete/jquery-ui.min.js\"></script>";
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-datepicker/jquery-ui-datepicker.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-datepicker.min.js\"></script>";
		
		overwritePostToGlobalVars();
	}
	
	function createsubmit(){
		$postFields = array(	
			"CourseCreateCourseName",
			"CourseCreateCourseStart",
			"CourseCreateCourseEnd",
			"CourseCreateCourseInstructorName",
			"CourseCreateCourseInstructorUserId",
			"CourseCreateCourseStatus",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/Create', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "create";
				overwritePostToGlobalVars();
				return;
			}
		}
		
		if(trim($_POST["CourseCreateCourseName"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("CourseName")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(!is_numeric($_POST["CourseCreateCourseStatus"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("CourseStatus")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(trim($_POST["CourseCreateCourseInstructorUserId"]) != "" && (!is_numeric($_POST["CourseCreateCourseInstructorUserId"]) || !getUser($_POST["CourseCreateCourseInstructorUserId"]))){
			flashMessage(GetLang("PleaseInputValidUserId"));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}

		$course_model = getModel("course");
		$new_course = array(
			"coursename" => $_POST["CourseCreateCourseName"],
			"coursestatus" => $_POST["CourseCreateCourseStatus"],
			"courseinorder" => 1,
		);
		
		if(trim($_POST["CourseCreateCourseStart"]) != ""){
			$new_course["coursestart"] = strtotime($_POST["CourseCreateCourseStart"]);
		}
		
		if(trim($_POST["CourseCreateCourseEnd"]) != ""){
			$new_course["courseend"] = strtotime($_POST["CourseCreateCourseEnd"]);
		}
		
		if(trim($_POST["CourseCreateCourseInstructorName"]) != ""){
			$new_course["courseinstructorname"] = $_POST["CourseCreateCourseInstructorName"];
		}
		
		if(trim($_POST["CourseCreateCourseInstructorUserId"]) != ""){
			$new_course["courseinstructoruserid"] = $_POST["CourseCreateCourseInstructorUserId"];
		}

		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		$courseid = $course_model->add($new_course);
		
		if(!$courseid){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(sprintf(GetLang("ErrorCreatingCourse") . ". Error: ".$course_model->getError().".- Array[".print_r($_POST, true)."]", $new_course["coursename"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
		flashMessage(GetLang("CourseCreatedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/course/edit?courseid=".$courseid);
		return;
	}
	
	function edit(){
		$this->breadcrumbs = array("course/admin" => GetLang("Courses"), "#" => GetLang("Edit"));
		if(!isset($_GET['courseid']) || !isId($_GET['courseid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/EditSubmit', 'CourseEditCourseId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.structure.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-autocomplete/jquery-ui.theme.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-autocomplete/jquery-ui.min.js\"></script>";
		
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.structure.min.css\">";
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-sortable/jquery-ui.theme.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-sortable/jquery-ui.min.js\"></script>";
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-ui-datepicker/jquery-ui-datepicker.min.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-ui-datepicker.min.js\"></script>";
		
		$courseid = $_GET['courseid'];
		$GLOBALS["CourseEditCourseId"] = $courseid;
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $courseid));
		
		if(!$course){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/EditSubmit', 'CourseEditCourseId'));
			flashMessage(GetLang('ErrorNoCourseFound'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		overwritePostToGlobalVars(array(
			"CourseEditCourseName" => $course["coursename"],
			"CourseEditCourseStart" => date("m/d/Y", $course["coursestart"]),
			"CourseEditCourseEnd" => date("m/d/Y", $course["courseend"]),
			"CourseEditCourseInstructorName" => $course["courseinstructorname"],
			"CourseEditCourseInstructorUserId" => $course["courseinstructoruserid"],
			"CourseEditCourseStatus" => $course["coursestatus"],
		));
		
		$coursemodule_model = getModel("coursemodule");
		$course_modules = $coursemodule_model->getResultSet(0, "*", array("courseid" => $courseid), array("moduleorder" => "ASC"));
		
		if(empty($course_modules)){
			$GLOBALS["CourseEditModuleList"] = "<tr align=\"center\" class=\"CourseEditAddFirstModule\"><td colspan=\"6\">".GetLang("CourseNoModulesFound"). " "."<a href=\"#\"><img src=\"".$GLOBALS["AppPath"]."/images/addicon.png\" /></a></td></tr>";
		}
		else {
			$GLOBALS["CourseEditModuleList"] = "";
			foreach($course_modules as $module){
				$moduleobject = $this->getModuleObject($module["moduletype"], $module["moduleobjectid"]);
				$requiredtext = ($module["moduleobjectrequired"] == "1") ? GetLang("LangYes") : GetLang("LangNo");
				$statustext = ($module["modulestatus"] == "1") ? GetLang("Active") : GetLang("Blocked");
				$GLOBALS["CourseEditModuleList"].= "<tr class=\"RowCourseModule\" id=\"RowCourseModuleId".$module["moduleid"]."\" moduleid=\"".$module["moduleid"]."\">".
								"<td>".$module["modulename"]."</td>".
								"<td>".GetLang("CourseModuleType".$module["moduletype"])."</td>".
								"<td>".$this->getModuleNameFromObject($module["moduletype"], $moduleobject)."</td>".
								"<td>". $requiredtext ."</td>".
								"<td>". $statustext ."</td>".
								"<td>".
									"<input type=\"hidden\" name=\"RowCourseModuleOrder\" id=\"RowCourseModuleOrder\" value=\"".$module["moduleorder"]."\" />".
									"<a class=\"RowCourseModuleEdit\" href=\"#\"><img src=\"".$GLOBALS["AppPath"]."/images/icon-edit.png\" /></a> ".
									"<a class=\"RowCourseModuleAdd\" href=\"#\"><img src=\"".$GLOBALS["AppPath"]."/images/addicon.png\" /></a> ".
									"<a class=\"RowCourseModuleDelete\" href=\"#\"><img src=\"".$GLOBALS["AppPath"]."/images/delicon.png\" /></a>".
								"</td>".
						"</tr>";
			}
		}
		
		$GLOBALS["CourseEditCurrentMembersTable"] = '<tr class="CourseEditCurrentMembersNoUsersFoundRow"><td colspan="3">'.GetLang('LoadingTable').'</td></tr>';
	}
	
	function editsubmit(){
		if(!isset($_POST['CourseEditCourseId']) || !isId($_POST['CourseEditCourseId'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/EditSubmit', 'CourseEditCourseId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		$courseid = $_POST['CourseEditCourseId'];
		$GLOBALS["CourseEditCourseId"] = $courseid;
		
		$postFields = array(
				"CourseEditCourseName",
				"CourseEditCourseStart",
				"CourseEditCourseEnd",
				"CourseEditCourseInstructorName",
				"CourseEditCourseInstructorUserId",
				"CourseEditCourseStatus",
		);
	
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/Edit', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "edit";
				overwritePostToGlobalVars();
				return;
			}
		}
	
		if(trim($_POST["CourseEditCourseName"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("CourseName")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
	
		if(!is_numeric($_POST["CourseEditCourseStatus"])){
			flashMessage(sprintf(GetLang("PleaseInputNumber"), GetLang("CourseStatus")));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
	
		if(trim($_POST["CourseEditCourseInstructorUserId"]) != "" && (!is_numeric($_POST["CourseEditCourseInstructorUserId"]) || !getUser($_POST["CourseEditCourseInstructorUserId"]))){
			flashMessage(GetLang("PleaseInputValidUserId"));
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
	
		$course_model = getModel("course");
		$edit_course = array(
				"coursename" => $_POST["CourseEditCourseName"],
				"coursestatus" => $_POST["CourseEditCourseStatus"],
				"coursestart" => strtotime($_POST["CourseEditCourseStart"]),
				"courseend" => strtotime($_POST["CourseEditCourseEnd"]),
				"courseinstructorname" => $_POST["CourseEditCourseInstructorName"],
				"courseinstructoruserid" => $_POST["CourseEditCourseInstructorUserId"]
		);
	
		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		$courseid = $course_model->edit($edit_course, array("courseid" => $courseid));
	
		if(!$courseid){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(sprintf(GetLang("ErrorCreatingCourse") . ". Error: ".$course_model->getError().".- Array[".print_r($_POST, true)."]", $new_course["coursename"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			$GLOBALS["AppRequestVars"][1] = "edit";
			overwritePostToGlobalVars();
			return;
		}
	
		$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
		flashMessage(GetLang("CourseEditedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/course/admin/");
		return;
	}
	
	private function getObjectsByModuleType($moduletype){
		$objects = array();
		
		switch ($moduletype){
			case 1:
				$model = getModel("file");
				$objects = $model->getResultSet(
						0,
						"*",
						null,
						array("filetitle" => "ASC"),
						array(
							"fileid AS \"value\"",
							"fileid AS \"id\"",
							"filetitle AS \"name\"",
							)
						);
				
				break;
			case 2:
				$model = getModel("exam");
				$objects = $model->getResultSet(
						0,
						"*",
						null,
						array("examname" => "ASC"),
						array(
							"examid AS \"value\"",
							"examid AS \"id\"",
							"examname AS \"name\"",
						)
						);
				break;
			case 3:
			case 2:
				$model = getModel("video");
				$objects = $model->getResultSet(
						0,
						"*",
						null,
						array("videotitle" => "ASC"),
						array(
							"videoid AS \"value\"",
							"videoid AS \"id\"",
							"videotitle AS \"name\"",
					)
				);
				break;
			default:
				return false;
				break;
		}
		
		return $objects;
	}
	
	function remote_getobjects(){
		if(!isset($_GET["objecttype"]) || !is_numeric($_GET["objecttype"]) || $_GET["objecttype"] == 0){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorNoObjectType")));
			exit;
		}
		
		$objects = $this->getObjectsByModuleType($_GET["objecttype"]);
		
		if(empty($objects)){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorNoObjectsFound")));
			exit;
		}
		else {
			if(array_key_exists("input", $objects)){
				$return = array("success" => 1, "input" => $objects["input"]);
			}
			else {
				array_unshift($objects, array("value" => "", "name" => GetLang("SelectOne"), "selected" => "selected"));
				$return = array("success" => 1, "objects" => $objects);
			}
		}
		
		echo app_json_encode($return);
		exit;
	}
	
	private function getModuleObject($type, $objectid){
		if(!array_key_exists($type, $this->validModuleTypes)){
			return false;
		}
		
		$model = getModel($this->validModuleTypes[$type]);
		return $model->get($objectid);
	}
	
	private function getModuleNameFromObject($type, $object){
		if(!$object){
			return "";
		}
		
		switch($type){
			case 1:
				return $object["filetitle"];
				break;
			case 2:
				return $object["examname"];
				break;
			case 3:
				return $object["videotitle"];
				break;
			default:
				return "";
				break;
		}
	}
	
	private function _verifyModule($module){
		$getFields = array(
				"courseid",
				"modulename",
				"moduleorder",
				"moduletype",
				"moduleobjectid",
				"modulerequired",
				"modulestatus",
		);

		foreach($getFields as $field){
			if(!isset($module[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/SaveNewModule', $field));
				return false;
			}
		}
		
		if(trim($_GET["modulename"]) == ""){
			echo app_json_encode(array("success" => 0, "msg" => sprintf(GetLang("PleaseInputText"), GetLang("CourseModuleName"))));
			exit;
		}
		
		if(!is_numeric($_GET["moduleorder"])){
			echo app_json_encode(array("success" => 0, "msg" => sprintf(GetLang("PlaseInputNumber"), GetLang("CourseModuleOrder"))));
			exit;
		}
		
		if(!array_key_exists($_GET["moduletype"], $this->validModuleTypes)){
			echo app_json_encode(array("success" => 0, "msg" => sprintf(GetLang("PleaseSelectValid"), GetLang("CourseModuleType"))));
			exit;
		}
		
		if(!isId($_GET["moduleobjectid"]) || !($moduleobject = $this->getModuleObject($_GET["moduletype"], $_GET["moduleobjectid"]))){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorModuleObjectNotValid")));
			exit;
		}
		
		if(!is_numeric($_GET["modulestatus"])){
			echo app_json_encode(array("success" => 0, "msg" => sprintf(GetLang("PlaseInputNumber"), GetLang("CourseModuleStatus"))));
			exit;
		}
		
		return true;
	}
	
	function remote_savenewmodule(){
		if(!$this->_verifyModule($_GET)){
			AddLog(GetLang("ErrorModuleVerifyFailed").print_array($_GET, true, true));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $_GET["courseid"]));
		
		if(!$course){
			AddLog(GetLang("ErrorRetrievingCourse").print_array($_GET, true, true));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorCourseNotValid")));
			exit;
		}
		
		$_GET["modulerequired"] = ($_GET["modulerequired"] == "true") ? "1" : "0";
		
		$module_model = getModel("coursemodule");
		$new_module = array(
			"courseid" => $_GET["courseid"],
			"modulename" => $_GET["modulename"],
			"moduleorder" => $_GET["moduleorder"],
			"moduletype" => $_GET["moduletype"],
			"moduleobjectid" => $_GET["moduleobjectid"],
			"moduleobjectrequired" => $_GET["modulerequired"],
			"modulestatus" => $_GET["modulestatus"],
		);
		
		$moduleid = $module_model->add($new_module);
		
		if(!$moduleid){
			echo app_json_encode(array("success" => 0, "msg" =>GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$new_module["moduleid"] = $moduleid;
		$new_module["moduletypetext"] = GetLang("CourseModuleType".$new_module["moduletype"]);
		$moduleobject = $this->getModuleObject($new_module["moduletype"], $new_module["moduleobjectid"]);
		$new_module["moduleobjectname"] = $this->getModuleNameFromObject($new_module["moduletype"], $moduleobject);
		$new_module["modulerequiredtext"] = ($new_module["moduleobjectrequired"] == "1") ? GetLang("LangYes") : GetLang("LangNo");
		$new_module["modulestatustext"] = ($new_module["modulestatus"] == "1") ? GetLang("Active") : GetLang("Blocked");
		
		echo app_json_encode(array("success" => 1, "module" => $new_module));
		exit;
	}
	
	function remote_deletemodule(){
		if(!isset($_GET["moduleid"]) || !isId($_GET["moduleid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/RemoteDeleteModule', "moduleid"));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$id = $_GET["moduleid"];
		$module_model = getModel("coursemodule");
		$success = $module_model->delete(array("moduleid" => $id));
		
		if($success){
			echo app_json_encode(array("success" => 1));
			exit;			
		}
		else {
			AddLog(sprintf(GetLang("ErrorDeletingModule"), $id). " Error: ".$module_model->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	}
	
	function remote_getmodulerowedit(){
		if(!isset($_GET["moduleid"]) || !isId($_GET["moduleid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/RemoteDeleteModule', "moduleid"));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$id = $_GET["moduleid"];
		$module_model = getModel("coursemodule");
		$module = $module_model->get(array("moduleid" => $id));
		
		$objects = $this->getObjectsByModuleType($module["moduletype"]);
		
		if(is_array($objects)){
			foreach($objects as $key => $object){
				if($object["id"] == $module["moduleobjectid"]){
					$objects[$key]["selected"] = "selected=selected";
				}
			}
		}	
			
		if(!$module){
			AddLog(sprintf(GetLang("ErrorGettingCourseModule"), 'Course/GetModuleEditRow', "moduleid").", Error: ".$module->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "module" => $module, "objects" => $objects));
			exit;
		}
	}
	
	function remote_getmodulerow(){
		if(!isset($_GET["moduleid"]) || !isId($_GET["moduleid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/GetMOduleRow', "moduleid"));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		$id = $_GET["moduleid"];
		$module_model = getModel("coursemodule");
		$module = $module_model->get(array("moduleid" => $id));
		
		$module["moduletypetext"] = GetLang("CourseModuleType".$module["moduletype"]);
		
		$moduleobject = $this->getModuleObject($module["moduletype"], $module["moduleobjectid"]);
		$module["moduleobjectname"] = $this->getModuleNameFromObject($module["moduletype"], $moduleobject);

		$module["modulerobjectequiredtext"] = ($module["moduleobjectrequired"] == "1") ? GetLang("LangYes") : GetLang("LangNo");
		$module["modulestatustext"] = ($module["modulestatus"] == "1") ? GetLang("Active") : GetLang("Blocked");
		
		if(!$module){
			AddLog(sprintf(GetLang("ErrorGettingCourseModule"), 'Course/GetModuleEditRow', "moduleid").", Error: ".$module->getError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			echo app_json_encode(array("success" => 1, "module" => $module));
			exit;
		}
	}
	
	function remote_savemodule(){
		if(!isset($_GET["moduleid"]) || !isId($_GET["moduleid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/RemoteSaveModule', 'ModuleID'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;			
		}
		
		if(!$this->_verifyModule($_GET)){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$course_model = getModel("course");
		$course = $course_model->get(array("courseid" => $_GET["courseid"]));
		
		if(!$course){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorCourseNotValid")));
			exit;
		}
		
		$_GET["modulerequired"] = ($_GET["modulerequired"] == "true") ? "1" : "0";
		
		$module_model = getModel("coursemodule");
		$edit_module = array(
				"courseid" => $_GET["courseid"],
				"modulename" => $_GET["modulename"],
				"moduleorder" => $_GET["moduleorder"],
				"moduletype" => $_GET["moduletype"],
				"moduleobjectid" => $_GET["moduleobjectid"],
				"moduleobjectrequired" => $_GET["modulerequired"],
				"modulestatus" => $_GET["modulestatus"],
		);
		
		$moduleid = $module_model->edit($edit_module, array("moduleid" => $_GET["moduleid"]));
		
		if(!$moduleid){
			echo app_json_encode(array("success" => 0, "msg" =>GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$edit_module["moduleid"] = $_GET["moduleid"];
		$edit_module["moduletypetext"] = GetLang("CourseModuleType".$edit_module["moduletype"]);
		$moduleobject = $this->getModuleObject($edit_module["moduletype"], $edit_module["moduleobjectid"]);
		$edit_module["moduleobjectname"] = $this->getModuleNameFromObject($edit_module["moduletype"], $moduleobject);
		$edit_module["modulerobjectequiredtext"] = ($edit_module["moduleobjectrequired"] == "1") ? GetLang("LangYes") : GetLang("LangNo");
		$edit_module["modulestatustext"] = ($edit_module["modulestatus"] == "1") ? GetLang("Active") : GetLang("Blocked");
		
		echo app_json_encode(array("success" => 1, "module" => $edit_module));
		exit;		
	}
	
	function remote_savemodulesorder(){

		if(!isset($_GET["serialized"]) || trim($_GET["serialized"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/SaveModulesOrder', 'serialized'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}

		$tmp = explode("&", $_GET["serialized"]);
		
		$values = array();
		foreach($tmp as $value){
			$parts = explode("=", $value);
			if(isset($parts[1])) {
				$values[$parts[0]] = $parts[1];
			}
		}
		
		$module_model = GetModel("coursemodule");
		
		foreach($values as $key => $value){
			$module = $module_model->edit(array("moduleorder" => $value), array("moduleid" => $key));
		}
		
		echo app_json_encode(array("success" => 1));
		exit;
	}
	
	function remote_addusertocourse(){
		if(!isset($_GET["courseid"]) || trim($_GET["courseid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'courseid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["userid"]) || trim($_GET["userid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'userid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$user_model = getModel("user");
		$user = $user_model->getResultSet(0, "*", array("userid" => $_GET["userid"]));
		
		if(!$user){
			AddLog(sprintf(GetLang("ErrorAddingUserToCourse"), $_GET["courseid"], $_GET["userid"]));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$course_user_model = getModel("course_user");
		$data = array(
				"courseid" => $_GET["courseid"],
				"userid" => $_GET["userid"],
				"course_user_access" => 1,
		);
		
		$success = $course_user_model->add($data);
		if(!$success){
			AddLog(sprintf(GetLang("ErrorAddingUserToCourse"), $_GET["courseid"], $_GET["userid"]). ". ".$course_user_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		echo app_json_encode(array("success" => 1, "user" => $user[0]));
		exit;
	}
	
	function remote_deleteuserfromcourse(){
		if(!isset($_GET["courseid"]) || trim($_GET["courseid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'courseid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["userid"]) || trim($_GET["userid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'userid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$course_user_model = getModel("course_user");
		$success = $course_user_model->delete(array("courseid" => $_GET["courseid"], "userid" => $_GET["userid"]));
		if(!$success){
			AddLog(sprintf(GetLang("ErrorDeletingUserFromCourse"), $_GET["courseid"], $_GET["userid"]). ". ".$course_user_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		echo app_json_encode(array("success" => 1));
		exit;
	}
	
	function remote_changeusercourseaccess(){
		if(!isset($_GET["courseid"]) || trim($_GET["courseid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'courseid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		if(!isset($_GET["userid"]) || trim($_GET["userid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'userid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		if(!isset($_GET["course_user_access"]) || trim($_GET["course_user_access"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/AddUserToCourse', 'userid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		$course_user_model = getModel("course_user");
		$course_user_row = $course_user_model->get(array("courseid" => $_GET["courseid"], "userid" => $_GET["userid"]));
		if(!$course_user_row){
			AddLog(sprintf(GetLang("ErrorGettingUserFromCourse"), $_GET["courseid"], $_GET["userid"]). ". ".$course_user_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$data = array(
				"course_user_access" => $_GET["course_user_access"],
		);
		
		$success = $course_user_model->edit($data, array("courseid" => $_GET["courseid"], "userid" => $_GET["userid"]));
		if(!$success){
			AddLog(sprintf(GetLang("ErrorEditingUserCourseAccess"), $_GET["courseid"], $_GET["userid"], $_GET["course_user_access"]). ". ".$course_user_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
	
		echo app_json_encode(array("success" => 1));
		exit;
	}
	
	function remote_getCourseUsers(){
		if(!isset($_GET["courseid"]) || trim($_GET["courseid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/getCourseUsers', 'courseid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		// ToDo: Hacer esta consulta con JOIN para la info de los usuarios
		$course_user_model = getModel("course_user");
		$course_users = $course_user_model->getResultSet(0, "*", array("courseid" => $_GET["courseid"]));
		
		if(empty($course_users)){
			echo app_json_encode(array("success" => 1, "users" => array()));
			exit;
		}
		else {				
			$user_model = getModel("user");
			$users = array();
			foreach($course_users as $user_course){
				$user = $user_model->get(array("userid" => $user_course["userid"]));
				$users[] = array(
								"userid" => $user_course["userid"],
								"username" => $user["username"],
								"lastname" => $user["lastname"],
								"firstname" => $user["firstname"],
								"user_course_access".$user_course["course_user_access"] => 1,
						);
			}
			
			echo app_json_encode(array("success" => 1, "users" => $users));
			exit;
		}
	}
	
	function remote_getModules(){
		if(!isset($_GET["courseid"]) || trim($_GET["courseid"]) == ""){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'Course/getModules', 'courseid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$coursemodule_model = getModel("coursemodule");
		$coursemodules = $coursemodule_model->getResultSet(
			0, 
			"*",
			array(
				"courseid" => $_GET["courseid"],	
			), //where
			array(
				"moduleorder" => "ASC",
			), //order
			array(
					
			) //columns
		);
		
		if(empty($coursemodules)){
			echo app_json_encode(array("success" => 1, "modules" => array()));
			exit;
		}
		
		$user = getUserData();
		
		$coursemodule_user = array();
		if(is_array($user)){
			$coursemodule_user_model = getModel("coursemodule_user");
			$coursemodule_user = $coursemodule_user_model->getResultSet(
				0, 
				"*",
				array(
					"userid" => $user["userid"],	
				), //where
				array(
					
				), //order
				array(
						
				) //columns
			);
			
			$new_coursemodule_user = array();
			if(is_array($coursemodule_user) && !empty($coursemodule_user)){
				foreach($coursemodule_user as $value){
					$new_coursemodule_user[$value["moduleid"]] = $value;
				}
				
				$coursemodule_user = $new_coursemodule_user;
				unset($new_coursemodule_user);
			}
		}

		$modules = array();
		foreach($coursemodules as $module){
			$module["modulenameurl"] = urlencode($module["modulename"]);
			
			if(array_key_exists($module["moduleid"], $coursemodule_user)){
				$module["module_user_status"] = $coursemodule_user[$module["moduleid"]]["module_user_status"];
				$module["module_user_statussmalltext"] = GetLang("CourseModuleUserStatusSmalltext".$coursemodule_user[$module["moduleid"]]["module_user_status"]);
				$module["module_user_statustext"] = GetLang("CourseModuleUserStatusText".$coursemodule_user[$module["moduleid"]]["module_user_status"]);
			}
			
			$modules[] = $module;
		}
		echo app_json_encode(array("success" => 1, "modules" => $modules));
		exit;
	}
	
	function remote_resetCourse(){
		if(!isset($_GET["courseid"]) || !isId($_GET["courseid"])){
			echo app_json_encode(array("success" => 0, "msg" => GetLang("CourseInvalid")));
			exit;			
		}
		
		$coursemodule_user_model = getModel("coursemodule_user");
		
		$cm = $coursemodule_user_model->delete(array("courseid" => $_GET["courseid"]));
		if(!($cm)){
			AddLog(sprintf(GetLang("ErrorResetingCourse"), $_GET["courseid"]).". ".$coursemodule_user_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$course_model = getModel("course");
		
		if(!($course_model->edit(array("coursemodified" => time()), array("courseid" => $_GET["courseid"])))){
			AddLog(sprintf(GetLang("ErrorResetingCourse"), $_GET["courseid"]).". ".$course_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}

		echo app_json_encode(array("success" => 1, "msg" => GetLang("CourseResetSuccess")));
		exit;
		
	}
	
	function addusermembership(){
		if(!isset($_POST["CourseEditAddUserMembershipToCourseCourseId"]) || !isId($_POST["CourseEditAddUserMembershipToCourseCourseId"])){
			flashMessage(GetLang("CourseInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		if(!isset($_POST["CourseEditAddUserMembershipToCourseMembershipType"]) || !is_numeric($_POST["CourseEditAddUserMembershipToCourseMembershipType"])){
			flashMessage(GetLang("CourseMembsershipInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		if(!isset($_POST["CourseEditAddUserMembershipToCourseUserAccess"]) || !is_numeric($_POST["CourseEditAddUserMembershipToCourseUserAccess"])){
			flashMessage(GetLang("CourseAccessInvalid"));
			header("Location: ".$GLOBALS["AppPath"]."/course/admin");
			exit;
		}
		
		$courseid = $_POST["CourseEditAddUserMembershipToCourseCourseId"];
		$membershiptype = $_POST["CourseEditAddUserMembershipToCourseMembershipType"];
		$user_access = $_POST["CourseEditAddUserMembershipToCourseUserAccess"];
		
		$course_user_model = getModel("course_user");
		$user_model = getModel("user");
		$users = $user_model->getResultSet(
				0,
				"*",
				array(
					"membershiptype" => $membershiptype,
				)
				);
		
		foreach($users as $user){
			$course_user = array(
					"courseid" => $courseid,
					"userid" => $user["userid"],
					"course_user_access" => $user_access,
			);
			
			$success = $course_user_model->add($course_user);
			if(!$success){
				flashMessage(GetLang("ErrorCourseMembership"));
				AddLog(sprintf("Ocurrio un error al asignar el usuario '%s' al curso id '%s'. Error: ", $user["username"], $courseid));
				exit;
			}
		}
		
		flashMessage(GetLang("CourseMembershipAdded"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS["AppPath"]."/course/edit?courseid=".$courseid);
		exit;
	}
}