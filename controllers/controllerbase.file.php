<?php

class APPCONTROLLERBASE_FILE extends APP_BASE {
	
	public $menu = array(
		"view" => 1,
		"admin" => 2,
		"create" => 2,
		"createsubmit" => 2,
		"edit" => 2,
		"editsubmit" => 2,
		"remote_deleteFile" => 2,
	);
	
	function view(){
		
	}
	
	function admin(){
		$file_model = getModel("file");
		
		$session_user = getUserData();
		if($session_user["usergroup"] == "3"){
			$where = array(); 
		}
		else {
			$where = array(
					"filecreator" => $session_user["userid"],
			);
		}
		
		$files = $file_model->getResultSet(
			0, 
			"*",
			$where	
			, //where
			array(
				"filetitle" => "ASC",
			) //order
		);
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";

		$fileAdminList = '<thead>
				<tr>
					<th>'.GetLang('Name').'</th>
					<th>'.GetLang('FileLink').'</th>
					<th>'.GetLang('FileCreator').'</th>
					<th>'.GetLang('Action').'</th>
				</tr>
			</thead>';
		if(empty($files)){
			$fileAdminList .= '<tbody><tr><td colspan="4">'.GetLang("NoFilesFound").'</td></tr></tbody>';
		}
		else {
			$fileAdminList .= '<tbody>';
			foreach($files as $file){
				$user = getUser($file["filecreator"]);
				$fileAdminList .= '<tr fileid="'.$file['fileid'].'">
							<td>'.$file["filetitle"].'</td>
							<td><a href="'.$GLOBALS["AppPath"].'/files/'.$file['fileid'].'/'.$file["filepath"].'">'.GetLang('FileLink').'</a></td>
							<td>'.makeUserLink($file["filecreator"], $user['firstname'].' '.$user['lastname']).'</td>
							<td>
								<a class="FileAdminEdit" href="'.$GLOBALS['AppPath'].'/file/edit?fileid='.$file['fileid'].'"><img src='.$GLOBALS['AppPath'].'/images/icon-edit.png /></a>
								<a class="FileAdminDelete"><img src='.$GLOBALS['AppPath'].'/images/delicon.png /></a> 
							</td>
						</tr>';
			}
			$fileAdminList .= '</tbody>';
		}
		
		$GLOBALS["FileAdminFileList"] = $fileAdminList;
	}
	
	function create(){
		overwritePostToGlobalVars();
	}
	
	function createsubmit(){
		$file_model = getModel('file');
		$user = getUserData();

		if(!isset($_POST['FileCreateFileTitle']) || trim(($_POST['FileCreateFileTitle']) == "")){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'File/CreateSubmit', 'FileCreateFileTitle'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/file/create");
			exit;
		}
		
		if(!isset($_FILES['FileCreateFileUpload'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'File/CreateSubmit', 'FileCreateFileUpload'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/file/create");
			exit;
		}
		
		$new_file = array(
			"filetitle" => $_POST['FileCreateFileTitle'],
			"filedesc" => $_POST['FileCreateFileDescription'],
			"filepath" => $_FILES['FileCreateFileUpload']['name'],
			"filecreator" => $user['userid'],
		);
		
		$GLOBALS['APP_CLASS_DB']->StartTransaction();
		$fileid = $file_model->add($new_file);
		
		$newpath = APP_BASE_PATH.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR.$fileid;
		if(!is_dir($newpath) && !app_mkdir($newpath, "0755")){
			flashMessage(GetLang('ErrorCreatingFile'));
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath'].'/file/create');
		}
		
		$newname = $newpath.DIRECTORY_SEPARATOR.$_FILES['FileCreateFileUpload']['name'];
		
		if(@rename($_FILES['FileCreateFileUpload']['tmp_name'], $newname)){
			chmod($newname, 0755);
			$GLOBALS['APP_CLASS_DB']->CommitTransaction();
			flashMessage(GetLang('FileCreatedSuccess'), APP_SEVERITY_SUCCESS);
			header("Location: ".$GLOBALS['AppPath'].'/file/admin');
			exit;
		}
		else {
			$GLOBALS['APP_CLASS_DB']->RollbackTransaction();
			flashMessage(GetLang('ErrorCreatingFile'), APP_SEVERITY_ERROR);
			AddLog('SQL: '.$file_model->getLastResultSetQuery());
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath'].'/file/create');
			exit;
		}
	}
	
	function edit(){
		if(!isset($_GET['fileid']) || !is_numeric($_GET['fileid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'File/Edit', 'fileid'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/file/admin");
			exit;
		}
		
		$file_model = getModel('file');
		$file = $file_model->get($_GET['fileid']);
		
		overwritePostToGlobalVars(array(
				"FileEditFileId" => $file['fileid'],
				"FileEditFileTitle" => $file['filetitle'],
				"FileEditFileDescription" => $file['filedesc'],
				"FileEditFilePath" => $file['filepath'],
			)
		);
	}
	
	function editsubmit(){
		$file_model = getModel('file');
		
		if(!isset($_POST['FileEditFileId']) || !is_numeric($_POST['FileEditFileId'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'File/EditSubmit', 'FileEditFileId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/file/admin");
			exit;
		}
		
		$file = $file_model->get($_POST['FileEditFileId']);
		
		if(!$file){
			flashMessage(GetLang("FileIdInvalid"), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/file/edit?fileid=".$_POST['FileEditFileId']);
			exit;
		}
		
		if(!isset($_POST['FileEditFileTitle']) || trim(($_POST['FileEditFileTitle']) == "")){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'File/EditSubmit', 'FileEditFileTitle'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS["AppPath"]."/file/edit?fileid=".$_POST['FileEditFileId']);
			exit;
		}

		if(trim($_FILES['FileEditFileUpload']['tmp_name']) == ""){
			$filepath = $file['filepath'];
		}
		else {
			$oldpath = APP_BASE_PATH.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR.$file['fileid'].DIRECTORY_SEPARATOR.$file['filepath'];
			if(file_exists($oldpath)){
				unlink($oldpath);
			}
			
			$newpath = APP_BASE_PATH.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR.$file['fileid'];
			if(!is_dir($newpath) && !app_mkdir($newpath, "0755")){
				flashMessage(GetLang('ErrorCreatingFile'));
				overwritePostToGlobalVars();
				header("Location: ".$GLOBALS['AppPath'].'/file/edit');
			}
			
			$newname = $newpath.DIRECTORY_SEPARATOR.$_FILES['FileEditFileUpload']['name'];
			if(!rename($_FILES['FileEditFileUpload']['tmp_name'], $newname)){
				flashMessage(GetLang('ErrorCreatingFile'), APP_SEVERITY_ERROR);
				overwritePostToGlobalVars();
				header("Location: ".$GLOBALS['AppPath']."/file/edit?fileid=".$_POST['FileEditFileId']);
				exit;
			}
			else {
				chmod($newname, 0755);
				$filepath = $_FILES['FileEditFileUpload']['name'];
			}
		}
		
		$edit_file = array(
				"filetitle" => $_POST['FileEditFileTitle'],
				"filedesc" => $_POST['FileEditFileDescription'],
				"filepath" => $filepath,
		);
		
		if(!$file_model->edit($edit_file, array('fileid' => $_POST['FileEditFileId']))){
			flashMessage(GetLang('ErrorCreatingFile'), APP_SEVERITY_ERROR);
			AddLog($file_model->getError());
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath']."/file/edit?fileid=".$_POST['FileEditFileId']);
			exit;
		}
		else {
			flashMessage(GetLang('FileEditedSuccess'), APP_SEVERITY_SUCCESS);
			header("Location: ".$GLOBALS['AppPath'].'/file/admin');
			exit;
		}
	}
	
	function remote_deleteFile(){
		if(!isset($_GET['fileid']) || !isId($_GET['fileid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'file/RemoteDeleteFile', 'fileid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$file_model = getModel("file");
		
		$file = $file_model->get(array("fileid" => $_GET["fileid"]));
		$success = $file_model->delete(array("fileid" => $_GET["fileid"]));
		
		if(!$success){
			AddLog(GetLang("ErrorDeletefile"). "Error: ".$file_model->GetError());
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		else {
			$user = getUserData();
			AddLogSuccess(sprintf(GetLang("SuccessUserDeletefile"), $user["username"], substr($file["filetext"], 0, 25), $_GET["fileid"], $file["questionid"], $file["examid"]));
			echo app_json_encode(array("success" => 1));
			exit;
		}
	}
}