<?php

class APPCONTROLLER_NEWS extends APP_BASE {

	public $menu = array(
			"view" => 0,
			"admin" => 2,
			"create" => 2,
			"createsubmit" => 2,
			"remote_getNewsList" => 2,
			"edit" => 2,
			"editsubmit" => 2,
			"remote_deleteNews" => 2,
			"sendemail" => 2,
			"sendemailsubmit" => 2,
	);
	
	function view(){
		$request = parseGetVars();
		
		if(isset($request[2])){
			$GLOBALS['AppRequestVars'][1] = "viewsingle";
			return $this->viewsingle($request[2]);
		}
		
		$this->breadcrumbs = array("index" => GetLang("Home"), "#" => GetLang("News"));
		$GLOBALS["NewsViewNewsList"] = $this->renderNews("*");
	}
	
	private function viewsingle($newsid){
		if(!isId($newsid)){
			flashMessage(GetLang("NewsItemInvalid"));
			$GLOBALS['AppRequestVars'][1] = "view";
			unset($GLOBALS['AppRequestVars'][2]);
			return;
		}
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Templates/base/news.css\">";
		
		$news_model = getModel("news");
		$news_item = $news_model->getSingleResultSet(
			0, 
			"*",
			array(
				"newsid" => $newsid,	
			), //where
			array(
				
			), //order
			array(
					
			) //columns
		);
		
		$this->breadcrumbs = array("index" => GetLang("Home"), "News" => GetLang("News"), "#" => $news_item["newstitle"]);
		
		$author = getUser($news_item["newsauthor"]);
		if(!$author){
			$author = $author = array("username" => GetLang("Unknown"));
		}
		
		$GLOBALS["NewsViewSingleNewsTitle"] = $news_item["newstitle"];
		$GLOBALS["NewsViewSingleNewsAuthor"] = $author["username"];
		$GLOBALS["NewsViewSingleNewsDate"] = date('d-M-Y H:m:s', $news_item["created"]);
		$GLOBALS["NewsViewSingleNewsContent"] = html_entity_decode($news_item["newscontent"]);
	}
	
	function admin(){
		$this->breadcrumbs = array("news/admin" => GetLang("News"), "#" => GetLang("Admin"));
		
		$GLOBALS["ViewStylesheet"] .= "<link rel=\"stylesheet\" href=\"".$GLOBALS["AppPath"]."/views/Styles/jquery-tablesorter/theme.blue.css\">";
		$GLOBALS["ViewScripts"] .= "<script src=\"".$GLOBALS["AppPath"]."/javascript/jquery-tablesorter/jquery.tablesorter.combined.min.js\"></script>";
	}

	function create(){
		$GLOBALS["ViewScripts"] .= '<script src="%%GLOBAL_AppPath%%/javascript/tinymce/tinymce.min.js"></script>';
	}
	
	function createsubmit(){
		$postFields = array(
				"NewsCreateNewsTitle",
				"NewsCreateNewsContent",
				"NewsCreateNewsStatus",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/Create', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				$GLOBALS['AppRequestVars'][1] = "create";
				overwritePostToGlobalVars();
				return;
			}
		}
		
		if(trim($_POST["NewsCreateNewsTitle"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("NewsTitle")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}
		
		if(trim($_POST["NewsCreateNewsStatus"]) != "" && !is_numeric($_POST["NewsCreateNewsStatus"])){
			flashMessage(sprintf(GetLang("PlaseInputNumber"), GetLang("NewsStatus")));
			$GLOBALS["AppRequestVars"][1] = "create";
			overwritePostToGlobalVars();
			return;
		}

		$user = getUserData();
		
		$news_model = getModel("news");
		$new_news = array(
				"newstitle" => $_POST["NewsCreateNewsTitle"],
				"newscontent" => htmlentities($_POST["NewsCreateNewsContent"]),
				"status" => $_POST["NewsCreateNewsStatus"],
				"newsauthor" => $user["userid"],
				"created" => microtime(true),
		);
		
		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		$newsid = $news_model->add($new_news);
		
		if(!$newsid){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(sprintf(GetLang("ErrorCreatingNews") . ". Error: ".$news_model->getError().".- Array[".print_r($_POST, true)."]", $new_news["newstitle"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath']."/news/create");
			exit;
		}
		
		$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
		flashMessage(GetLang("NewsCreatedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/news/admin");
		exit;
	}
	
	function remote_getNewsList(){
		$news_model = getModel("news");
		$news = $news_model->getResultSet(
			0, 
			"*",
			array(
					
			), //where
			array(
				"modified" => "DESC",
				"created" => "DESC",
				"id" => "DESC",
			), //order
			array(
				"*",
				"created_date" => "FROM_UNIXTIME(created)",
				"modified_date" => "IFNULL(FROM_UNIXTIME(modified), '-')",
			) //columns
		);
		
		foreach($news as $index => $news_item){
			$news[$index]["content_cropped"] = preg_replace('/[\r\n]+/', ' ', substr(strip_tags(html_entity_decode($news_item["newscontent"])), 0, 20));
		}
		
		echo app_json_encode(array("news" => $news));
		exit;
	}
	
	function edit(){
		$this->breadcrumbs = array("news/admin" => GetLang("News"), "#" => GetLang("Edit"));
		if(!isset($_GET['newsid']) || !isId($_GET['newsid'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/Edit', 'NewsEditNewsId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/news/admin");
			exit;
		}
		$GLOBALS["ViewScripts"] .= '<script src="%%GLOBAL_AppPath%%/javascript/tinymce/tinymce.min.js"></script>';
		
		$newsid = $_GET['newsid'];
		$GLOBALS["NewsEditNewsId"] = $newsid;
		
		$news_model = getModel("news");
		$news = $news_model->get(array("newsid" => $newsid));
		
		if(!$news){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/EditSubmit', 'NewsEditNewsId'));
			flashMessage(GetLang('ErrorNoNewsFound'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/news/admin");
			exit;
		}
		
		overwritePostToGlobalVars(array(
			"NewsEditNewsTitle" => $news["newstitle"],
			"NewsEditNewsContent" => $news["newscontent"],
			"NewsEditNewsStatus" => $news["status"],
		));
	}
	
	function editsubmit(){
		if(!isset($_POST['NewsEditNewsId']) || !isId($_POST['NewsEditNewsId'])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/EditSubmit', 'NewsEditNewsId'));
			flashMessage(GetLang('ErrorMsgGeneric'), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/news/admin");
			exit;
		}
		
		$newsid = $_POST['NewsEditNewsId'];
		$GLOBALS["NewsEditNewsId"] = $newsid;
		
		$postFields = array(
				"NewsEditNewsTitle",
				"NewsEditNewsContent",
				"NewsEditNewsStatus",
		);
		
		foreach($postFields as $field){
			if(!isset($_POST[$field])){
				AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/Create', $field));
				flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
				overwritePostToGlobalVars();
				header("Location: ".$GLOBALS['AppPath']."/news/edit?newsid=".$newsid);
				exit;
			}
		}
		
		if(trim($_POST["NewsEditNewsTitle"]) == ""){
			flashMessage(sprintf(GetLang("PleaseInputText"), GetLang("NewsTitle")));
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath']."/news/edit?newsid=".$newsid);
			exit;
		}
		
		if(trim($_POST["NewsEditNewsStatus"]) != "" && (!is_numeric($_POST["NewsEditNewsStatus"]))){
			flashMessage(sprintf(GetLang("PlaseInputNumber"), GetLang("NewsStatus")));
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath']."/news/edit?newsid=".$newsid);
			exit;
		}
		
		$news_model = getModel("news");
		$new_news = array(
				"newstitle" => $_POST["NewsEditNewsTitle"],
				"newscontent" => htmlentities($_POST["NewsEditNewsContent"]),
				"status" => $_POST["NewsEditNewsStatus"],
				"modified" => microtime(true),
		);
		
		$GLOBALS["APP_CLASS_DB"]->StartTransaction();
		$success = $news_model->edit($new_news, array("newsid" => $newsid));
		
		if(!$newsid){
			$GLOBALS["APP_CLASS_DB"]->RollbackTransaction();
			AddLog(sprintf(GetLang("ErrorCreatingNews") . ". Error: ".$news_model->getError().".- Array[".print_r($_POST, true)."]", $new_news["newstitle"]), APP_SEVERITY_ERROR);
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			overwritePostToGlobalVars();
			header("Location: ".$GLOBALS['AppPath']."/news/edit?newsid=".$newsid);
			exit;
		}
		
		$GLOBALS["APP_CLASS_DB"]->CommitTransaction();
		flashMessage(GetLang("NewsEditedSuccess"), APP_SEVERITY_SUCCESS);
		header("Location: ".$GLOBALS['AppPath']."/news/admin");
		exit;		
	}
	
	function remote_deleteNews(){
		if(!isset($_GET["newsid"]) || !isId($_GET["newsid"])){
			AddLog(sprintf(GetLang("ErrorPostVarNotSet"), 'News/RemoteDeleteNews', 'newsid'));
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;
		}
		
		$news_model = getModel("news");
		$success = $news_model->delete(array("newsid" => $_GET["newsid"]));

		if($success){
			echo app_json_encode(array("success" => 1));
			exit;
		}
		else {
			AddLog(sprintf(GetLang("ErrorDeletingNews"), $_GET["newsid"]) . ". Error: ".$news_model->getError(),  APP_SEVERITY_ERROR);
			echo app_json_encode(array("success" => 0, "msg" => GetLang("ErrorMsgGeneric")));
			exit;			
		}
	}
	
	public function renderNews($limit = 5){
		$news_model = getModel("news");
		
		$news = $news_model->getResultSet(
			0,
			$limit,
			array(
				"status" => 1,		
			), //where
			array(
				"created" => "DESC",
			), //order
			array(
					
			) //columns
		);

		if(empty($news)){
			return "<div>".GetLang("NoNewsFound")."</div>";
		}
		
		$return = "";
		foreach($news as $news_item){
			$author = getUser($news_item["newsauthor"]);
			if(!$author){
				$author = $author = array("username" => GetLang("Unknown"));
			}
			$return .= '<div class="NewsItem">
					<div class="NewsTitle"><h3><a href="'.$GLOBALS["AppPath"].'/news/view/'.$news_item["newsid"].'">'.$news_item["newstitle"].'</a></h3></div>
					<h4><span class="NewsAuthor">'.$author["username"].'</span> - <span class="NewsDate">'.date('d-M-Y H:m:s', $news_item["created"]).'</span></h4>
					<div class="NewsContent">'.html_entity_decode($news_item["newscontent"]).'</div>
				</div>';
		}
		
		return $return;
	}
	
	public function sendemail(){
		if(!isset($_GET["newsid"]) || !isId($_GET["newsid"])){
			AddLogError(GetLang("ErrorInvalidNews"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			return;
		}
		
		$GLOBALS["NewsSendMailNewsId"] = $_GET["newsid"];
		
		$usergroup_model = getModel("usergroup");
		$usergroups = $usergroup_model->getResultSet(0, "*");

		$GLOBALS["NewsSendEmailUserGroups"] = "";
		if(is_array($usergroups) && !empty($usergroups)){
			foreach($usergroups as $usergroup){
				$GLOBALS["NewsSendEmailUserGroups"] .= '<li><label for="NewsSentEmailUserGroup'.$usergroup["groupid"].'"><input type="checkbox" name="NewsSentEmailUserGroups[]" id="NewsSentEmailUserGroup'.$usergroup["groupid"].'" value="'.$usergroup["groupid"].'" />'.$usergroup["groupname"].'</label></li>';
			}
		}
		
		if(isset($_SESSION["UserSendMailResultTable"]) && trim($_SESSION["UserSendMailResultTable"]) != ""){
			$GLOBALS["NewsSendMailResultsShow"] = "display: block";
			$GLOBALS["UserSendMailResultTable"] = $_SESSION["UserSendMailResultTable"];
			unset($_SESSION["UserSendMailResultTable"]);
		}
		else {
			$GLOBALS["NewsSendMailResultsShow"] = "display: none";
		}
	}
	
	public function sendemailsubmit(){
		if(!isset($_POST["NewsSendMailNewsId"]) || !isId($_POST["NewsSendMailNewsId"])){
			AddLogError(GetLang("ErrorInvalidNews"));
			flashMessage(GetLang("ErrorMsgGeneric"), APP_SEVERITY_ERROR);
			header("Location: ".$GLOBALS["AppPath"]."/news/admin");
			return;
		}
		
		$news_model = getModel("news");
		
		$newsitem = $news_model->get(array("newsid" => $_POST["NewsSendMailNewsId"]));
		
		$mailer = getLib("smtpmailer");
		
		$subject = "[".GetConfig("SiteName")."] ".GetLang("MailSubjectNews").": ".$newsitem["newstitle"];
		
		$GLOBALS["MailNewsTitle"] = $newsitem["newstitle"];
		$author = getUser($newsitem["newsauthor"]);
		$GLOBALS["MailNewsAuthorName"] = $author["firstname"]." ".$author["lastname"];
		$GLOBALS["MailNewsDate"] = formatDateSpanish($newsitem["created"]);
		$GLOBALS["MailNewsContent"] = html_entity_decode($newsitem["newscontent"]);

		$body = $GLOBALS["APP_CLASS_VIEW"]->GetSnippet("MailNews");

		$user_model = getModel("user");
		$users = $user_model->getResultSet(0, "*", 'membershiptype IN ("'.implode('", "', $_POST["NewsSentEmailUserGroups"]).'")');
		
		$_SESSION["UserSendMailResultTable"] = "";
		$_SESSION["NewsSendMailResultsShow"] = "display: none";
		if(is_array($users) && !empty($users)){
			flashMessage(GetLang("EmailsSent"), APP_SEVERITY_SUCCESS);
			$GLOBALS["NewsSendMailResultsShow"] = "display: block";
			foreach($users as $user){
				if(trim($user["mail"]) == ""){
					continue;
				}
				$success = $mailer->send($subject, $body, $user["mail"]);
				
				if($success){
					$class = "UserSendMailResultRowSuccess";
					$msg = "-";
				}
				else {
					$class = "UserSendMailResultRowError";
					$msg = $mailer->getErrors();
				}
				
				$_SESSION["UserSendMailResultTable"] .= '<tr class="'.$class.'">
							<td>'.$user["mail"].'</td>
							<td>'.$msg.'</td>
								</tr>';
			}
		}
		
		header("Location: ".$GLOBALS["AppPath"]."/news/sendemail?newsid=".$_POST["NewsSendMailNewsId"]);
		exit;
	}
}