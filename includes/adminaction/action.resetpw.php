<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPACTION_RESETPW extends ADMINACTIONBASE {
	
	public function actiondefault(){
		$user_model = getModel("user");
		
		$filters = array();
		
		if(isset($_GET["emptypw"]) && $_GET["emptypw"] == "1"){
			$filters["password"] = "";
		}
		
		if(isset($_GET["user"]) && trim($_GET["user"]) != ""){
			$filters["username"] = $_GET['user'];
		}

		$users = $user_model->getResultSet(0, "*", $filters);

		if(!$users || empty($users)){
			$this->addToLog("No hay usuarios");
			$this->printLog('RESETPW');
			exit;
		}

		foreach($users as $user){
			$password = substr(md5(uniqid()), 0, 8);
			$salt = substr(md5(uniqid()), 0, 16);
			
			$user_edit = array(
				"salt" => $salt,
				"password" => appGeneratePasswordHash($password, $salt),
			);
		
			$success = $user_model->edit($user_edit, array("userid" => $user["userid"]));
		
			if(!$success){
				$this->addToLog(sprintf("Error al modificar el usuario %s. Error: ".$user_model->getError(), $user["userid"]));
				break;
			}
			else {
				$this->addToLog(sprintf("Usuario: %s (%s, %s). Nuevo password: %s", $user["username"], $user["lastname"], $user["firstname"], $password));			
			}
		}
	
		$this->printLog("RESETPW");
	}
} 