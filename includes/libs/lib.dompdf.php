<?php

include_once 'Classes/dompdf/autoload.inc.php';

use Dompdf\Dompdf;

class APPLIB_DOMPDF {

	private $dompdf;
	
	private $paper = 'A4';
	private $orientation = 'portrait';
	private $return_type = 'stream';
	
	public function __construct(){
		$this->dompdf = new Dompdf();
	}
	
	public function setReturn($type = "stream"){
		if(in_array($type, array("stream", "html"))){
			$this->return_type = $type;
		}
	}
	
	public function renderPdf($viewname){
		$view = $GLOBALS["APP_CLASS_VIEW"]->parseView($viewname, true);
		
		if($this->return_type == 'stream'){
			$this->dompdf->loadHtml($view);
			
			$this->dompdf->setPaper($this->paper, $this->orientation);
			
			$this->dompdf->render();
			
			header('Content-Type: application/pdf');
			header('Content-Disposition: inline; filename="userexam"');
			print $this->dompdf->output();
			exit;
		}
		
		if($this->return_type == "html"){
			print $view;
			exit;
		}
	}
}