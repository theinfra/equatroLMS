<?php

include_once 'Classes/PHPMailer/PHPMailerAutoload.php';

class APPLIB_SMTPMAILER {
	private $mailer;
	private $user;
	private $password;

	public function __construct(){
		$this->mailer = new PHPMailer();
		
		$this->mailer->isSMTP();
		$this->mailer->Host = GetConfig("SMTP_server");
		$this->mailer->SMTPAuth = true;
		$this->mailer->Username = GetConfig("SMTP_user");
		$this->mailer->Password = GetConfig("SMTP_password");
		$this->mailer->SMTPSecure = 'tls';
		$this->mailer->Port = 465;
		$this->mailer->CharSet = 'UTF-8';
		
		$this->mailer->setFrom($this->mailer->Username);
	}
	
	public function send($subject = "", $body = "", $to ="", $cc = array(), $bcc = array()){
		$this->mailer->addAddress($to);
		$this->mailer->isHTML(true);
		
		$this->mailer->Subject = $subject;
		$this->mailer->Body    = $body;
		$this->mailer->AltBody = strip_tags($body);
		
		if(!$this->mailer->send()){
			AddLogError(GetLang("ErrorSendingMail").". ".print_array($this, true, true));
			return false;
		}
		else {
			return true;
		}
	}
	
	public function getErrors(){
		return $this->mailer->ErrorInfo;
	}
}