<?php
include 'Classes/PHPExcel.php';
include 'Classes/PHPExcel/Writer/Excel2007.php';

class APPLIB_EXCELEXPORTER {
	private $class;
	private $writer;
	
	public function __construct(){
		$this->class = new PHPExcel();
		$this->writer = new PHPExcel_Writer_Excel2007($this->class);
	}
	
	public function loadData($data){
		$this->class->getActiveSheet()->fromArray($data, null, 'A1');
	}
	
	public function download($filename){
		if(substr($filename, strrpos($filename, ".")) == "xls"){
			header('Content-type: application/vnd.ms-excel');
		}
		else {
			header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		}
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		
		$this->writer->save('php://output');
	}
}