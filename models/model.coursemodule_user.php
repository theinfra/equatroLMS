<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_COURSEMODULE_USER extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"moduleid" => array(
					"type" => "int",
					"size" => 11,
					"null" => false,
				),
				"courseid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"userid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"dateupdated" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"module_user_status" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
		);

		$tableName = "coursemodule_user";

		$primaryKeyName = array(
				"moduleid",
				"userid",
				);
		$searchFields = array(
				"courseid",
				"moduleorder",
				
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
