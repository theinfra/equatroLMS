<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_QUESTION extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"questionid" => array(
					"type" => "int",
					"size" => 11,
					"auto_increment" => true,
					"null" => false,
				),
				"examid" => array(
						"type" => "varchar",
						"size" => 20,
						"null" => false,
				),
				"questionorder" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"questiontext" => array(
						"type" => "text",
						"null" => false,
				),
				"questionhelp" => array(
						"type" => "text",
						"null" => true,
				),
				"correctanswer" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
		);

		$tableName = "question";
		$primaryKeyName = array(
				"questionid",
				);
		$searchFields = array(
				"examid",
				"questionorder",
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
