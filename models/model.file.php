<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_FILE extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"fileid" => array(
					"type" => "int",
					"size" => 11,
					"auto_increment" => true,
					"null" => false,
				),
				"filetitle" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => false,
				),
				"filedesc" => array(
						"type" => "text",
						"null" => true,
				),
				"filepath" => array(
						"type" => "varchar",
						"size" => 250,
						"null" => false,
				),
				"filecreator" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
		);

		$tableName = "file";
		$primaryKeyName = array(
				"fileid",
				);
		$searchFields = array(
				"filetitle",
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
