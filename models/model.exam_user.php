<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_EXAM_USER extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"examid" => array(
					"type" => "int",
					"size" => 11,
					"null" => false,
				),
				"userid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"exam_user_tryno" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"exam_user_submitdate" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"questionid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"answerid_selected" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
		);

		$tableName = "exam_user";
		$primaryKeyName = array(
				"examid",
				"userid",
				"exam_user_tryno",
				"questionid",
				);
		$searchFields = array(
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
