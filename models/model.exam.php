<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_EXAM extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"examid" => array(
					"type" => "int",
					"size" => 11,
					"auto_increment" => true,
					"null" => false,
				),
				"examname" => array(
						"type" => "varchar",
						"size" => 20,
						"null" => false,
				),
				"examtitle" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => false,
				),
				"examdesc" => array(
						"type" => "text",
						"null" => true,
				),
				"examdatecreated" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"examdatestart" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"examdateend" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"examtriesperuser" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"examshowanswers" => array(
						"type" => "enum",
						"size" => "'no','question','end'",
						"null" => false,
						"default" => "no",
				),
				"examcreator" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"examnumquestions" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
						"default" => 0,
				),
				"exampassinggrade" => array(
						"type" => "int",
						"size" => 3,
						"null" => false,
				),
				"examquestionorder" => array(
						"type" => "enum",
						"size" => "'random','defined'",
						"null" => "false",
				),
				"examduration" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
		);

		$tableName = "exam";
		$primaryKeyName = array(
				"examid",
				);
		$searchFields = array(
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
