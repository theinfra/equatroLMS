<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_COURSE_USER extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"courseid" => array(
					"type" => "int",
					"size" => 11,
					"null" => false,
				),
				"userid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"course_user_access" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
		);

		$tableName = "course_user";
		$primaryKeyName = array(
				"courseid",
				"userid"
				);
		$searchFields = array(

		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
