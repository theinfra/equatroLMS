<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_USERGROUP extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"groupid" => array(
							"type" => "int",
							"size" => 11,
							"auto_increment" => true,
							"null" => false,
						),
				"groupname" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => false,
				),
		);

		$tableName = "user_group";
		$primaryKeyName = array(
				"groupid",
				);
		$searchFields = array(
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
