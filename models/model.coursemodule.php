<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_COURSEMODULE extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"moduleid" => array(
					"type" => "int",
					"size" => 11,
					"auto_increment" => true,
					"null" => false,
				),
				"courseid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"modulename" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => true,
				),
				"moduleorder" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"moduletype" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"moduleobjectid" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"moduleobjectrequired" => array(
						"type" => "smallint",
						"size" => 1,
						"null" => true,
				),
				"modulestatus" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
		);

		$tableName = "coursemodule";

		$primaryKeyName = array(
				"moduleid",
				);
		$searchFields = array(
				"courseid",
				"moduleorder",
				
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
