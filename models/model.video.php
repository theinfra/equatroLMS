<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_VIDEO extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"videoid" => array(
						"type" => "int",
						"size" => 11,
						"auto_increment" => true,
						"null" => false,
				),
				"videoexternalid" => array(
						"type" => "varchar",
						"size" => 64,
						"null" => false,
				),
				"videotitle" => array(
						"type" => "varchar",
						"size" => 100,
						"null" => false,
				),
				"videodesc" => array(
						"type" => "varchar",
						"size" => 5000,
						"null" => false,
				),
				"allowpause" => array(
						"type" => "smallint",
						"size" => 1,
						"null" => false,
				),
		);

		$tableName = "video";
		$primaryKeyName = array(
				"videoid",
				);
		$searchFields = array(

		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
