<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_ANSWER extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"answerid" => array(
						"type" => "int",
						"size" => 11,
						"auto_increment" => true,
						"null" => false,
				),
				"examid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"questionid" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"answerorder" => array(
						"type" => "int",
						"size" => 1,
						"null" => false,
				),
				"answertext" => array(
						"type" => "text",
						"null" => false,
				),
		);

		$tableName = "answer";
		$primaryKeyName = array(
				"answerid",
				);
		$searchFields = array(
				"examid",
				"questionid",
				"answerorder",
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
