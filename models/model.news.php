<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_NEWS extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"newsid" => array(
						"type" => "int",
						"size" => 11,
						"auto_increment" => true,
						"null" => false,
				),
				"newstitle" => array(
						"type" => "varchar",
						"size" => 250,
						"null" => false,
				),
				"newscontent" => array(
						"type" => "text",
						"null" => false,
				),
				"newsauthor" => array(
						"type" => "int",
						"size" => 1,
						"null" => false,
				),
				"status" => array(
						"type" => "smallint",
						"size" => 1,
						"null" => false,
				),
				"created" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"modified" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
		);

		$tableName = "news";
		$primaryKeyName = array(
				"newsid",
				);
		$searchFields = array(
				"newstitle",
				"newsauthor",
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
