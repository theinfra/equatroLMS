<?php

require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'base.php');

class APPMODEL_COURSE extends APPMODELBASE
{

	/**
	 * Constructor
	 *
	 * Base constructor
	 *
	 * @access public
	 */
	public function __construct()
	{
		$schema = array(
				"courseid" => array(
					"type" => "int",
					"size" => 11,
					"auto_increment" => true,
					"null" => false,
				),
				"coursename" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => false,
				),
				"coursestart" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"courseend" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"courseinstructorname" => array(
						"type" => "varchar",
						"size" => 50,
						"null" => true,
				),
				"courseinstructoruserid" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,
				),
				"coursestatus" => array(
						"type" => "int",
						"size" => 11,
						"null" => false,
				),
				"courseinorder" => array(
						"type" => "smallint",
						"size" => 1,
						"null" => false,
						"default" => 1,
				),
				"coursemodified" => array(
						"type" => "int",
						"size" => 11,
						"null" => true,					
				),
		);

		$tableName = "course";
		$primaryKeyName = array(
				"courseid",
				);
		$searchFields = array(
				"coursename",
				"courseinstructorname",
				"coursetstatus",
		);
		
		$customKeyName = array();

		parent::__construct($schema, $tableName, $primaryKeyName, $searchFields, $customKeyName);
	}

}
